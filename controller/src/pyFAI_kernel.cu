/*
 *(c) Kieffer and DAU/ESRF
 *
 * original code for OpenCL 
 *
 */
#ifndef X87_VOLATILE
#define X87_VOLATILE
#endif

#define WORKGROUP_SIZE  32

// calculate a + b with error compensation
__device__ static inline float2 kahan_sum(float2 acc, float value)
{
    if (value)
    {
        float sum = acc.x;
        float err = acc.y;
        if (fabs(value) > fabs(sum))
        {
            float tmp = sum;
            sum = value;
            value = tmp;
        }

        float cor = value + err;
        X87_VOLATILE float target = sum + cor;
        err = cor - (target - sum);
        return make_float2(target, err);
    }
    else
    {
        return acc;
    }
}

__device__ static inline float2 compensated_sum(float2 a, float2 b)
{
    float err = a.y + b.y;
    float first = a.x;
    float second = b.x;
    if (fabs(second) > fabs(first))
    {//swap first and second
        float tmp = first;
        first = second;
        second = tmp;
    }
    float cor = second + err;
    
    X87_VOLATILE float target = first + cor;    
    err = cor - (target - first);
    
    return make_float2(target, err);
}


//Compute Azimuthal integration
__global__ void
csr_integrate(  const     float   *weights, //input image

                const     float   *coefs,   //CSR matrix prepared with ai.setFit2D, constant duing experiment
                const     int     *indices,
                const     int     *indptr,

                const     char     do_dummy,
                const     float    dummy,
                const     int      coef_power,

                          float   *sum_data,
                          float   *sum_count,
                          float   *merged
             )
{

    // each workgroup (ideal size: warp) is assigned to 1 bin
    int bin_num = blockIdx.x;
    int thread_id_loc = threadIdx.x;
    int active_threads = blockDim.x;
    
    int2 bin_bounds = make_int2 (indptr[bin_num], indptr[bin_num + 1]);
    int bin_size = bin_bounds.y - bin_bounds.x;
    // we use _K suffix to highlight it is float2 used for Kahan summation
    float2 sum_data_K = make_float2(0.0f, 0.0f);
    float2 sum_count_K = make_float2(0.0f, 0.0f);
    const float epsilon = 1e-10f;
    float coef, data;
    int idx, k, j;

    for (j=bin_bounds.x;j<bin_bounds.y;j+=WORKGROUP_SIZE)
    {
        k = j+thread_id_loc;
        if (k < bin_bounds.y)
        {
               coef = coefs[k];
               idx = indices[k];
               data = weights[idx];
               if  (! isfinite(data))
                   continue;

               if( (!do_dummy) || (data!=dummy) )
               {
                   //sum_data +=  coef * data;
                   //sum_count += coef;
                   //Kahan summation allows single precision arithmetics with error compensation
                   //http://en.wikipedia.org/wiki/Kahan_summation_algorithm
                   // defined in kahan.cl
                   sum_data_K = kahan_sum(sum_data_K, ((coef_power == 2) ? coef*coef: coef) * data);
                   sum_count_K = kahan_sum(sum_count_K, coef);
               };//end if dummy
       } //end if k < bin_bounds.y
       };//for j
/*
 * parallel reduction
 */

    // REMEMBER TO PASS WORKGROUP_SIZE AS A CPP DEF
    __shared__ float2 super_sum_data[WORKGROUP_SIZE];
    __shared__ float2 super_sum_count[WORKGROUP_SIZE];

    int index;

    if (bin_size < WORKGROUP_SIZE)
    {
        if (thread_id_loc < bin_size)
        {
            super_sum_data[thread_id_loc] = sum_data_K;
            super_sum_count[thread_id_loc] = sum_count_K;
        }
        else
        {
            super_sum_data[thread_id_loc] = make_float2(0.0f, 0.0f);
            super_sum_count[thread_id_loc] = make_float2(0.0f, 0.0f);
        }
    }
    else
    {
        super_sum_data[thread_id_loc] = sum_data_K;
        super_sum_count[thread_id_loc] = sum_count_K;
    }
    __syncthreads();
    
    while (active_threads != 1)
    {
        active_threads /= 2;
        if (thread_id_loc < active_threads)
        {
            index = thread_id_loc + active_threads;
            super_sum_data[thread_id_loc] = compensated_sum(super_sum_data[thread_id_loc], super_sum_data[index]);
            super_sum_count[thread_id_loc] = compensated_sum(super_sum_count[thread_id_loc], super_sum_count[index]);
        }
        __syncthreads();
    }

    if (thread_id_loc == 0)
    {
        sum_data[bin_num] = super_sum_data[0].x;
        sum_count[bin_num] = super_sum_count[0].x;
        if (sum_count[bin_num] > epsilon)
            merged[bin_num] =  sum_data[bin_num] / sum_count[bin_num];
        else
            merged[bin_num] = dummy;
    }
}

