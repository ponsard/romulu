#ifndef __ROMULU_JF_H_INCLUDED__
#define __ROMULU_JF_H_INCLUDED__

#define BUFFER_LENGTH 4096

#pragma pack(push)
#pragma pack(2)

typedef struct _jf_header {
  char emptyheader[6];
  uint64_t framenum; ///
  uint32_t exptime;
  uint32_t packetnum; ///
  uint64_t bunchid;   ///
  uint64_t timestamp;
  uint16_t moduleID;
  uint16_t xCoord;
  uint16_t yCoord;
  uint16_t zCoord;
  uint32_t debug;
  uint16_t roundRobin;
  uint8_t detectortype;
  uint8_t headerVersion;
} jf_header;

typedef struct _jf_packet {
  jf_header hdr;
  uint16_t data[BUFFER_LENGTH];
} jf_packet;

#endif
