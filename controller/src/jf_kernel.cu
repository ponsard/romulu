
/**
 * 
 * unpack n images in one kernel
 * 
 */ 
 __global__ void
jfdepack(const uint16_t *raw,  
		const float *P0, const float *P1, const float *P2,
		const float *G0, const float *G1, const float *G2,
		const uint16_t n,
		float *res)
{
	unsigned int tid = threadIdx.x;

	gain = raw >> 14 ;
	#define mask ((1 << 14) -1)
	
	if (gain == 1)
		res[tid] = (((float)raw[tid] & mask) - P0[tid])/G0[tid];
	else
	if (gain == 2)
		res[tid] = (((float)raw[tid] & mask) - P1[tid])/G1[tid];
	else
	if (gain == 3)
		res[tid] = (((float)raw[tid] & mask) - P2[tid])/G2[tid];
	else 
		res[tid] = 0.0f
}

__global__ void
Average(const float *in,  float *out, int n)
{
	extern __shared__ int sdata[];
	
	unsigned int tid = threadIdx.x;
	int i = threadIdx.x + blockIdx.x * blockDim.x;

	
	sdata[i] = in[i] + in[i+1];
	
	
	if (tid == 0) g_odata[blockIdx.x] = sdata[0]
}

template<typename T1, typename T2>
__global__ void rightRotation(T1 *inputImage, T2 *outputImage,int Width, int Height) 
{
	unsigned int i = blockIdx.x * blockDim.x + threadIdx.x, ii;
	unsigned int j = blockIdx.y * blockDim.y + threadIdx.y, jj;
	unsigned int N = blockIdx.z;

	ii = j;
	jj = Width - 1 - i;

	if (i < Width && j < Height && ii < Height && jj < Width) 
	{
		outputImage[ii + jj * Height + N * Width * Height] = (T2) inputImage[i + j * Width + N * Width * Height];
	}
}

template<typename Tin, typename Tout>
__global__ void leftRotation_float(Tin *inputImage, Tout *outputImage, int Width, int Height) 
{
	unsigned int i = blockIdx.x * blockDim.x + threadIdx.x, ii;
	unsigned int j = blockIdx.y * blockDim.y + threadIdx.y, jj;
	unsigned int N = blockIdx.z;

	ii = Height-1 - j;
	jj = i;

	if (i < Width && j < Height && ii < Height && jj < Width) 
	{
		outputImage[ii + jj * Height + N * Width * Height] = (Tout) inputImage[i + j * Width + N * Width * Height];
	}
}


template <typename Tin, typename Tout> 
__global__ void Tin2Tout_lut(Tin *data, Tout *outputImage, int Width, int Height) 
{
	unsigned int i = blockIdx.x * blockDim.x + threadIdx.x;
	unsigned int j = blockIdx.y * blockDim.y + threadIdx.y;
	unsigned int N = blockIdx.z;
	
	#define HEIGHT 512
	__shared__ unsigned int lut[HEIGHT*16]; //16 is bunch size

	if (i < Width && j < Height) 
	{
		lut[j + N*Height] = ((jf_packet*)((uint64_t) data + j/4*sizeof(jf_packet) + N * Height / 4*sizeof(jf_packet)))->hdr.bunchid;
	}
	
	__syncthreads();


	// if(i==0 && j==0 && N==0)
	// 	for(int i=0;i<512;i++)
	// 		printf("%d %d\n",i,lut[i]);
	int jj = 4*lut[j + N * Height];
	void* img = (void*)(((uint64_t)data) + N * Height/4*sizeof(jf_packet) + sizeof(jf_header)+ j/4*sizeof(jf_packet));

	if (i < Width && j < Height ) 
	{	
		//jungfrau send 4 lines at once 
		outputImage[i + (jj + 0) * Width + N * Width * Height] = (Tout) ((Tin *) img) [i + 0 * Width];
		outputImage[i + (jj + 1) * Width + N * Width * Height] = (Tout) ((Tin *) img) [i + 1 * Width];
		outputImage[i + (jj + 2) * Width + N * Width * Height] = (Tout) ((Tin *) img) [i + 2 * Width];
		outputImage[i + (jj + 3) * Width + N * Width * Height] = (Tout) ((Tin *) img) [i + 3 * Width];
	}
};

// __global__ void rightRotation_uint16tofloat_lut(Tin *data, Tout *outputImage, int Width, int Height) {
// 	unsigned int i = blockIdx.x * blockDim.x + threadIdx.x;
// 	unsigned int j = blockIdx.y * blockDim.y + threadIdx.y;
// 	unsigned int N = blockIdx.z;
	
// 	#define HEIGHT 512
// 	__shared__ unsigned int lut[HEIGHT*16]; //16 is bunch size

// 	if (i < Width && j < Height) 
// 	{
// 		lut[j + N*Height] = ((jf_packet*)((void*)data + j/4*sizeof(jf_packet) + N * Height / 4*sizeof(jf_packet)))->hdr.bunchid;
// 	}

// 	__syncthreads();

	


// 	// int jj = 4*lut[j + N * Height];
// 	void* img = ((void*)data) + N * Height/4*sizeof(jf_packet) + sizeof(jf_header)+ j/4*sizeof(jf_packet);

// 	int ii = j;
// 	int jjj = Width - 1 - i;


// 	if (i < Width && j < Height && ii < Height && jjj < Width) 
// 	{	
// 		//outputImage[ii + jj * Height + N * Width * Height] = (Tout) inputImage[i + j * Width + N * Width * Height];
// 		outputImage[ii + (jjj + 0) * Height + N * Width * Height] = (Tout) ((Tin *) img) [ii + 0 * Width];
// 		outputImage[ii + (jjj + 1) * Height + N * Width * Height] = (Tout) ((Tin *) img) [ii + 1 * Width];
// 		outputImage[ii + (jjj + 2) * Height + N * Width * Height] = (Tout) ((Tin *) img) [ii + 2 * Width];
// 		outputImage[ii + (jjj + 3) * Height + N * Width * Height] = (Tout) ((Tin *) img) [ii + 3 * Width];
// 	}
// }


//~ template <unsigned int blockSize>
//~ __global__ void
//~ Average(const float16 *A,  float16 *C, int n)
//~ {
	//~ extern __shared__ int sdata[];
	
	//~ unsigned int tid = threadIdx.x;
	//~ unsigned int i = blockIdx.x*(blockDim.x*2) + threadIdx.x;
	//~ unsigned int gridSize = blockSize*2*gridDim.x;
	
	//~ sdata[tid] = 0;
	//~ while (i < n) {
		//~ sdata[tid] += g_idata[i] + g_idata[i+blockSize];
		//~ i += gridSize;
	//~ }
	//~ __syncthreads()
	
	//~ if (blockSize >= 512) { if (tid < 256) { sdata[tid] += sdata[tid + 256]; } __syncthreads(); }
	//~ if (blockSize >= 256) { if (tid < 128) { sdata[tid] += sdata[tid + 128]; } __syncthreads(); }
	//~ if (blockSize >= 128) { if (tid <   64) { sdata[tid] += sdata[tid +   64]; } __syncthreads(); }
	
	//~ if (tid < 32)
	//~ {
		//~ if (blockSize >=  64) sdata[tid] += sdata[tid + 32];
		//~ if (blockSize >=  32) sdata[tid] += sdata[tid + 16];
		//~ if (blockSize >=  16) sdata[tid] += sdata[tid +  8];
		//~ if (blockSize >=    8) sdata[tid] += sdata[tid +  4];
		//~ if (blockSize >=    4) sdata[tid] += sdata[tid +  2];
		//~ if (blockSize >=    2) sdata[tid] += sdata[tid +  1];
	//~ }
	
	//~ if (tid == 0) g_odata[blockIdx.x] = sdata[0]
//~ }
	//  if (in_bdepth==2) T_in = uint16_t;
	//  else T_in=float;

	
	//Using Nividia Performances Library
	// Npp64f mean_f = 13.0, std_f = 2.0,Sum=0, *d_mean_f, *d_std_f, *pSum;
    // cudaMalloc(&d_mean_f, sizeof(Npp64f));
	// cudaMalloc(&d_std_f, sizeof(Npp64f));
	// cudaMalloc(&pSum, sizeof(Npp64f));

	// NppiSize total_npp;//upper_npp;
    // total_npp.width = width;
    // total_npp.height = height;
	

    //scratch buffer
    // int scratchBuffSize = 0;
    // nppiSumGetBufferHostSize_32f_C1R(total_npp, &scratchBuffSize);
    // std::cout << "Size of scratch buffer for Sum = " << scratchBuffSize << std::endl;
    // nppiMeanStdDevGetBufferHostSize_32f_C1R(total_npp, &scratchBuffSize);
    // std::cout << "Size of scratch buffer for MeanStdDev = " << scratchBuffSize << std::endl;
    // Npp8u *d_scratch;
    // cudaMalloc(reinterpret_cast<void **>(&d_scratch), scratchBuffSize * sizeof(Npp8u));


    //Npp32f * h_input = reinterpret_cast<Npp32f*>(data);
    // Npp32f * d_input;
    // uint16_t*d_In16;
    //gpuErrchk(cudaMalloc(reinterpret_cast<void **>(&d_input), sizeof(Npp32f) * TOTAL_SIZE));
    //gpuErrchk(cudaMalloc(&d_In16, sizeof(uint16_t) * TOTAL_SIZE));

    //cudaMemcpy(d_In16, data, TOTAL_SIZE * sizeof(uint16_t), cudaMemcpyHostToDevice);

	for(int i = 0;i<0; i++)
			{
				// nppiMean_StdDev_32f_C1R(	(float*)d_Out1+i*width*height,
				// 							width * sizeof(Npp32f),
				// 							total_npp,
				// 							d_scratch,
				// 							d_mean_f,
				// 							d_std_f);

				// //back to CPU to compute sum
				// cudaStreamSynchronize(gpu->s1);
				// cudaMemcpy(&mean_f, d_mean_f, sizeof(Npp64f), cudaMemcpyDeviceToHost);
				// cudaMemcpy(&std_f, d_std_f, sizeof(Npp64f), cudaMemcpyDeviceToHost);

				// nppiThreshold_LTVal_32f_C1IR	((float*)d_Out1+i*width*height,
				// 								width * sizeof(Npp32f),
				// 								total_npp,
				// 								mean_f+std_f,
				// 								0
				// 								);

				// nppiThreshold_GTVal_32f_C1IR	((float*)d_Out1+i*width*height,
				// 									width * sizeof(Npp32f),
				// 									total_npp,
				// 									0,
				// 									1
				// 									);

				// nppiSum_32f_C1R((float*)d_Out1+i*width*height,
				// 			width * sizeof(Npp32f),
				// 			total_npp,
				// 			d_scratch,
				// 			pSum
				// 			);

				// //cudaStreamSynchronize(gpu->s1);
				// cudaMemcpy(&Sum, pSum, sizeof(Npp64f), cudaMemcpyDeviceToHost);
				// // if (Sum>1000)
				// // 	std::cout << "img " << (i+rashpa_bunch*link->nImgOnGpu) % nImages << " bragg peak = " << Sum << " mean " << mean_f << " std dev " << std_f << std::endl;
			 }

			// std::cout << "nppiThreshold_LTVal_32f_C1IR error output = " << int(err) << std::endl;
			// cudaPeekAtLastError();



/**
 * 
 * 
 * 

while (link->remu_state == REMU_RUNNING)
	{
		//sending h lines of w pixels from the ROI
		for (int i = 0 ; i < roi_number ; i++)
			desc[i]=compute_descriptors(i);

			rashpa_send_line_async(desc[i],h[i])

			rashpa_poll_for_completion();

		for (int i = 0 ; i < roi_number ; i++) {
			for (int lines = 0; lines < h[i] ; lines++) {
				//let compute where are the ROI bytes of line i
				data = link->source_addr 							//location of dataset
						+ (counter % nImages) * image_W * image_H	//where start next image
						+ off_y[i] * image_W 						//where top of roi start
						+ lines * image_W 							//add i lines
						+ off_x[i];									//where left of roi
				rashpa_send_line(data, w[i]);
			}
		}

		counter ++;
	}

 * 
 * 
 * /




/**
 *
 *
 * Jungfrau detector send 4k uint16_t data, from central line to the edge
 *
 */


// void* rashpa_JF_receiver_mainloop(void* arg)
// {
// 	rashpa_ctx_t* 		link 				= &g_rashpa;
// 	int 				roi_number 			= link->roi_number;

// 	int 				bdepth 				= link->bdepth ;
// 	int  	  			nImages				= link->nImages;
// 	int 				nIter				= link->nIter; 
// 	int 				gpu_w 				= link->gpu_w * bdepth ;
// 	int 				gpu_h 				= link->gpu_h;
// 	int 				rashpa_bunch		= link->rashpa_bunch ;

// 	int*				w 					= (int*)malloc(roi_number*sizeof(int));
// 	int* 				h					= (int*)malloc(roi_number*sizeof(int));
// 	int*				off_x 				= (int*)malloc(roi_number*sizeof(int));
// 	int*				off_y 				= (int*)malloc(roi_number*sizeof(int));

// 	for (int i = 0 ; i < roi_number ; i++) {
// 		w[i] 				= link->roi_w[i]* bdepth;
// 		h[i]				= link->roi_h[i];
// 		off_x[i] 			= link->roi_dst_x[i] * bdepth;
// 		off_y[i] 			= link->roi_dst_y[i];
// 		printf ("roi %d w[i] %d h[i] %d off_x[i] %d, off_y[i] %d\n",i,w[i],h[i],off_x[i],off_y[i]);
// 	}
	
// 	printf ("rashpa_Jungfrau_receiver_mainloop  nIter %d, bunch %d nImages %d link->dest_addr %p\n",
// 		nIter,rashpa_bunch, nImages,link->dest_addr );

// 	select_cpu(RASHPA_BACKEND);

// 	struct timespec startTime, endTime;
// 	int nRecvLines;
// 	nRecvLines = 0;
// 	char msg[32];
// 	strcpy(msg,RASHPA_LINK_NAME[link->link_type]);
// 	void* addr = link->dest_addr;

// 	//wait for SoF
// 	printf ("waiting for Start Of Frame\n");

// 	while (1)
// 	{
// 		rashpa_recv_line (addr, sizeof(jf_packet));
// 		if (*(float*)(((jf_packet*)addr)->data) == 1.0) break;
// 	}

// 	int nImg = 0;
// 	clock_gettime(CLOCK_MONOTONIC, &startTime);
// 	while (nImg <  nIter * nImages)
// 	{
// 		for (int i = 0 ; i < roi_number ; i++) {
// 			for (int lines = 0 ; lines < h[i] ; lines +=4) {
// 				addr = link->dest_addr 
// 						+ (nImg % (2*rashpa_bunch)) * h[i] / 4 * sizeof(jf_packet) 
// 						+ lines/4 * sizeof(jf_packet);
// 				rashpa_recv_line(addr,sizeof(jf_packet));
// 			}
// 			nRecvLines += h[i];
// 		}
// 		nImg++;
// 		//signal cuda kernel each bunch images
// 		if ((nImg % rashpa_bunch) == (rashpa_bunch - 1)) {
// 			if ( (((jf_packet*)addr)->hdr.framenum % rashpa_bunch) != (rashpa_bunch - 1) ) 
// 			{
// 				printf(A_C_RED "RECEIVER LOST PACKETS frame nImg %d framenum %d!!!!\n" A_C_RESET, nImg,((jf_packet*)addr)->hdr.framenum );
// 				strcat(msg, "_ISSUE");
// 				nIter = 0;
// 			}
					
// 			if (gpustate == GPUBUSY) printf("##################  TOO FAST ############\n");
// 			pthread_cond_signal(&link->condition);
// 		}
// 	}
	
// 	clock_gettime(CLOCK_MONOTONIC, &endTime);

// 	if (nIter) 
// 	{
// 		strcat(msg, "_OK");
// 		displayProfilingResult(msg, &endTime, &startTime);
// 	}

// 	sprintf(link->messages, "<rashpa_transfer type='TRANSFER_ISSUE' />");

// 	link->romulu_state = ROMULU_LINK_READY;
// 	return NULL;
// }




// void* rashpa_JF_detector_mainloop(void* arg)
// {
// 	rashpa_ctx_t* 		link 			= & g_rashpa;
// 	int 				roi_number 		= link->roi_number;
// 	int 				bdepth 			= link->bdepth;
// 	int 				nImages   		= link->nImages;
// 	int					image_W 		= link->hsz * bdepth;
// 	int					image_H 		= link->vsz;
// 	int*				w 				= (int*)malloc(roi_number*sizeof(int));
// 	int* 				h				= (int*)malloc(roi_number*sizeof(int));
// 	int*				off_x 			= (int*)malloc(roi_number*sizeof(int));
// 	int*				off_y 			= (int*)malloc(roi_number*sizeof(int));

// 	for (int i = 0 ; i < roi_number ; i++) 
// 	{
// 		w[i] 				= link->roi_w [i]* bdepth;
// 		h[i]				= link->roi_h[i];
// 		off_x[i] 			= link->roi_src_x[i] * bdepth;
// 		off_y[i] 			= link->roi_src_y[i];
// 		printf ("Jungfrau Fake Detector roi %d w[i] %d h[i] %d off_x[i] %d, off_y[i] %d\n",i,w[i],h[i],off_x[i],off_y[i]);
// 	}

// 	select_cpu(RASHPA_DETECTOR);

// 	int counter;
// 	void* data;
// 	counter =  0;

// 	jf_packet buffer;
// 	//put a SOF in the first image at the end of ROI (at beginning of the 4th first line, the last to be sent)
// 	data = link->source_addr + off_y[roi_number-1] * image_W + (0) * image_W + off_x[roi_number-1];
// 	*((float*)  data) = 1.0;

// 	while (link->remu_state == REMU_RUNNING)
// 	{
// 		//printf("sending %d\n",roi_number);
// 		//sending h lines of w pixels from the ROI
// 		for (int i = 0 ; i < roi_number ; i++) 
// 		{
// 			int uplines = h[i]/2, downlines = h[i]/2, lines = h[i]/2 ;
// 			int b = 0;
// 			int pkt = 0;

// 			while (lines < h[i]) 
// 			{
// 				//let compute where are the ROI bytes of line i
// 				data = link->source_addr 							//location of dataset
// 						+ (counter % nImages) * image_W * image_H	//where start next image
// 						+ off_y[i] * image_W 						//where top of roi start
// 						+ lines * image_W 							//add i lines
// 						+ off_x[i];									//where left of roi

// 				buffer.hdr.bunchid = (uint64_t) (lines/4);
// 				buffer.hdr.framenum = counter % nImages;
// 				buffer.hdr.packetnum = pkt++;
// 				memcpy((void*) ((uint64_t)&buffer + sizeof(jf_header)), data, 4*w[i]);
// 				rashpa_send_line(&buffer, sizeof(jf_packet));
// 				if (b)
// 				{
// 					uplines += 4;
// 					lines = uplines;
// 				}
// 				else
// 				{
// 					downlines -= 4;
// 					lines = downlines;
// 				}
// 				b = ! b;
// 			}
// 		}
// 		counter ++;
// 	}
// 	return NULL;
// }
