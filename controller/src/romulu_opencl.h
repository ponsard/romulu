#ifndef __ROMULU_OPENCL_H_INCLUDED__
#define __ROMULU_OPENCL_H_INCLUDED__

typedef struct _opencl_gpu_ctx {
  //	SDKDeviceInfo 	deviceInfo; 						/* SDKDeviceInfo
  //class object */
  //	cl_device_id*		devices;      						/* CL device
  //list */
  cl_context context;
  //	cl_event 			ev;
  cl_kernel kernel;
  cl_command_queue queue;

  cl_mem d_input;
  cl_mem d_input2;
  cl_mem d_output;
  cl_mem d_output2;

  char *h_aPinned;
  char *h_bPinned;
} opencl_gpu_ctx_t;

#endif
