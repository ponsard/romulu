
//int n = 0;
#include <omp.h>
void cpuapplyGainPedestalandCount(const uint16_t *data, float *outputImage, 
											float* gain, unsigned int *gCount, 
											const int Width, const int Height, 
											const int NN) 
{
		rashpa_ctx_t* link = &g_rashpa;
 
// static int SIG	= 0;

// #pragma omp parallel 
printf("cpuapplyGainPedestalandCount NN %d %d %d OMP %d\n",
	NN,Width,Height,omp_get_thread_num());

#pragma omp parallel for schedule(static) collapse(2) // shared(sCount) reduction(+: count) //

for (int ii=0;ii < 2*link->nImages*link->nIter/link->rashpa_bunch; ii=ii+2)
{	

for(int N=0;N<link->rashpa_bunch/2;N++)
{
	//to store result image
	int n= ii/2*link->rashpa_bunch/2 + N;

	//shared
	// int sCount[16];
	// int count = 0;
	//for(int j=0;j<Height;j++)

	while((*pstatusFlag) < (KERNELREADY+ii+1));


	for(int j=0;j<2048;j++)
	{	

		//busy loop waiting for RDMA data	
		// printf("busy waiting %d %d/%d\n",n,omp_get_thread_num(), omp_get_num_threads());


		// for (int i=0;i<Width;i++)
		for (int i=0;i<2048;i++)
		{
			float value		= 0.f;
			uint16_t p		= 0.f;
			uint16_t raw 	= data[i+j*Width + 2*N*Width*Height]; 
			uint16_t gn  	= raw >> 14;

			if (gn==1)
			{
				p = data[i+j*Width +(2*N+1)*Width*Height];
			}
			value = ((float) ((raw & 0b0011111111111111) - p) ) / gain[i+j*Width + gn*Width*Height];
			outputImage[i+j*Width + (n%(link->nImages/2))*Width*Height] = value ;

			// if (value>1.0f && value<6.0f) 
			// {
			// 	#pragma omp atomic
			// 	// sCount[n]++;
			// 	count ++;
			// }
		}
	// if (omp_get_thread_num()==0)
	// 	for (int i=0;i<12;i++)
	// 		gCount[n] += sCount[i];
	}
}
}
printf("cpuapplyGainPedestalandCount DONE !!!!!!!!!!!!!!!!!!!!!!!\n");
gpustate = 0;

}