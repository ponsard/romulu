

//not used anymore
__global__ void toBoolean(int Width, scan_type *bools, const float *idata) {
  int i = (blockIdx.x * blockDim.x) + threadIdx.x;
  int j = (blockIdx.y * blockDim.y) + threadIdx.y;

  bools[i + j * Width] = idata[i + j * Width] != 0.0f ? 1 : 0;
}

__global__ void scatter(int Width, int Height, void *odata, const float *idata,
                        const scan_type *bools, const scan_type *scan, int nImg,
                        unsigned int *LAST, int *peaknum) {
  int i = (blockIdx.x * blockDim.x) + threadIdx.x;
  int j = (blockIdx.y * blockDim.y) + threadIdx.y;

  unsigned int nnz = scan[Height * Width - 1];

  if ((bools[i + j * Width] == 1)) //(nnz>threshold) &&
  {
    // nnz x float csrval
    ((int *)odata)[*LAST + 1 + scan[i + j * Width] - 1] = idata[i + j * Width];
    // nnz x int indices
    ((int *)odata)[*LAST + 1 + nnz + scan[i + j * Width] - 1] = i + j * Width;
    // Height+1 x int indptr
    if (i == 0)
      ((int *)odata)[*LAST + 1 + nnz + nnz + j + 1] =
          scan[j * Width]; // CHECKME ADD last scan value
  }
  __syncthreads();
  if (i == 0 && j == 0) {
    // 1 int nnz
    ((int *)odata)[*LAST] = nnz;
    // first indptr
    ((int *)odata)[*LAST + 1 + nnz + nnz] = 0;
    //*LAST += (1 + nnz + nnz + Height + 1);
    if (*LAST > nImg * Width * Height) {
      *LAST = 0;
    }
    // extra copy of nnz to output buffer
    *peaknum = nnz;
  }
}

__global__ void createLut(char *data, char *glut, int Width, int Height) {
  unsigned int j = blockIdx.y * blockDim.y + threadIdx.y;
  unsigned int N = blockIdx.z;

  if (j < Height)
    glut[j + N * Height] =
        *(char *)(((long int)data) + 18 + j * Width + N * Height * Width);
}

__global__ void reorder(uint16_t *data, uint16_t *outputImage, char *glut,
                        int Width, int Height) {
  unsigned int i = blockIdx.x * blockDim.x + threadIdx.x;
  unsigned int j = blockIdx.y * blockDim.y + threadIdx.y;
  unsigned int N = blockIdx.z;

  if (i < Width && j < Height) {
    void *img = (void *)(((uint64_t)data) + 54 + (j + N * 128) * 8246);
    int jj = 4 * glut[j];

    // jungfrau send 4 lines at once
    outputImage[i + (jj + 0) * Width + N * Width * Height] =
        ((uint16_t *)img)[i + 0 * Width];
    outputImage[i + (jj + 1) * Width + N * Width * Height] =
        ((uint16_t *)img)[i + 1 * Width];
    outputImage[i + (jj + 2) * Width + N * Width * Height] =
        ((uint16_t *)img)[i + 2 * Width];
    outputImage[i + (jj + 3) * Width + N * Width * Height] =
        ((uint16_t *)img)[i + 3 * Width];
  }
}

//include atomic add
__global__ void applyGainPedestalandCount(const uint16_t *data,
                                          float *outputImage, float *gain,
                                          unsigned int *gCount, const int Width,
                                          const int Height, const int num,
                                          scan_type *bools) {
  //row, column
  unsigned int i = blockIdx.x * blockDim.x + threadIdx.x;
  unsigned int j = blockIdx.y * blockDim.y + threadIdx.y;
  //image in stack
  unsigned int N = blockIdx.z;
  //tid in block
  unsigned int tid = threadIdx.x + threadIdx.y * blockDim.x;

#if USESHARED
  __shared__ unsigned int count[1024];
  count[tid] = 0;
  __syncthreads();
#endif
  float value = 0.f;
  uint16_t p = 0;
  uint16_t raw = data[i + j * Width + 2 * N * Width * Height];
  // gain number : 2 MSB bits
  uint16_t gn = raw >> 14;

  if (gn == 1) {
    p = data[i + j * Width + (2 * N + 1) * Width * Height];
  }
  if (gn == 0) // should never happen...
    value = 666.0;
  else
    value = ((float)((raw & 0b0011111111111111) - p)) /
            gain[i + j * Width + gn * Width * Height];
  outputImage[i + j * Width + N * Width * Height] =
      round(value); //(float)raw;//

  //prepare for CSR compression
  bools[i + j * Width + N * Width * Height] = value != 0.0f ? 1 : 0;

#if USESHARED
  if (value != 0.0f)
    atomicInc(&count[tid], 0xffffffff);
 
  __syncthreads();

  int cnt = 0;
  if (threadIdx.x == 0 && threadIdx.y == 0) {
    for (int k = 0; k < 1024; k++)
      cnt += count[k];
    atomicAdd(&gCount[N + gridDim.z * num], cnt);
  }
#endif
};

// debug version
__global__ void applyGainPedestalandCount2(const uint16_t *data,
                                           float *outputImage, float *gain,
                                           unsigned int *gCount,
                                           const int Width, const int Height,
                                           const int num) {
  unsigned int i = blockIdx.x * blockDim.x + threadIdx.x;
  unsigned int j = blockIdx.y * blockDim.y + threadIdx.y;
  unsigned int N = blockIdx.z;

  outputImage[i + j * Width + N * Width * Height] =
      (float)data[i + j * Width + (2 * N) * Width * Height];
};

#if SINGLE
__global__ void
singleKernel(const uint16_t *data, float *outputImage, float *gain,
             unsigned int *gCount, const int Width, const int Height, int k_w,
             int k_h, int k, int nImages, unsigned int *L, int num_groups,
             int interval_size, int out_imgSize, float *d_Out,
             scan_type *d_scan, scan_type *d_bools, scan_type *dummy_results,
             scan_type *block_results, float *d_csrval, int *peaknum) {
  // if (threadIdx.x == 0 && threadIdx.y == 0) {
    // toBoolean<<<dim3(k_w / 64, k_h / 16, 1), dim3(64, 16, 1)>>>(
    //     k_w, d_boolsb, (float *)d_OutB + k_w * k_h * k);

    // __syncthreads();

for (int kk=0;kk<5;kk++)
{
  scan_scan_intervals<<<dim3(num_groups, 1), dim3(WG_SIZE, 1, 1)>>>(
  d_bools,// + k/4 * k_w * k_h, //CHECKME HERE
  out_imgSize,interval_size, d_scan, block_results);

  __syncthreads();

  scan_scan_intervals<<<dim3(1, 1), dim3(WG_SIZE, 1, 1)>>>(
  block_results, num_groups, interval_size, block_results,
  dummy_results);

  __syncthreads();

  scan_final_update<<<dim3(num_groups, 1), dim3(WG_SIZE2, 1, 1)>>>(
  d_scan, out_imgSize, interval_size, block_results);

  __syncthreads();

  scatter<<<dim3(k_w / 64, k_h / 16, 1), dim3(64, 16, 1)>>>(
  k_w, k_h, d_csrval, (float *)d_Out ,//+ k_w * k_h * k,
  d_bools,// + k * k_w * k_h, 
  d_scan, nImages, L,
  peaknum);
  __syncthreads();
}


}
#endif