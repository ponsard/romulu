#!/bin/bash
#~ cd romulu/controller/src

#~ source /segfs/deg/bin/degprofile.sh
RASHPA_DIR=/home/ponsard/

#change require in Makefile line 26
LIBVMA=$VMA
NOPROF=$PROF
CUDA=1
#~ make clean
#~ rm romulu_opencl.o romulu_nvidia.o $RASHPA_DIR/librashpa/src/*.o /tmp/controller

if [ "$CUDA" = "1" ]; then
echo "cuda defined"
#~ make  devel 
else
echo "cuda NOT defined"
#~ make devel
fi

#~ make 


g++ romulu_opencl.cpp -o romulu_opencl.o -c -I $RASHPA_DIR/librashpa/include/

LLIBS=" -lepci -lpci -lefpak -lespi -lz -ledma -luirq -lebuf -lpmem  \
-lm -ldl -lrt -lpthread -libverbs -lrdmacm -lxml2 -lzmq -lOpenCL -lnuma"

if [ "$CUDA" = "1" ]; then
echo "cuda defined"

#/usr/local/cuda/bin/
COMP="nvcc -ccbin g++ -m64  \
-gencode arch=compute_70,code=sm_70  \
-DCUDA=1 -rdc=true"

$COMP -c -o romulu_nvidia.o romulu_nvidia.cu -I $RASHPA_DIR/librashpa/include
 #~ -Xcompiler -fopenmp 
#$COMP -c -o romulu_nvidia.s romulu_nvidia.cu -I ~/librashpa/include -Xcompiler -fopenmp -Xcompiler -S

LLIBS=$LLIBS" -lcuda -lcudart "
# "-lnppist -lnppitc -lnppial -lnppisu -lnppc -lcusparse_static -lcublas_static -lculibos"
#echo $LLIBS
echo "using CUDA compiler "$COMP
else
COMP="g++-6" 
fi


$COMP  -Xcompiler -fopenmp \
-o /tmp/controller  romulu_opencl.o newController.o romulu_nvidia.o \
romulu_rsocket.o \
$RASHPA_DIR/librashpa/src/rashpa_main.o $RASHPA_DIR/librashpa/src/rashpa_udp.o \
$RASHPA_DIR/librashpa/src/rashpa_xml.o  $RASHPA_DIR/librashpa/src/rashpa_init.o \
$RASHPA_DIR/librashpa/src/rashpa_roce_uc.o $RASHPA_DIR/librashpa/src/rashpa_zmq.o \
 romulu_generic.o  \
-L /usr/lib/x86_64-linux-gnu/   \
-L $RASHPA_DIR/romulu/controller/lib/libepci/src/ \
-L $RASHPA_DIR/romulu/controller/src/../lib/libefpak/src \
-L $RASHPA_DIR/romulu/controller/src/../lib/libespi/src \
-L $RASHPA_DIR/romulu/controller/src/../lib/libedma/src \
-L $RASHPA_DIR/romulu/controller/src/../lib/libebuf/src \
-L $RASHPA_DIR/romulu/controller/src/../lib/libuirq/src/u \
-L $RASHPA_DIR/romulu/controller/src/../lib/libpmem/src/u \
-L $RASHPA_DIR/tcc-0.9.27 -ltcc  -L $RASHPA_DIR $LLIBS

#sudo setcap "cap_dac_override+ep cap_sys_rawio+ep" /tmp/controller


echo 'ROMULU DAnCE Controller.....' 

if [ "$LIBVMA" = "1" ]; then
echo "using libVMA Messaging Accelerator... Incompatibility with RoCE"

VMA_RX_BYTES_MIN=100000000 LD_PRELOAD=/usr/lib/libvma.so VMA_RX_BUFS=2000000 VMA_RX_PREFETCH_BYTES=4096 VMA_RX_CQ_DRAIN_RATE_NSEC=1 VMA_SELECT_POLL=-1 VMA_INTERNAL_THREAD_AFFINITY=3,7,11 VMA_TRACELEVEL=info \
LD_LIBRARY_PATH=$RASHPA_DIR/romulu/controller/lib/libdance/src/:$RASHPA_DIR/romulu/controller/lib/libepci/src/:../lib/libefpak/src/:../lib/libespi/src/:../lib/libedma/src/:../lib/libuirq/src/u/:../lib/libebuf/src/:../lib/libpmem/src/u/:../../../tcc-0.9.27/ \
/tmp/controller -lAPP 
else
if [ "$NOPROF" = "0" ]; then
LD_LIBRARY_PATH=../../../gdrcopy:../../../romulu/controller/lib/libdance/src/:../../../romulu/controller/lib/libepci/src/:../lib/libefpak/src/:../lib/libespi/src/:../lib/libedma/src/:../lib/libuirq/src/u/:../lib/libebuf/src/:../lib/libpmem/src/u/:../../../tcc-0.9.27/ \
/tmp/controller -lAPP 
else
LD_LIBRARY_PATH=../../../gdrcopy:../../../romulu/controller/lib/libdance/src/:../../../romulu/controller/lib/libepci/src/:../lib/libefpak/src/:../lib/libespi/src/:../lib/libedma/src/:../lib/libuirq/src/u/:../lib/libebuf/src/:../lib/libpmem/src/u/:../../../tcc-0.9.27/ \
nvprof -f -o /tmp/log_romulu /tmp/controller -lAPP
fi
fi
echo $PRE

#ok for power9
#nvcc newController.o    romulu_generic.o  romulu_nvidia.o  romulu_opencl.o  romulu_rsocket.o ../../../librashpa/src/rashpa_init.o ../../../librashpa/src/rashpa_udp.o ../../../librashpa/src/rashpa_xml.o ../../../librashpa/src/rashpa_zmq.o  ../../../librashpa/src/rashpa_main.o ../../../librashpa/src/rashpa_roce_uc.o -o /tmp/controller -libverbs -lpthread -lcuda -lcudart -lOpenCL -lzmq -lxml2 -lrdmacm
 

#nvprof -f -o /tmp/log_romulu 
#cuda-memcheck  
#/users/ponsard/nv-nsight-cu-cli 
