#include <arpa/inet.h>
#include <fcntl.h>
#include <malloc.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <infiniband/verbs.h>

#include "librashpa.h"
#include "romulu_gdrcopy.h"

void *map_nvidia_gpu_memory(int totalSize, void *gpu_addr) {

  int fd_mem = open("/dev/mem", O_RDWR | O_SYNC);
  if (fd_mem == -1) {
    printf(
        "accessing /dev/mem requires cap_dac_override+ep cap_sys_rawio+ep %p\n",
        gpu_addr);
    return NULL;
  }

  // int copy_size = _size;
  size_t size = (totalSize + GPU_PAGE_SIZE - 1) & GPU_PAGE_MASK;
  size_t rounded_size = (size + PAGE_SIZE - 1) & PAGE_MASK;
  // TODO Check this rounded_size
  printf("mapping gpu at %p len %ld\n", gpu_addr, (uint64_t)rounded_size);
  void *bar_ptr = mmap(NULL, rounded_size, PROT_WRITE, MAP_SHARED, fd_mem,
                       (uint64_t)gpu_addr); // PROT_READ
  if (bar_ptr == MAP_FAILED) {
    printf("ERROR OPENING GPU MEMORY REGION %p len %ld\n", gpu_addr,
           rounded_size);
    return NULL;
  }
  return bar_ptr;
}

// GPU local ; REMU/ROMULU same host
void *local_nvidia_gdrcopy_backend_receiver(void *arg) {
  //~ uint32_t port 		= link->port;
  rashpa_ctx_t *link = (rashpa_ctx_t *)arg;
  void *src_addr = link->source_addr;
  void *gpu_addr = link->dest_addr;
  uint32_t totalSize = link->dsSize;
  pid_t pid_romulu = link->computeProcessPid;
  pid_t nImages = link->nImages;

  int dataSize = totalSize / nImages;
  void *bar_ptr = map_nvidia_gpu_memory(totalSize, gpu_addr);

  int i;
  for (i = 0; i < nImages; i++) {
    gdr_copy_to_bar(bar_ptr, src_addr + i * dataSize, dataSize);
    kill(pid_romulu, SIGUSR1);
  }
  return NULL;
}
