/*---------------------------------------------------------------------------
 * ESRF -- The European Synchrotron
 *         Instrumentation Services and Development Division
 *
 * Project: ROMULU, a RASHPA GPU Receiver
 *
 *
 * $URL: $
 * $Rev: $
 * $Date: $
 *------------------------------------------------------------------------- */

#include "../../testclient/gdrcopy/gdrapi.h"
// #include "dancemain.h"
#include "rashpa_init.h"
#include "rashpa_zmq.h"
#include "romulu.h"
#include "romulu_rsocket.h"

#include <arpa/inet.h>
#include <fcntl.h>
#include <netdb.h>
#include <netinet/in.h>
#include <pthread.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <unistd.h>

#include "librashpa.h"
#include "rashpa_main.h"
#include "rashpa_roce_uc.h"
#include "romulu_gdrcopy.h"
#include "romulu_rsocket.h"

#define ROMULUPORT                5000
#define DANCE_OK                  0
#define DEBUG                     0
#define LOG_APP                   if (DEBUG) printf

static char data[RASHPA_TELEGRAM_MSG_SIZE];
int sock; //socket for telegram
struct sockaddr_in addr,addr2;


extern rashpa_ctx_t g_rashpa;
extern roce_ctx_t g_roce;
extern void init_gpu_device();
extern void allocate_gpu_device_memory();


romulu_globals_t *g_romulu;
static int romulu_init(romulu_globals_t *romulu) {
  g_romulu = romulu;

  rashpa_ctx_t *link = &g_rashpa;
  link->romulu_state = ROMULU_IDLE;
  link->roi_number = 0;
  // select_cpu(RASHPA_BACKENDGPU);
  set_cpu(1);

  return DANCE_OK;
}

// void set_romulu_process_pid(rashpa_ctx_t *link, char *pid_name) {
//   int f = open(pid_name, O_RDONLY);
//   if (f < 0) {
//     LOG_APP("open error %s\n", pid_name);
//     return;
//   }
//   read(f, &link->computeProcessPid, 4);
//   close(f);
// }
void exec_qRESULT(void) {
  rashpa_ctx_t *link = &g_rashpa;
  // LOG_APP("qRESULT %s\n",link->messages);
  //CHECKME
  // answerf("<rashpa>  %s </rashpa>\n", link->messages);
}

// return rashpa telegram with qpn
void romulu_qrashpaconf(void) {
  roce_ctx_t *roce = &g_roce;
  rashpa_ctx_t *link = &g_rashpa;
  int roi_number = link->roi_number;

  char tlgram[1024];
  char buf[256];

  LOG_APP("in qbRASHPACONF %s %d\n", RASHPA_LINK_NAME[link->link_type],
          link->romulu_state);

  if (link->romulu_state < ROMULU_LINK_READY ||
      link->link_type != RASHPA_LINK_ROCEV2_UC) {
    LOG_APP("LINK qpn not available\n");
    sprintf(tlgram,
            "<rashpa_telegram> not_yet_configured </rashpa_telegram>\n");
    goto send;
  }
  sprintf(tlgram, "<rashpa_telegram>\n");
  for (int i = 0; i < roi_number; i++) {
    sprintf(buf, "<rashpa_link id='%d' qpn='%d' roi_number='%d' rkey='%x' "
                 "daddr='%p'/>",
            i, roce->qp[i]->qp_num, link->roi_number, roce->rkey[i],
            roce->daddr[i]);
    strcat(tlgram, buf);
  }
  strcat(tlgram, "</rashpa_telegram>\n");
send:
  
  //wait a little bit to start receiver
  usleep(1000000);
  int n=sendto(sock, tlgram, strlen(tlgram), 0, (struct sockaddr *)(&addr2),
           sizeof(struct sockaddr_in));
  LOG_APP("sendto %s socket %d len %d status %d %s\n",
      tlgram,sock,strlen(tlgram),n,inet_ntoa(addr2.sin_addr));

}

extern int process_rashpa_telegram_for_romulu(char *rashpa_manager_request,
                                              rashpa_ctx_t *link,
                                              roce_ctx_t *roce);
// parse incoming xml rashpa telegram	and update link
void romulu_rashpaconf(void) {
  rashpa_ctx_t *link = &g_rashpa;
  roce_ctx_t *roce = &g_roce;
  
  int size =0; 
  char* buffer;

  if (link->romulu_state != ROMULU_IDLE) {
    LOG_APP("ALREADY CONFIGURED, STATE IS %s\n",
            ROMULU_STATE[link->romulu_state]);
    return;
  }

  if (process_rashpa_telegram_for_romulu(data+10, link, roce) !=
      RASHPA_XML_OK) {
    printf("Error processing xml\n");
    return;
  }

  link->romulu_state = ROMULU_RASHPA_CONFIGURED;
}



// start possible only if in idle state
void romulu_startlink() {
  rashpa_ctx_t *link = &g_rashpa;

  LOG_APP("romulu_startlink\n");

  // start data receiver, only one for now
  init_gpu_device();

  // must be done before rashpa_rocev2_ud_backend_receiver_init
  allocate_gpu_device_memory();

  LOG_APP("before calling rashpa_rocev2_uc_backend_receiver_init %d\n",
          link->link_type);

  if (link->link_type == RASHPA_LINK_ROCEV2_UC) {
    LOG_APP("calling rashpa_rocev2_uc_backend_receiver_init\n");
    for (int i = 0; i < link->roi_number; i++)
      rashpa_rocev2_uc_backend_receiver_init(i);
    link->romulu_state = ROMULU_LINK_READY;
  }

  if (link->link_type == RASHPA_LINK_ZMQ ||
      link->link_type == RASHPA_LINK_UDP ||
      link->link_type == RASHPA_LINK_UDP_JF) {
    LOG_APP("calling rashpa_init\n");
    rashpa_init(RASHPA_BACKEND);
    link->romulu_state = ROMULU_LINK_READY;
  }
}
#ifdef CUDA
extern void *cuda_gpu_compute_mainloop(void *data);
#endif
extern void *opencl_gpu_compute_mainloop(void *data);

void romulu_startgpu() {
  LOG_APP("romulu_startgpu\n");
  rashpa_ctx_t *link = &g_rashpa;
  int arg = 0;

  if (link->receiver_type == RASHPA_RECEIVER_NVIDIA_OPENCL_GPU &&
      pthread_create(&link->gpu_tid, NULL, opencl_gpu_compute_mainloop,
                     (void *)&arg)) {
    printf("ERROR launching thread opencl_gpu_compute_mainloop\n");
    return;
  }
#ifdef CUDA
  if ((link->receiver_type == RASHPA_RECEIVER_NVIDIA_CUDA_GPU ||
       link->receiver_type == RASHPA_RECEIVER_NVIDIA_CUDAMEM_GPU) &&
      pthread_create(&link->gpu_tid, NULL, cuda_gpu_compute_mainloop,
                     (void *)&arg)) {
    printf("ERROR launching thread launch_gpu_compute\n");
    return;
  }
#endif CUDA
  link->romulu_state = ROMULU_GPU_READY;
}

#ifdef CUDA
extern void release_nvidia_gpu_context();
#endif

void romulu_stop() {
  LOG_APP("romulu_stop\n");
  rashpa_ctx_t *link = &g_rashpa;

  // may have to kill thread ?
  link->romulu_state = ROMULU_IDLE;

  if (link->link_type == RASHPA_LINK_ROCEV2_UC)
    ;
  // for (int i=link->roi_number-1 ; i>=0 ; i--)
  // 	release_ib_context(RELEASE_RESET,i);
  else
    rashpa_fini(RASHPA_BACKEND);

  LOG_APP("release_gpu_context\n");
  release_gpu_context();
}

int roi_ids[64];

void romulu_startacq() {
  rashpa_ctx_t *link = &g_rashpa;
  LOG_APP("rashpa_receiver_mainloop\n");

  if (link->link_type == RASHPA_LINK_ROCEV2_UC) {
    if (pthread_create(&link->rashpa_tid, NULL,
                       rashpa_rocev2_uc_backend_receiver_mainloop_single_thread,
                       (void *)&roi_ids[0])) {
      LOG_APP("ERROR launching "
              "rashpa_rocev2_uc_backend_receiver_mainloop_single_thread\n");
      return;
    }
  }

  if (link->link_type == RASHPA_LINK_ZMQ ||
      link->link_type == RASHPA_LINK_UDP ||
      link->link_type == RASHPA_LINK_UDP_JF) {
    int arg = 0;
    if (pthread_create(&link->rashpa_tid, NULL, rashpa_receiver_mainloop,
                       (void *)&arg)) {
      LOG_APP("ERROR launching thread rashpa_receiver_mainloop\n");
      return;
    }
  }
}

void release_gpu_context() {
  rashpa_ctx_t *link = &g_rashpa;
#ifdef CUDA
  if (link->receiver_type == RASHPA_RECEIVER_NVIDIA_CUDA_GPU ||
      (link->receiver_type == RASHPA_RECEIVER_NVIDIA_CUDAMEM_GPU))
    release_nvidia_gpu_context();
#endif
  if (link->receiver_type == RASHPA_RECEIVER_NVIDIA_OPENCL_GPU)
    release_opencl_gpu_context();
}

void romulu_reset() {
  //~ int romuluId = 0;
  rashpa_ctx_t *link = &g_rashpa;
  printf("romulu_reset state %d\n", link->romulu_state);
  // if (link->romulu_state >= ROMULU_GPU_READY)
  release_gpu_context();

  if (link->romulu_state >= ROMULU_LINK_READY) {
    switch (link->link_type) {
    case RASHPA_LINK_ROCEV2_UC:
      release_ib_context(RELEASE_IBV_POLL, 0);
      break;

    case RASHPA_LINK_UDP:
    case RASHPA_LINK_ZMQ:
      rashpa_zmq_fini(RASHPA_BACKEND);
      break;
    }
  }
  // terminate thread if any ...
  link->romulu_state = ROMULU_IDLE;
  exit(0);
}

int I_NAMED_TAG(char*s)
{
  int n=strlen(s);
  return strncmp(s, data,n)==0;
}

void exec_ROMULU(void) {
  rashpa_ctx_t *link = &g_rashpa;
  LOG_APP("now in exec_ROMULU %s\n", ROMULU_STATE[link->romulu_state]);

  switch (link->romulu_state) {
  case ROMULU_IDLE:

    if (I_NAMED_TAG("RASHPACONF"))
      romulu_rashpaconf();
    break;
    if (I_NAMED_TAG("RESET"))
      romulu_reset();
    break;

  case ROMULU_RASHPA_CONFIGURED:

    if (I_NAMED_TAG("STARTLINK"))
      romulu_startlink();
    
    if (I_NAMED_TAG("STOP"))
      romulu_stop();
    if (I_NAMED_TAG("RESET"))
      romulu_reset();
    break;

  case ROMULU_LINK_READY:

    if (I_NAMED_TAG("STARTGPU"))
      romulu_startgpu();
    if (I_NAMED_TAG("?RASHPACONF"))
      romulu_qrashpaconf();
    if (I_NAMED_TAG("STOP"))
      romulu_stop();
    if (I_NAMED_TAG("RESET"))
      romulu_reset();
    break;

  case ROMULU_GPU_READY:

    if (I_NAMED_TAG("STARTACQ"))
      romulu_startacq();
    if (I_NAMED_TAG("?RASHPACONF"))
      romulu_qrashpaconf();
    break;
    if (I_NAMED_TAG("STOP"))
      romulu_stop();
    if (I_NAMED_TAG("RESET"))
      romulu_reset();
    break;

  case ROMULU_RUNNING:

    if (I_NAMED_TAG("STOP"))
      romulu_stop();
    if (I_NAMED_TAG("RESET"))
      romulu_reset();
    break;
  }
}

int main()
{
static romulu_globals_t romulu_globals;
rashpa_ctx_t *link = &g_rashpa;

printf("romulu controller main() version %s %s\n",__DATE__,__TIME__);

int r;
sock = socket(AF_INET, SOCK_DGRAM, 0);
if (sock == -1)
  printf("socket() error\n");

addr.sin_family = AF_INET;
addr.sin_port = htons(ROMULUPORT);
addr.sin_addr.s_addr = INADDR_ANY;
  //  if (setsockopt(new_fd, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int)) == -1)
  //  {
  //     SYSTEM_ERROR();
  //     goto on_error_1;
  //  }
int optval = 1;
setsockopt(sock, SOL_SOCKET, SO_REUSEPORT, &optval, sizeof(optval));

r = bind(sock, (struct sockaddr *)(&addr), sizeof(struct sockaddr));
if (r<0){
  LOG_APP("bind error.\n");
  return -1;
}

if (romulu_init(&romulu_globals) != DANCE_OK) {
  LOG_APP("Initialisation of REMU failed.\n");
  return -1;
}
int len;
while (1)  //CHECKME link->romulu_state > ROMULU_IDLE)
{
  // printf("recvfrom\n");
  r=recvfrom(sock, data, RASHPA_TELEGRAM_MSG_SIZE, 0, (struct sockaddr*)&addr2, &len);
  if (r==-1)
    printf("recvfrom error %s %d\n",inet_ntoa(addr2.sin_addr),r);
  exec_ROMULU();
}


return 0;
}


