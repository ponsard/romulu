#include <stdint.h>
#include <stdlib.h>

#include <math.h>
#include <stdio.h>

#include <fcntl.h>
#include <string.h>
#include <unistd.h>

#include "librashpa.h"

#include "rashpa_init.h"
#include "romulu_generic.h"

#include "rashpa_roce_uc.h"

extern void init_nvidia_opencl_gpu_device();
extern void init_nvidia_cuda_gpu_device();
// extern void allocate_gpu_nvidia_memory();

void init_gpu_device() {
  debug_printf("init_gpu_device generic\n");

  rashpa_ctx_t *link = &g_rashpa;
  switch (link->receiver_type) {

#ifdef CUDA
  case RASHPA_RECEIVER_NVIDIA_CUDA_GPU:
  case RASHPA_RECEIVER_NVIDIA_CUDAMEM_GPU:
    init_nvidia_cuda_gpu_device();
    break;

  case RASHPA_RECEIVER_NVIDIA_OPENCL_GPU:
    init_nvidia_opencl_gpu_device();
    break;
#endif
  case RASHPA_RECEIVER_AMD_GPU:

    break;
  case RASHPA_RECEIVER_OPENCL_XEON:

    //		init_nvidia_opencl_xeon_device();

    break;
  }
}

void allocate_gpu_device_memory() {
  debug_printf("allocate_gpu_device_memory generic\n");

  rashpa_ctx_t *link = &g_rashpa;
  switch (link->receiver_type) {

#ifdef CUDA
  case RASHPA_RECEIVER_NVIDIA_CUDA_GPU:
  case RASHPA_RECEIVER_NVIDIA_CUDAMEM_GPU:
    allocate_gpu_nvidia_memory();
    break;
#endif
  case RASHPA_RECEIVER_NVIDIA_OPENCL_GPU:
    // allocate_gpu_nvidia_opencl_memory();
    break;
  case RASHPA_RECEIVER_OPENCL_XEON:
    //		allocate_gpu_opencl_xeon();
    //		allocate_gpu_opencl_amd();
    break;
  }
}