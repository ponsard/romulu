////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// __device__ volatile int blkcnt1 = 0;
// __device__ volatile int blkcnt2 = 0;
// __device__ volatile int itercnt = 0;

// __global__ void spinningGainPedestal(	uint16_t *data, float *outputImage, 
// 										float* gain,unsigned int *gCount,
// 										const int Width, const int Height,
// 										const int offset_in, 
// 										volatile int *buffer1_ready,volatile int *buffer2_ready,
// 										const int iterations) 
// {
// 	// unsigned int i = blockIdx.x * blockDim.x + threadIdx.x;
// 	// unsigned int j = blockIdx.y * blockDim.y + threadIdx.y;
// 	// unsigned int N = blockIdx.z; 

// 	const int buffersize=Width*Height; 
// 	// int nImage=0,offset=0;

// 	int idx = threadIdx.x + blockDim.x*blockIdx.x;
//   	int iter_count = 0;
// 	while (iter_count < iterations)
// 	{	
// 		uint16_t *data_ = (iter_count & 1)? data+offset_in:data; // ping pong between buffers
// 		volatile int *bufrdy = (iter_count & 1)?(buffer2_ready):(buffer1_ready);
// 		volatile int *blkcnt = (iter_count & 1)?(&blkcnt2):(&blkcnt1);
// 		int my_idx = idx;
		
// 		while (iter_count - itercnt > 1); // don't overrun buffers on device
// 		while (*bufrdy == 2);  // wait for buffer to be consumed
// 		while (my_idx < buffersize)
// 		{ 	// perform the "work"
// 			//my_compute_function(buf, my_idx, iter_count);
// 			float value		= 0.f;
// 			uint16_t p		= 0.f;
// 			uint16_t raw 	= data_[idx+2*iter_count*Width*Height]; 
// 			uint16_t gn  	= raw >> 14;

// 			if (gn==1)
// 			{
// 				p = data_[idx + (2*iter_count+1)*Width*Height];//
// 			}
// 			value = ((float) ((raw & 0b0011111111111111) - p) ) / gain[idx + gn*Width*Height];
// 			outputImage[idx] = value ;
// 			if (value>4.0f && value<6.0f ) //i < Width && j < Height  && 
// 				atomicInc(&gCount[iter_count], 0xffffffff);
									
// 			my_idx += gridDim.x*blockDim.x; // grid-striding loop //
// 			//CHECK THIS

// 		}
// 		__syncthreads(); // wait for my block to finish
// 		__threadfence(); // make sure global buffer writes are "visible"
// 		if (!threadIdx.x) atomicAdd((int *)blkcnt, 1); // mark my block done
// 		if (!idx)
// 		{ 	// am I the master block/thread?
// 			while (*blkcnt < gridDim.x);  // wait for all blocks to finish
// 			*blkcnt = 0;
// 			*bufrdy = 2;  // indicate that buffer is ready
// 			__threadfence_system(); // push it out to mapped memory
// 			itercnt++;
// 		}

// 		iter_count++;
// 	}
// };
