


__device__ volatile int blockcounter1 = 0;
__device__ volatile int blockcounter2 = 0;

__global__ void mydoubleBuffer2D(float* inputImage, float *outputImage, int Width, int Height,
		volatile int* bufferstate1, volatile int* bufferstate2, 
		volatile int* terminateFlag, volatile int* thread_terminated) 
{
	unsigned int i = blockIdx.x * blockDim.x + threadIdx.x, ii;
	unsigned int j = blockIdx.y * blockDim.y + threadIdx.y, jj;
	unsigned int N = blockIdx.z;

	ii = j;
	jj = Width - 1 - i;

	// int transformID = blockIdx.y;
	// int eventID = blockIdx.x * blockDim.x + threadIdx.x;
	// int outIdx = transformID * DATA_BUFFER_SIZE + eventID;

	//if (eventID < DATA_BUFFER_SIZE) 
	if (i < Width && j < Height && ii < Height && jj < Width) 
	{
		int iteration = 1;
		while (*terminateFlag != 1) {
			volatile int * state = (iteration % 2 == 0) ? bufferstate1 : bufferstate2;
			//int * buffer = (iteration % 2 == 0) ? buffer1 : buffer2;
			inputImage = (iteration % 2 == 0) ? inputImage : inputImage+16*Width*Height;
			volatile int * blockcounter = (iteration % 2 == 0) ? &blockcounter1 : &blockcounter2;

			while (*state != iteration)
				if (*terminateFlag == 1)
					return; //Wait until the host releases the buffer
			//Do something ======
			//outputList[outIdx] = iteration;
			outputImage[ii + jj * Height + N * Width * Height] = (float) inputImage[i + j * Width + N * Width * Height];
			if (i==0 && j==0) 
				printf("in spinning kernel N %d state %d bcounter %d (%d %d)\n",N, *state, *blockcounter,i,j);
			//Finish doing something =====

			__syncthreads(); // wait for my block to finish
			if (!threadIdx.x)
				atomicAdd((int *) blockcounter, 1); //Mark my block as finished
			__threadfence(); //make sure everyone can see my atomicAdd before proceeding

			if (i == 0 && j == 0) { // I'm the master thread!
				while (*blockcounter < 48)
					; //Wait for everyone to be done.
				*thread_terminated=iteration;
				*blockcounter = 0;
				*state = 0; //Release the buffer back to the host
			}
			iteration++;
		}
	}

}
// template<typename T1, typename T2>
// __global__ void rightRotation_resident(T1 *inputImage, T2 *outputImage,int Width, int Height) 
// {
// 	unsigned int i = blockIdx.x * blockDim.x + threadIdx.x, ii;
// 	unsigned int j = blockIdx.y * blockDim.y + threadIdx.y, jj;
// 	unsigned int N = blockIdx.z;

// 	ii = j;
// 	jj = Width - 1 - i;

// 	if (i < Width && j < Height && ii < Height && jj < Width) 
// 	{
// 		int iteration = 1;
// 		while (*terminateFlag != 1) {
// 			//volatile int * state = (iteration % 2 == 0) ? bufferstate1 : bufferstate2;
// 			float * buffer = (iteration % 2 == 0) ? inputImage : inputImage+N*Width*Height;
// 			//volatile int * blockcounter = (iteration % 2 == 0) ? &blockcounter1 : &blockcounter2;

// 			while (*state != iteration)
// 				if (*terminateFlag == 1)
// 					return; //Wait until the host releases the buffer
// 			//Do something ======
// 			outputImage[ii + jj * Height + N * Width * Height] = (T2) inputImage[i + j * Width + N * Width * Height];
// 			//Finish doing something =====

// 			__syncthreads(); // wait for my block to finish
// 			if (!threadIdx.x)
// 				atomicAdd((int *) blockcounter, 1); //Mark my block as finished
// 			__threadfence(); //make sure everyone can see my atomicAdd before proceeding
// 			if (transformID == 0 && eventID == 0) { // I'm the master thread!
// 				while (*blockcounter < NUM_TRANSFORMS)
// 					; //Wait for everyone to be done.
// 				*thread_terminated=iteration;
// 				*blockcounter = 0;
// 				*state = 0; //Release the buffer back to the host
// 			}
// 			iteration++;
// 		}



// 	}
// }
void spinning_kernel_test()
{

	volatile int * outputBuffer1_state; //when the state=0, the host is claiming the buffer, if state=1 the device is claiming the buffer, if state=-1 the buffer is released
	volatile int * outputBuffer2_state;
	volatile int * terminate_thread;
	volatile int * thread_terminated;
	cudaHostAlloc(&outputBuffer1_state, sizeof(int), cudaHostAllocMapped);
	cudaHostAlloc(&outputBuffer2_state, sizeof(int), cudaHostAllocMapped);
	cudaHostAlloc(&terminate_thread, sizeof(int), cudaHostAllocMapped);
	cudaHostAlloc(&thread_terminated, sizeof(int), cudaHostAllocMapped);
	*outputBuffer1_state = 0;
	*outputBuffer2_state = 0;
	*terminate_thread = -1;
	*thread_terminated=0;

	printf("loo reentrent kernel\n");
	mydoubleBuffer2D<<<dimGrid0, dimBlock0, 0, gpu->s1>>>((float*)link->dest_addr,d_Out1 ,width,height,
			outputBuffer1_state, outputBuffer2_state,
			terminate_thread, thread_terminated);
int iteration=0;
for (int i = 0; i < 5; i++)  //6 x 16 = 96
{
		printf("========== Iteration %d ==========\n", i);
		//int * output_buffer = (i % 2 == 0) ? d_buffer1 : d_buffer2;
		volatile int * buffer_state = (i % 2 == 0) ? outputBuffer1_state : outputBuffer2_state;
		if (i % 2 == 0) printf("Buffer=buffer1, state=%d \n", *buffer_state);
		if (i % 2 == 1) printf("Buffer=buffer2, state=%d \n", *buffer_state);
		printf(" Waiting for release...");
		while (*buffer_state != 0) ;//printf(" Waiting for release...buffer_state %d",*buffer_state); //wait for the device to release the buffer
		printf("\n Released! Buffer state = %d", *buffer_state);
		printf("\n Copying memory...");
		// cudaMemcpyAsync(output_buffer, &data[0],
		// 		sizeof(int) * DATA_BUFFER_SIZE * EVENT_DIMENSION,
		// 		cudaMemcpyHostToDevice, streamc);
		printf("\n Waiting for copy...");
		
		//cudaStreamSynchronize(streamc); //Wait for the copy to be done
		gpustate = GPUIDLE;
		pthread_cond_wait(&link->condition, &link->mutex);
		//gpustate = GPUBUSY;

		*buffer_state = i; //Release the buffer to the device
		printf("\n Releasing Buffer...\n Buffer state = %d \n", *buffer_state);
		printf("val i=%d \n", i);
		iteration=i;
}
	while(*thread_terminated!=iteration);
	*terminate_thread = 1; //release the device
	cudaStreamSynchronize(gpu->s1);
	// cudaMemcpy(&h_outputList[0], d_outputList,
	// 		sizeof(int) * NUM_TRANSFORMS * DATA_BUFFER_SIZE,
	// 		cudaMemcpyDeviceToHost);
	cudaDeviceSynchronize();	


printf("========== end loop\n");



}