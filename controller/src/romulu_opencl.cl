/*
 * 
 * 
 * 
 */											
#pragma OPENCL EXTENSION cl_amd_printf : enable	
//#pragma OPENCL EXTENSION cl_intel_printf :enable
								                      		
__kernel void myrotate(__global unsigned short int*	inputImage,                    
                       __global float*	outputImage,                   
					    int 	w,                      
					    int 	h)                    	                   	
{                                      		
	int i = get_global_id(0),ii;
	int j = get_global_id(1),jj; 
	int N = get_global_id(2); 

	ii = h-1 - j;
	jj = i; 

	if (i < w && j < h && ii < h && jj < w) {
		outputImage[ii + h * jj + N * w * h] = (float) inputImage[i + w * j + N * w * h]; 
	}
}

__kernel void myflip(__global float *inputImage,                    
                     __global float *outputImage,                   
                     int w,                      
                     int h)                    	
{                                      		
	int i = get_global_id(0),ii;
	int j = get_global_id(1),jj; 

	ii = i;
	jj = h-1 - j;

	if (i < w && j < h && ii < w && jj < h) {
		outputImage[ii + h*jj]= inputImage[i + w*j]; 
	}
}
 