/*
 * DEPRECATED CODE
 * *
 * *
 * first CUDA kernel (PI/2 rotation)
 * 
 * 	- pin gpu memory to pcie bar
 * 	- stream synchronization
 * 
 * 
Getting Started :
sudo modprobe nvidia-current-uvm
sudo insmod gdrdrv/gdrdrv.ko
sudo dmesg
[233787.225525] gdrdrv:device registered with major number 242
sudo mknod /dev/gdrdrv c 242 0
sudo chmod a+rw /dev/gdrdrv
sudo setcap "cap_dac_override+ep cap_sys_rawio+ep" ./romulu_gpudirect

howto build static libgdrapi (CAP_SYS_RAWIO) :
ar rcs libgdrapi.a memcpy_*o gdrapi.o

howto compile CUDA code :

/usr/local/cuda-8.0/bin/nvcc -ccbin g++-4.4 -m64 \
 -I ~/NVIDIA_CUDA-8.0_Samples/common/inc -I ~/gdrcopy -L ~/gdrcopy   \
 -gencode arch=compute_50,code=sm_50 -gencode arch=compute_52,code=sm_52 -gencode arch=compute_60,code=sm_60 -gencode arch=compute_60,code=compute_60 \
 -o romulu_gpudirect romulu_gpudirect.cu \
 -lgdrapi -lcuda

ldel04
/usr/local/cuda-8.0/bin/nvcc -ccbin g++-4.4 -m64  -I /media/ponsard/NVMESSD/NVIDIA_CUDA-8.0_Samples/common/inc -I ~/gdrcopy -L ~/gdrcopy    -gencode arch=compute_50,code=sm_50 -gencode arch=compute_52,code=sm_52 -gencode arch=compute_60,code=sm_60 -gencode arch=compute_60,code=compute_60  \
-o romulu_gpudirect romulu_gpudirect.cu  -lgdrapi -lcuda -libverbs
 *
 * 
 */

#include <stdlib.h>
#include <getopt.h>
#include <memory.h>
#include <stdio.h>
#include <math.h>
#include <iostream>

#include <stdio.h>
#include <unistd.h>
#include <string.h>

#include <sys/stat.h> 
#include <fcntl.h> 
#include <signal.h>


#include <infiniband/verbs.h>
#include <arpa/inet.h>
#include <malloc.h>

#include <cuda.h>
#include <cuda_runtime_api.h>
#include <cuda_runtime.h>
#include <helper_functions.h>    
#include <helper_cuda.h>  
using namespace std;
#define OUT cout

#include "gdrapi.h"
#include "common.hpp"
#define __CUCHECK(stmt, cond_str)					\
	do {								\
		CUresult result = (stmt);				\
		if (CUDA_SUCCESS != result) {				\
			const char *err_str = NULL;			\
			cuGetErrorString(result, &err_str);		\
			fprintf(stderr, "Assertion \"%s != cudaSuccess\" failed at %s:%d error=%d(%s)\n", cond_str, __FILE__, __LINE__, result, err_str); \
			exit(EXIT_FAILURE);                             \
		}							\
        } while (0)

#define CUCHECK(stmt) __CUCHECK(stmt, #stmt)

//----

#define __CUDACHECK(stmt, cond_str)					\
	do {								\
		cudaError_t result = (stmt);				\
		if (cudaSuccess != result) {				\
			fprintf(stderr, "Assertion \"%s != cudaSuccess\" failed at %s:%d error=%d(%s)\n", cond_str, __FILE__, __LINE__, result, cudaGetErrorString(result)); \
			exit(EXIT_FAILURE);				\
		}							\
        } while (0)

#define CUDACHECK(stmt) __CUDACHECK(stmt, #stmt)


CUstream 	gpu_stream;
CUevent 	hEvent;
int			waitFlag;
CUdeviceptr d_waitFlag;
int 		isGPUReady;

//to wake up process
void isrUSR1(int n)
{
	//printf("got sigusr1\n");
	cuStreamWriteValue32(gpu_stream, d_waitFlag,	2222,	0);
	
	//FIXME
	if (isGPUReady == 0) printf("DATA OVERRUN\n");
	//~ waitFlag = 2222;
	//checkCudaErrors (cudaEventRecord(hEvent, 0));

	isGPUReady = 0;
	return;
}






////////////////////////////////////////////////////////////////////////////////
// cuda kernel : rotate an image 	
////////////////////////////////////////////////////////////////////////////////
__global__ void rotate(char *inputImage,char *outputImage,int Width,int Height)
{   unsigned int i = blockIdx.x*blockDim.x + threadIdx.x,ii;
    unsigned int j = blockIdx.y*blockDim.y + threadIdx.y,jj;
    // (Height-1 - j,i)    <=  (i,j)
    ii = Height-1 - j;
    jj = i;
    
printf("i %d j %d\n",i,j);

    if (i < Width && j < Height && ii < Height && jj < Width) {
        outputImage[3*(ii+jj* Height)+0]=inputImage[3*(i+j* Width)+0];
        outputImage[3*(ii+jj* Height)+1]=inputImage[3*(i+j* Width)+1];
        outputImage[3*(ii+jj* Height)+2]=inputImage[3*(i+j* Width)+2];
        }
}











static CUdevice cuDevice;
static CUcontext cuContext;
#define N_IMAGES 		2
int main(int argc, char *argv[])
{
    unsigned int 	width 		= 640, 
					height 		= 480;
    size_t 			__size 		= 3*width*height; 	//200MB ok 256MB max with nvidia gpu
    
    const size_t gpu_page_size = 64*1024;
	size_t size = (__size + gpu_page_size - 1) & ~(gpu_page_size - 1);
	
		printf("initializing CUDA\n");
	CUresult error = cuInit(0);
	if (error != CUDA_SUCCESS) {
		printf("cuInit(0) returned %d\n", error);
		exit(1);
	}

	int deviceCount = 0;
	error = cuDeviceGetCount(&deviceCount);
	if (error != CUDA_SUCCESS) {
		printf("cuDeviceGetCount() returned %d\n", error);
		exit(1);
	}
	/* This function call returns 0 if there are no CUDA capable devices. */
	if (deviceCount == 0) {
		printf("There are no available device(s) that support CUDA\n");
		return 1;
	} else if (deviceCount == 1)
		printf("There is 1 device supporting CUDA\n");
	else
		printf("There are %d devices supporting CUDA, picking first...\n", deviceCount);

	int devID = 0;

	/* pick up device with zero ordinal (default, or devID) */
	CUCHECK(cuDeviceGet(&cuDevice, devID));

	char name[128];
	CUCHECK(cuDeviceGetName(name, sizeof(name), devID));
	printf("[pid = %d, dev = %d] device name = [%s]\n", getpid(), cuDevice, name);
	printf("creating CUDA Ctx\n");

	/* Create context */
	error = cuCtxCreate(&cuContext, CU_CTX_MAP_HOST, cuDevice);
	if (error != CUDA_SUCCESS) {
		printf("cuCtxCreate() error=%d\n", error);
		return 1;
	}

	printf("making it the current CUDA Ctx\n");
	error = cuCtxSetCurrent(cuContext);
	if (error != CUDA_SUCCESS) {
		printf("cuCtxSetCurrent() error=%d\n", error);
		return 1;
	}

	printf("=========>>>>>>cuMemAlloc() of a %zd bytes GPU buffer\n", size);
	CUdeviceptr d_A;
	error = cuMemAlloc(&d_A, size);
	if (error != CUDA_SUCCESS) {
		printf("cuMemAlloc error=%d\n", error);
		return 1;
	}
	printf("allocated GPU buffer address at %016llx pointer=%p\n", d_A, (void *) d_A);
	//////////////////////////////////////////////////////////////////////////////////
	
	
	//~ //configure compute unit, on GPU id
    //~ int 			dev_id 		= 0;
    //~ ASSERTRT(cudaSetDevice(dev_id));
    //~ void *dummy;
    //~ ASSERTRT(cudaMalloc(&dummy, 0));

    //allocate input memory
    //~ //size_t _size = (N_IMAGES*_size + GPU_PAGE_SIZE - 1) & GPU_PAGE_MASK;	
    //~ CUdeviceptr d_inputA;
    //~ ASSERTDRV(cuMemAlloc(&d_inputA, size));
	//~ CUdeviceptr deviceData = NULL;
    //~ ASSERTDRV(cuMemAlloc(&deviceData, size));
    CUCHECK(cuStreamCreate(&gpu_stream, CU_STREAM_DEFAULT));
//~ char*buf = (char*)deviceData;
	//map gpu device memory to PCIe BAR
	//~ gdr_t g = gdr_open();
	//~ gdr_mh_t mh;
	//~ gdr_pin_buffer(g, deviceData, size, 0, 0, &mh);
	//~ void *bar_ptr  = NULL;
	//~ printf("we mapp %d bytes\n",size);
	//~ gdr_map(g, mh, &bar_ptr, size);
    
    
    
struct ibv_device   **dev_list;
struct ibv_device	*ib_dev;
	
struct ibv_comp_channel *channel;

struct ibv_pd *pd;
struct ibv_mr *mr,*mr_gth;
struct ibv_cq *cq;
struct ibv_qp *qp;
struct ibv_qp_init_attr qp_init_attr;
struct ibv_context	*context;

//FIXME somewhere else
int rx_depth   = 500;
int ib_port = 1 ;
   
int 	page_size=sysconf(_SC_PAGESIZE);
char 	*buf_gth = (char*)memalign(page_size, 40);
printf("buf_gth %p\n",buf_gth);
    
dev_list = ibv_get_device_list(NULL);
	if (!dev_list) {
		perror("Failed to get IB devices list");
		return NULL;
	}
ib_dev = dev_list[0];
if (!ib_dev) {
	fprintf(stderr, "No IB devices found\n");
	return NULL;
}

//create context
context = ibv_open_device(ib_dev);
if (!context) {
	fprintf(stderr, "Couldn't get context for %s\n",
		ibv_get_device_name(ib_dev));
	
	}
printf("using device %s\n",ibv_get_device_name(ib_dev));

//create completion channel	
channel= ibv_create_comp_channel(context);
if (!channel) {
	fprintf(stderr, "Error, ibv_create_comp_channel() failed\n");
	return NULL;
}

//port_info
//~ struct ibv_port_attr port_info = {};
//~ if (ibv_query_port(context, ib_port, &port_info)) {
	//~ fprintf(stderr, "Unable to query port info for port %d\n", ib_port);
	//~ //goto clean_device;
//~ }
//~ int mtu = 1 << (port_info.active_mtu + 7);
//~ printf("MTU (%d)\n", mtu);
	
//Protection Domain
pd = ibv_alloc_pd(context);
if (!pd) {
	fprintf(stderr, "Couldn't allocate PD\n");
	
}
//~ d_A=(CUdeviceptr)malloc(size);
//Memory Region
mr 	=  ibv_reg_mr(pd, (void *) d_A, size, 	IBV_ACCESS_LOCAL_WRITE|IBV_ACCESS_REMOTE_WRITE);
if (!mr) { 
	fprintf(stderr, "Caution Couldn't register MR mr %p. V0.1.5 errno%d\n",d_A,errno);
	
}
mr_gth 	=  ibv_reg_mr(pd, buf_gth,  40, 		IBV_ACCESS_LOCAL_WRITE|IBV_ACCESS_REMOTE_WRITE);
if (!mr_gth) {
	fprintf(stderr, "Couldn't register MR mrgth\n");
	
}   
//~ printf("getchar\n");
//~ getchar();
    
 //Completion Queue
//Completion Queue
//~ printf("ibv_create_cq %p %p %d\n",context,channel,rx_depth);
cq = ibv_create_cq(context, rx_depth + 1, NULL,channel, 0);
	if (!cq) {
		fprintf(stderr, "Couldn't create CQ\n");
		//~ goto clean_mr;
	}

//Queue Pair
//~ printf("creating QP\n");
memset(&qp_init_attr, 0, sizeof(struct ibv_qp_init_attr));
qp_init_attr.send_cq = cq;
qp_init_attr.recv_cq = cq;
qp_init_attr.cap.max_send_wr  = 10;
qp_init_attr.cap.max_send_sge = 10;
//qp_init_attr.cap.max_inline_data = 100;
//qp_init_attr.srq = NULL;
qp_init_attr.cap.max_recv_wr  = 10;
qp_init_attr.cap.max_recv_sge = 10;
qp_init_attr.qp_type = IBV_QPT_UD; 
//~ printf("ibv_create_qp\n");
qp = ibv_create_qp(pd,&qp_init_attr);
if (!qp) {
	fprintf(stderr, "---------Error, ibv_create_qp() failed\n");
	return NULL;
}
printf("FOR RASHPA MANAGER qp_number %d\n",qp->qp_num);

		
		
struct ibv_qp_attr attr;
attr.qp_state        = IBV_QPS_INIT;
attr.pkey_index      = 0;
attr.port_num        = ib_port;
attr.qkey            = 0xCAFECAFE;
printf("ibv_modify_qp to INIT\n");
if (ibv_modify_qp(qp, &attr,
		  IBV_QP_STATE              |
		  IBV_QP_PKEY_INDEX         |
		  IBV_QP_PORT 				| 
		  IBV_QP_QKEY //IBV_QP_ACCESS_FLAGS
		  )) {
	fprintf(stderr, "Failed to modify QP to INIT\n");
	return NULL;
}

//Modify QP to RTR
printf("ibv_modify_qp to RTR\n");
memset(&attr, 0, sizeof(attr));
attr.qp_state		= IBV_QPS_RTR;
if (ibv_modify_qp(qp, &attr, IBV_QP_STATE)) {
		fprintf(stderr, "Failed to modify QP to RTR\n");
		return NULL;
	}
printf("ibv_modify_qp to RTR done\n");

//Post Recv Work Request
struct ibv_sge sge [2];
struct ibv_recv_wr wr, *bad_wr;
wr.wr_id	    	= 0x1234;
wr.sg_list    		= &sge[0];
wr.num_sge    		= 2;
wr.next=NULL;
	
sge[0].length  		= 40; // 40 extra bytes from RoCEv2 UD SEND (GTH)
sge[0].addr 		= (uintptr_t)buf_gth;
sge[0].lkey			= mr_gth->lkey;
sge[1].lkey			= mr->lkey;


//nWR should be less tham rec_wr_max
int total=0,nWR=0,counter=0,missingPacket=0,imm=-1,oldimm=0,stage=1;
//~ while(total<totalSize) {
int dataSize=size;
int udsendSize=512;

	int length = 0, chunk=0;
	while (length < dataSize) {
		chunk = ((dataSize-length) < udsendSize) ? (dataSize-length) : udsendSize;
		if (stage) {
			sge[1].addr = (uintptr_t)(d_A + length);
			}
		else {
			//~ sge[1].addr = (uintptr_t)(buf + dataSize + length);
			}
		sge[1].length = chunk;
	
		//~ printf("before ibv_post_recv  v1\n");
		if (ibv_post_recv(qp, &wr, &bad_wr)) {
			printf("ibv_post_recv ERROR v0\n");
			return NULL;
			}
		//~ printf("after ibv_post_recv  v1\n");

		//printf("ibv_post_recv WR %d chunk %d\n",nWR,chunk);
		struct ibv_wc wc;
		int num_comp;
		do {
			num_comp = ibv_poll_cq(cq, 1, &wc);
		} while (num_comp == 0);
		
		if (num_comp < 0) {
			fprintf(stderr, "ibv_poll_cq() failed\n");
			return NULL;
		}
		 
		/* verify the completion status */
		if (wc.status != IBV_WC_SUCCESS) {
			fprintf(stderr, "Failed status %s (%d) for wr_id %d\n", 
				ibv_wc_status_str(wc.status),
				wc.status, (int)wc.wr_id);
			return NULL;
		}
		//check missing packets
		oldimm	= imm;
		imm		= ntohl(wc.imm_data);
				//~ printf("psn ! %d  %p\n",imm,sge[1].addr);

		length += chunk*(imm-oldimm);
		nWR ++;	
	}

	//~ kill(pid_romulu,SIGUSR1);
	//~ total += dataSize;
	//~ stage = !stage;
	//printf(" data %x data2 %x %d\n",*(int*)buf,*(int*)(buf+dataSize),stage);
//~ }	 
	
printf("pktCount %d\n",nWR);   

    
    
    
    
    
    /////////// TODO ///////////////////////////////////////
    //get PCIE BAR for rashpamanager
    //
    
    //allocate device & host memory for result
	char *d_outputData = NULL;
    checkCudaErrors(cudaMalloc((void **) &d_outputData, size));
    char *h_outputData = (char *) malloc(size);
    
        	checkCudaErrors(cudaMemcpy(h_outputData, (void*)d_A, size, cudaMemcpyDeviceToHost));

    
    //~ int i;
//~ for(i=0;i<100;i++)
		//~ printf("%x ",*(char*)(h_outputData+i) & 0xff);
//~ printf("\n");
    
    
    dim3 dimBlock(32, 32, 1);
    dim3 dimGrid(640, 480, 1);
    
    int imgCounter=0;
    char fname[256];
	sprintf(fname,"/tmp/pid.romulu_gpudirect");
	int f=open(fname, O_RDWR|O_TRUNC|O_CREAT,S_IRWXU);
	int pid=getpid();
	write(f,&pid,4);
	close(f);
	printf("/tmp/pid.romulu_gpudirect %d\n",pid);

	//synchronize GPU / rdma scheme : check only overrun
	//~ isGPUReady = 1;
	ASSERTDRV(cuMemHostRegister((void*)&waitFlag, 4, CU_MEMHOSTALLOC_DEVICEMAP));
	//*waitFlag=0;

	ASSERTDRV(cuMemHostGetDevicePointer(&d_waitFlag, &waitFlag, 0));
	
	//~ *waitFlag = 0;
	//~ checkCudaErrors(cudaEventCreate(&hEvent, CU_EVENT_BLOCKING_SYNC| CU_EVENT_DISABLE_TIMING ));
	//~ //install irq handler
	signal(SIGUSR1,isrUSR1);
	
	//main loop
//~ sigset_t mask, oldmask;
 //~ sigemptyset (&mask);
 //sigaddset (&mask, SIGUSR1);
 //sigprocmask (SIG_BLOCK, &mask, &oldmask);
	while(1) {
		//will continue on cpu, but halt  gpu stream waiting for incoming data
		waitFlag = 0;
		
		checkCudaErrors(cuStreamWaitValue32( gpu_stream, d_waitFlag, 2222, CU_STREAM_WAIT_VALUE_EQ ));
		//cuEventQuery (  hEvent );
		//checkCudaErrors (cudaEventRecord(hEvent, gpu_stream));
	    //checkCudaErrors(cuStreamWaitEvent ( gpu_stream, hEvent, 0));
		//checkCudaErrors(cuStreamSynchronize ( gpu_stream));

		//checkCudaErrors (cudaEventSynchronize(hEvent));
		//printf("waitFlag=%d\n",*waitFlag);//getchar();
		sprintf(fname,"/home/ponsard/res%d.data",imgCounter++);
		f=open(fname, O_RDWR|O_CREAT,S_IRWXU);
		if (f == -1){	
			printf("error open res.data\n");
			}
    	// TODO kernel Warmup
    	//~ sigsuspend (&mask);
    	//~ printf("getchar\n");gpu_stream
    	//~ getchar();
    	
    	printf("before rotate\n");
		rotate<<<dimGrid, dimBlock,0>>>((char*)d_A, d_outputData, width, height);
		printf("after rotate\n");

		cudaError_t err = cudaGetLastError();
		printf("after rotate2\n");
		printf("after rotate2111\n");

    if (err != cudaSuccess)
    {
				printf("after rotate23333\n");

        fprintf(stderr, "Failed to launch vectorAdd kernel (error code %s)!\n", cudaGetErrorString(err));
        exit(EXIT_FAILURE);
    }
		//ASYNC
		printf("after rotate2bis\n");

    	checkCudaErrors(cudaMemcpy(h_outputData, d_outputData, 921600, cudaMemcpyDeviceToHost));
printf("after rotate2ter\n");
printf("after rotate3\n");

//~ for(i=0;i<100;i++)
	//~ printf("%x ",*(char*)(h_outputData+i) & 0xff);
//~ printf("\n");
    	//usleep(1000); //for testing only
    	isGPUReady = 1;
		write(f, h_outputData, width*height*3);
		close(f);
		printf("res file %s saved\n",fname);
		}
		
		
		clean_mr:
	ibv_dereg_mr(mr);

clean_pd:
	ibv_dealloc_pd(pd);

clean_comp_channel:
	if (channel) ibv_destroy_comp_channel(channel);

clean_device:
	ibv_close_device(context);

clean_buffer:
	//free(buf);
	return 0;
}
