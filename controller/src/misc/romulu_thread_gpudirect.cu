/*
 *
 * gpudirect/nvpeermem
 * nvidia/mellanox
 * 
 *
 * cuStreamWaitValue32( s0, d_waitFlag1, ROMULU_GPU_SIG, CU_STREAM_WAIT_VALUE_EQ ) 
 * 
 */


#include <stdlib.h>
#include <getopt.h>
#include <memory.h>
#include <stdio.h>
#include <math.h>
#include <iostream>

#include <stdio.h>
#include <unistd.h>
#include <string.h>

#include <sys/stat.h> 
#include <fcntl.h> 
#include <signal.h>

#include <sched.h>

#include <infiniband/verbs.h>
#include <arpa/inet.h>
#include <malloc.h>

#include <cuda.h>
#include <cuda_profiler_api.h>
#include <cuda_runtime_api.h>
#include <cuda_runtime.h>
#include <unistd.h>
#include <sys/mman.h>
#include <fcntl.h>

#define DEBUG 			1
#define debug_printf	if (DEBUG==1) printf



#define __CUCHECK(stmt, cond_str)					\
	do {								\
		CUresult result = (stmt);				\
		if (CUDA_SUCCESS != result) {				\
			const char *err_str = NULL;			\
			cuGetErrorString(result, &err_str);		\
			fprintf(stderr, "Assertion \"%s != cudaSuccess\" failed at %s:%d error=%d(%s)\n", cond_str, __FILE__, __LINE__, result, err_str); \
			exit(EXIT_FAILURE);                             \
		}							\
        } while (0)

#define CUCHECK(stmt) __CUCHECK(stmt, #stmt)

//----

#define __CUDACHECK(stmt, cond_str)					\
	do {								\
		cudaError_t result = (stmt);				\
		if (cudaSuccess != result) {				\
			fprintf(stderr, "Assertion \"%s != cudaSuccess\" failed at %s:%d error=%d(%s)\n", cond_str, __FILE__, __LINE__, result, cudaGetErrorString(result)); \
			exit(EXIT_FAILURE);				\
		}							\
        } while (0)

#define CUDACHECK(stmt) __CUDACHECK(stmt, #stmt)
#define ASSERT(x)                                                       \
    do                                                                  \
        {                                                               \
            if (!(x))                                                   \
                {                                                       \
                    fprintf(stdout, "Assertion \"%s\" failed at %s:%d\n", #x, __FILE__, __LINE__); \
                    /*exit(EXIT_FAILURE);*/                                 \
                }                                                       \
        } while (0)
#define ASSERTDRV(stmt)				\
    do                                          \
        {                                       \
            CUresult result = (stmt);           \
            ASSERT(CUDA_SUCCESS == result);     \
        } while (0)

#define checkCudaErrors(err) {                                          \
 cudaError_t e=cudaGetLastError();                                 \
 if(e!=cudaSuccess) {                                              \
   printf("Cuda failure %s:%d: '%s'  ----%d\n",__FILE__,__LINE__,cudaGetErrorString(e),err);           \
   exit(0); \
 }                                                                 \
}

inline
cudaError_t checkCuda(cudaError_t result)
{
  if (result != cudaSuccess) {
    fprintf(stderr, "CUDA Runtime Error: %s\n", 
            cudaGetErrorString(result));
    ASSERT(result == cudaSuccess);
  }
  return result;
}

#include "gdrapi.h"
#include "romulu_thread_gpudirect.h"
#include "cuda_common.hpp"
#include "librashpa.h"
#include "romulu.h"


////////////////////////////////////////////////////////////////////////////////
// cuda demo kernel : rotate an image 	
////////////////////////////////////////////////////////////////////////////////

__global__ void rightRotation(char *inputImage,char *outputImage,int Width,int Height)
{   unsigned int i = blockIdx.x*blockDim.x + threadIdx.x,ii;
    unsigned int j = blockIdx.y*blockDim.y + threadIdx.y,jj;
    // (Height-1 - j,Width-1 - i)    <=  (i,j)
    ii = j; //Height-1 - j;
    jj = Width-1 - i;

    if (i < Width && j < Height && ii < Height && jj < Width) {
        outputImage[(ii+jj* Height)+0] = inputImage[(i+j* Width)+0];
        }
}

__global__ void
leftRotation(const char *A,  char *C, int Width, int Height)
{
	unsigned int i = blockIdx.x*blockDim.x + threadIdx.x,ii;
    unsigned int j = blockIdx.y*blockDim.y + threadIdx.y,jj;
    // (Height-1 - j,i)    <=  (i,j)
    ii = Height-1 - j;
    jj = i;

    if (i < Width && j < Height && ii < Height && jj < Width)
    {
		//~ if (i==0 && j==0) 
			//~ printf("i %d j %d | %x %x \n",i,j, A[3*(i+j* Width)+0],C[3*(ii+jj* Height)+0]);
        C[(ii+jj* Height)+0] = A[(i+j* Width)+0] ;
    }
}

template <unsigned int blockSize>
__global__ void
Average(const float16 *A,  float16 *C, int n)
{
	extern __shared__ int sdata[];
	
	unsigned int tid = threadIdx.x;
	unsigned int i = blockIdx.x*(blockDim.x*2) + threadIdx.x;
	unsigned int gridSize = blockSize*2*gridDim.x;
	
	sdata[tid] = 0;
	while (i < n) {
		sdata[tid] += g_idata[i] + g_idata[i+blockSize];
		i += gridSize;
	}
	__syncthreads()
	
	if (blockSize >= 512) { if (tid < 256) { sdata[tid] += sdata[tid + 256]; } __syncthreads(); }
	if (blockSize >= 256) { if (tid < 128) { sdata[tid] += sdata[tid + 128]; } __syncthreads(); }
	if (blockSize >= 128) { if (tid <   64) { sdata[tid] += sdata[tid +   64]; } __syncthreads(); }
	
	if (tid < 32)
	{
		if (blockSize >=  64) sdata[tid] += sdata[tid + 32];
		if (blockSize >=  32) sdata[tid] += sdata[tid + 16];
		if (blockSize >=  16) sdata[tid] += sdata[tid +  8];
		if (blockSize >=    8) sdata[tid] += sdata[tid +  4];
		if (blockSize >=    4) sdata[tid] += sdata[tid +  2];
		if (blockSize >=    2) sdata[tid] += sdata[tid +  1];
	}
	
	if (tid == 0) g_odata[blockIdx.x] = sdata[0]
}
	
static CUdevice 		cuDevice;
static CUcontext 		cuContext;
CUdeviceptr 			d_A;

cudaStream_t 			s0,s1,s2,s3;
CUdeviceptr 			d_waitFlag1; 

extern pthread_cond_t 	condition;
pthread_mutex_t 		mutex;

int 					nImgOnGpu;

char *h_aPinned,*h_bPinned;

extern "C" void release_gpu_context();
void release_gpu_context()
{
	//~ checkCuda( cudaFree(&d_A) );
	CUCHECK( cuCtxDestroy(cuContext) );
	checkCuda( cudaDeviceReset() );
}


/**
 * 
 * allocate_gpu_device_memory: map gpu memory for DMA
 * 
 * @size : in bytes
 * @devID : cuda device id
 * 
 * 
 * called from rashpa_rocev2_ud_backend_receiver_init (start_IB)
 */
extern "C" void* allocate_gpu_device_memory(size_t, uint64_t, int);
void* allocate_gpu_device_memory(size_t size, uint64_t	pinnedbufSize,int devID)
{
	//~ waitFlagPtr1 = &waitFlag1;
	debug_printf("allocate_gpu_device_memory: size %d devID %d\n",size,devID);

	CUresult error = cuInit(0);
	if (error != CUDA_SUCCESS) {
		printf("cuInit(0) returned %d\n", error);
		exit(1);
	}

	int deviceCount = 0;
	error = cuDeviceGetCount(&deviceCount);
	if (error != CUDA_SUCCESS) {
		printf("cuDeviceGetCount() returned %d\n", error);
		exit(1);
	}
	/* This function call returns 0 if there are no CUDA capable devices. */
	if (deviceCount == 0 || devID > (deviceCount-1) ) {
		printf("There are no available device(s) that support CUDA or bad devId\n");
		return NULL;
	} 

	/* pick up device with zero ordinal (default, or devID) */
	CUCHECK(cuDeviceGet(&cuDevice, devID));

	char name[128];
	CUCHECK(cuDeviceGetName(name, sizeof(name), devID));
	debug_printf("[pid = %d, dev = %d] device name = [%s]\n", getpid(), cuDevice, name);

	int prop = -1;
	cuDeviceGetAttribute (&prop,CU_DEVICE_ATTRIBUTE_CAN_USE_STREAM_MEM_OPS,cuDevice);
	if (prop == 1) 
		printf("CU_DEVICE_ATTRIBUTE_CAN_USE_STREAM_MEM_OPS enable\n");
	else {
		printf("Cuda USE_STREAM_MEM_OPS not supported, try moprobe nvidia NVreg_EnableStreamMemOPs=1\n");
		return NULL;
	} 
	
	/* Create context */
	error = cuCtxCreate(&cuContext, CU_CTX_MAP_HOST, cuDevice);
	if (error != CUDA_SUCCESS) {
		printf("cuCtxCreate() error=%d\n", error);
		return NULL;
	}

	error = cuCtxSetCurrent(cuContext);
	if (error != CUDA_SUCCESS) {
		printf("cuCtxSetCurrent() error=%d\n", error);
		return NULL;
	}

	error = cuMemAlloc(&d_A, size);
	if (error != CUDA_SUCCESS) {
		printf("cuMemAlloc size %d  error=%d\n", size,error );
		return NULL;
	}

	/**
	 * 
	 * create 2 output buffers into pinned memory
	 * 
	 * 
	 */
	
	debug_printf("cudaMallocHost h_aPinned %f GB\n",(float)pinnedbufSize/1e9);
	checkCuda( cudaMallocHost((void**)&h_aPinned, pinnedbufSize) ); 
	debug_printf("cudaMallocHost h_bPinned %f GB\n",(float)pinnedbufSize/1e9);
	checkCuda( cudaMallocHost((void**)&h_bPinned, pinnedbufSize) );
	
	debug_printf("memset buf1 & buf2\n");
	memset(h_aPinned, 0, pinnedbufSize);
	memset(h_bPinned, 0, pinnedbufSize);

	printf("GPU DMA buffer address at 0x%016llx of %d bytes\n", d_A, size);
	return (void*)d_A;
}

/**
 * 
 * launch_gpu_compute: thread code executing cuda kernel when RDMA data available
 * 
 * @data : struct with relevant info
 * 
 */
struct timespec startgpuTime, endgpuTime;

extern "C" void* launch_gpu_compute(void*data);
void* launch_gpu_compute(void*data)
{
	rashpa_link_arg_t* link			= &g_romulu->link;

	int 		imgSize				= link->imgSize;
	int 		width 				= link->roi_w;
	int 		height				= link->roi_h;
	int 		nImages				= link->nImages;
	int 		nIter 				= link->nIter;
	int 		rashpa_bunch 				= link->rashpa_bunch;
	
	//~ int 		gpu_aligned_size	= link->gpu_aligned_size;
	
	CUCHECK( cuCtxSetCurrent(cuContext) );

    checkCuda(cudaStreamCreate(&s0));
    checkCuda(cudaStreamCreate(&s1));
	
	//FIXME choose 32 multiple ?
	dim3 dimBlock1(width,1, 1);
	dim3 dimGrid1 (1,height, 1);
	
	//~ dim3 dimBlock2(512,1, 1);
	//~ dim3 dimGrid2 (1,512, 1);
	
	/**
	 * take care of cpu affinity : mlx5 root complex
	 * 
	 */
	cpu_set_t set;
	CPU_ZERO(&set);
	CPU_SET(1, &set);
	CPU_SET(3, &set);
	CPU_SET(5, &set);
	CPU_SET(7, &set);
	//~ CPU_SET(9, &set);
	//~ CPU_SET(11, &set);
	//~ CPU_SET(13, &set);
	//~ CPU_SET(15, &set);
	sched_setaffinity(0, sizeof(set), &set);
	unsigned cpu = sched_getcpu();
	
	char *d_C1, *d_C2,*d_C11, *d_C22;
	checkCuda( cudaMalloc(&d_C1,rashpa_bunch*imgSize) );
	checkCuda( cudaMalloc(&d_C2,rashpa_bunch*imgSize) );
	checkCuda( cudaMalloc(&d_C11,imgSize) );
	checkCuda( cudaMalloc(&d_C22,imgSize) );
	
	//warmup
	leftRotation<<<dimGrid1, dimBlock1, 0, s0 >>>((char*)d_A, d_C1, width,height);
	rightRotation<<<dimGrid1, dimBlock1,0, s0 >>>((char*)d_A, d_C2, width,height);

	debug_printf("launch_gpu_compute on cpu core %d waiting for a total of %d images (imgSize %d) bunch %d\n",
				cpu, nImages*nIter, imgSize,rashpa_bunch);
	
	
			
	unsigned long int offset1 = 0,offset2 = 0;
	offset1		= 0;
	offset2		= 0;
	nImgOnGpu 	= 0;
	
	while ((rashpa_bunch*nImgOnGpu/2) < (nImages*nIter))
	{	

		pthread_cond_wait(&condition, &mutex);
		//~ debug_printf("gpu compute loop %d\n",nImgOnGpu);

		if (!(nImgOnGpu % 2))
		{	
			//~ debug_printf("gpu compute offset1 %d,  %d\n",offset1,nImgOnGpu);
			checkCudaErrors( cudaStreamSynchronize (s0) );

			int off1;
			off1 = 0; //circular buffer
			for (int i = 0; i < rashpa_bunch/2; i ++) {
				rightRotation  <<< dimGrid1, dimBlock1, 0, s0 >>>((char*) d_A + off1, d_C1 + off1, width,height);
				off1 += imgSize;
				}
			checkCuda( cudaMemcpyAsync(	h_aPinned + offset1, (void*)(d_C1), rashpa_bunch/2*imgSize, cudaMemcpyDeviceToHost,s1) );
			
			
			//to measure H->D D->H bw
			//~ checkCuda( cudaMemcpyAsync(	h_aPinned + offset1, (void*)(d_A), rashpa_bunch/2*imgSize, cudaMemcpyDeviceToHost,s1) );
			//~ offset1  += rashpa_bunch/2*imgSize;

		}
		else
		{				
			//~ debug_printf("gpu compute bank %d, %d left rot %d bank offset %d\n",bank, rashpa_bunch/2,nImgOnGpu, rashpa_bunch*imgSize);
			checkCudaErrors( cudaStreamSynchronize (s1) );

			int off2;
			off2 = 0;
			for (int i = 0; i < rashpa_bunch/2; i ++) {
				leftRotation  <<< dimGrid1, dimBlock1, 0, s1 >>>((char*)d_A  + off2 + rashpa_bunch/2*imgSize,
																		d_C2 + off2, width,height);
				off2 += imgSize;
				}
			checkCuda( cudaMemcpyAsync(h_bPinned + offset2, (void*)(d_C2), rashpa_bunch/2*imgSize, cudaMemcpyDeviceToHost,s1) );
			
			
			//to measure H->D D->H bw
			//~ checkCuda( cudaMemcpyAsync(h_bPinned + offset2, (void*)(d_A + rashpa_bunch/2*imgSize), rashpa_bunch/2*imgSize, cudaMemcpyDeviceToHost,s1) );
		}

		if (nImgOnGpu == 0) 
		{
			printf("---------------------> start GPU clock\n");
			clock_gettime(CLOCK_MONOTONIC, &startgpuTime);
		}
		
		nImgOnGpu++;
	}

	clock_gettime(CLOCK_MONOTONIC, &endgpuTime);
	double dt_ms 	= (endgpuTime.tv_nsec-startgpuTime.tv_nsec)/1000000.0 + (endgpuTime.tv_sec-startgpuTime.tv_sec)*1000.0;
	unsigned long int pinnedbufSize = (unsigned long int) nImages * nIter *imgSize/2;
	double gbps 	= (double)2*pinnedbufSize * 8 / dt_ms * 1e3 / 1024.0 / 1024.0 / 1024.0;

	printf("\n\nGPU Compute BW %lf Gbits/s. Terminated in %f ms. \n", gbps, dt_ms);
	printf("GPU received %d x %d  Images. buf size %lu\n", 
					nImgOnGpu, rashpa_bunch/2, link->pinnedbufSize);
	//~ k = 0;

	//to save time 
	if (!savePinnedBuf)
		pinnedbufSize = 0;
	
	int f1 = open("/tmp/pinnedres1.data", O_RDWR|O_CREAT|O_TRUNC,S_IRWXU);
	int f2 = open("/tmp/pinnedres2.data", O_RDWR|O_CREAT|O_TRUNC,S_IRWXU);

	if (f1 == -1 || f2 == -1)
	{	
		printf("error open pinnedres?.data\n");
	}

	unsigned long int res = 0;
	do
	{
		res += write(f1, h_aPinned+res, pinnedbufSize-res);	
		debug_printf("saving /tmp/pinnedres1 %ld bytes\n",res);

	}
	while (res < pinnedbufSize);
	
	res = 0;
	do
	{
		res += write(f2, h_bPinned+res, pinnedbufSize-res);	
		debug_printf("saving /tmp/pinnedres2 %ld bytes\n",res);

	}
	while (res < pinnedbufSize);
	
	printf("syncing on HD may take some time...\n");
	close(f1);
	close(f2);
	
	//prepare another transfer
	
	
    //~ CUCHECK( cuMemHostUnregister((void*)&waitFlag1) );
    //~ checkCuda( cudaFree(&d_A) ); 
    
    //~ checkCuda( cudaFree(d_C1) ); checkCuda( cudaFree(d_C2) );
    //~ checkCuda( cudaFreeHost(h_aPinned) ); checkCuda( cudaFreeHost(h_bPinned) );
    
	//~ munmap(h_aPinned, totalSize/2);
	//~ munmap(h_bPinned, totalSize/2);
    return NULL;
}


