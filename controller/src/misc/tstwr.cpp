
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <malloc.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <signal.h>

#include <infiniband/verbs.h>

///////////////////////////////
//should be 16 16
#define RASHPA_WR				8
#define RASHPA_SGE				1

int udsendSize=640;
void *buf =(void*)0x1234;
void *buf_gth =(void*)0x5678;

void disp_wr(struct ibv_recv_wr *wr)
{
	for(int i=0;i<RASHPA_WR;i++) {
		printf("wr %d addr %p wr_id %lx sg_list %p num_sge %d next %p\n", i,
			&wr[i]			,
			wr[i].wr_id     ,
			wr[i].sg_list   ,
			wr[i].num_sge 	,
			wr[i].next 		);
		for (int j=0; j<RASHPA_SGE;j++) {
			struct ibv_sge*sg = wr[i].sg_list;
			printf("sge %p %p\n",&sg[0],wr[i].sg_list);
			printf("     sge0 %p \taddr %lx \tlen %d \t\t\tkey %x\n",
				&sg[2*j],
				sg[2*j].addr,
				sg[2*j].length,
				sg[2*j].lkey	 );
			printf("     sge1 %p \taddr %lx \tlen %d \t\tkey %x\n",
				&sg[2*j+1],
				sg[2*j+1].addr,
				sg[2*j+1].length,
				sg[2*j+1].lkey	 );
		}
	}
	
}

main()
{
	//~ struct ibv_recv_wr WR[RASHPA_WR];
	//~ struct ibv_sge SG[RASHPA_WR*RASHPA_SGE*2];
	
struct ibv_recv_wr	*wr= (struct ibv_recv_wr*) malloc(RASHPA_WR*             sizeof(struct ibv_recv_wr));
struct ibv_sge 		*sg= (struct ibv_sge*)     malloc(RASHPA_WR*RASHPA_SGE*2*sizeof(struct ibv_sge));
					
memset(&wr[0], 0, RASHPA_WR*				sizeof(struct ibv_recv_wr));
memset(&sg[0], 0, RASHPA_WR*RASHPA_SGE*2*	sizeof(struct ibv_sge));

printf("sizeof wr %d %d sge %d %d\n",sizeof(*wr),sizeof(struct ibv_recv_wr),sizeof(*sg),sizeof(struct ibv_sge));

//build WR/SGE for 1 image
uint64_t length = 0;

for (int i = 0 ; i < RASHPA_WR ; i++) {
	//~ pktCount++;
	wr[i].wr_id     = (0x33 << 8) | i;
	wr[i].sg_list   = &sg[i*2*RASHPA_SGE];
	wr[i].num_sge 	= 2*RASHPA_SGE; // gth and data
	wr[i].next 		= (i == (RASHPA_WR-1)) ? NULL : &wr[i+1];
	
	for (int j = 0 ; j < RASHPA_SGE ; j++) {
		int k = 2*(RASHPA_SGE*i+j);

		sg[k].addr 			= (uintptr_t)buf_gth;
		sg[k].length  		= 40; 
		sg[k].lkey	  		= 0xaaaa;
		
		sg[k+1].addr	 	= (uintptr_t)(buf + length);
		sg[k+1].length  	= udsendSize; 
		sg[k+1].lkey	  	= 0xbbbb;
		
		//~ sg[2*j+1][j].addr	= (uintptr_t)(buf + gpu_aligned_size + length);

		//~ length 								+=  udsendSize;

		//~ if (length >= imgSize) {
			//~ printf("end of image \n");
			//~ imgDone = 1;
			//~ wr[i].next = NULL;
			//~ wr[i].num_sge = 2*j;
			//~ j=RASHPA_SGE; i=RASHPA_WR;
		//~ }
	}
	//~ if () break; 
}

disp_wr(wr);
}
