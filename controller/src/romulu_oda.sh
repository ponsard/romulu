#!/bin/bash
cd romulu/controller/src

source /segfs/deg/bin/degprofile.sh

#change require in Makefile line 26
LIBVMA=$VMA
NOPROF=$PROF
CUDA=1
make clean
rm romulu_opencl.o romulu_nvidia.o ~/librashpa/src/*.o /tmp/controller

if [ "$CUDA" = "1" ]; then
echo "cuda defined"
make  devel 
else
echo "cuda NOT defined"
make devel
fi

g++ romulu_opencl.cpp -o romulu_opencl.o -c -I /users/ponsard/librashpa/include/

LLIBS="-ldance -lepci -lpci -lefpak -lespi -lz -ledma -luirq -lebuf -lpmem  \
-lm -ldl -lrt -lpthread -ldance -libverbs -lrdmacm -lxml2 -lzmq -lOpenCL -lnuma"

if [ "$CUDA" = "1" ]; then
echo "cuda defined"

COMP="/usr/local/cuda/bin/nvcc -ccbin g++-6 -m64  \
-gencode arch=compute_70,code=sm_70 \
-DCUDA=1 -I /usr/local/cuda/include -L /usr/local/cuda/lib64 -L /usr/lib/x86_64-linux-gnu/"

#-Xcompiler -fopenmp
$COMP -c -o romulu_nvidia.o romulu_nvidia.cu -I ~/librashpa/include  
#$COMP -c -o romulu_nvidia.s romulu_nvidia.cu -I ~/librashpa/include -Xcompiler -fopenmp -Xcompiler -S

LLIBS=$LLIBS" -lnuma -lcuda -lcudart "
# "-lnppist -lnppitc -lnppial -lnppisu -lnppc -lcusparse_static -lcublas_static -lculibos"
#echo $LLIBS
echo "using CUDA compiler "$COMP
else
COMP="g++-6" 
fi

# -Xcompiler-fopenmp
$COMP   \
-o /tmp/controller  romulu_opencl.o controller.o romulu_nvidia.o \
romulu_rsocket.o \
~/librashpa/src/rashpa_main.o ~/librashpa/src/rashpa_udp.o \
~/librashpa/src/rashpa_xml.o  ~/librashpa/src/rashpa_init.o \
~/librashpa/src/rashpa_roce_ud.o ~/librashpa/src/rashpa_zmq.o \
 romulu_generic.o  \
-L /usr/lib/x86_64-linux-gnu/   \
-L ~/romulu/controller/lib/libdance/src/ \
-L ~/romulu/controller/lib/libepci/src/ \
-L ~/romulu/controller/src/../lib/libefpak/src \
-L ~/romulu/controller/src/../lib/libespi/src \
-L ~/romulu/controller/src/../lib/libedma/src \
-L ~/romulu/controller/src/../lib/libebuf/src \
-L ~/romulu/controller/src/../lib/libuirq/src/u \
-L ~/romulu/controller/src/../lib/libpmem/src/u \
-L ~/romulu/controller/src/../lib/libdance/src \
-L ~/tcc-0.9.27 -ltcc  -L ~ $LLIBS

#sudo setcap "cap_dac_override+ep cap_sys_rawio+ep" /tmp/controller


echo 'ROMULU DAnCE Controller.....' 

if [ "$LIBVMA" = "1" ]; then
echo "using libVMA... Incompatibility with RoCE"
LD_PRELOAD=/usr/lib/libvma.so VMA_SPEC=latency \
LD_LIBRARY_PATH=~/romulu/controller/lib/libdance/src/:~/romulu/controller/lib/libepci/src/:../lib/libefpak/src/:../lib/libespi/src/:../lib/libedma/src/:../lib/libuirq/src/u/:../lib/libebuf/src/:../lib/libpmem/src/u/:../../../tcc-0.9.27/ \
/tmp/controller -lAPP 
else
if [ "$NOPROF" = "0" ]; then
LD_LIBRARY_PATH=../../../gdrcopy:../../../romulu/controller/lib/libdance/src/:../../../romulu/controller/lib/libepci/src/:../lib/libefpak/src/:../lib/libespi/src/:../lib/libedma/src/:../lib/libuirq/src/u/:../lib/libebuf/src/:../lib/libpmem/src/u/:../../../tcc-0.9.27/ \
/tmp/controller -lAPP 
else
LD_LIBRARY_PATH=../../../gdrcopy:../../../romulu/controller/lib/libdance/src/:../../../romulu/controller/lib/libepci/src/:../lib/libefpak/src/:../lib/libespi/src/:../lib/libedma/src/:../lib/libuirq/src/u/:../lib/libebuf/src/:../lib/libpmem/src/u/:../../../tcc-0.9.27/ \
/usr/local/cuda/bin/nvprof -f -o /tmp/log_romulu /tmp/controller -lAPP 
fi
fi
echo $PRE

 # 

#nvprof -f -o /tmp/log_romulu 
#nvprof -f -o /tmp/log_romulu cuda-memcheck  cuda-memcheck /users/ponsard/nv-nsight-cu-cli 