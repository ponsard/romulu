#ifndef __ROMULU_NVIDIA_H_INCLUDED__
#define __ROMULU_NVIDIA_H_INCLUDED__

typedef struct _cuda_gpu_ctx {
  CUdevice cuDevice;
  CUcontext cuContext;
  // cudaStream_t 		s1;
  // cudaStream_t 		s2;
} cuda_gpu_ctx_t;

extern volatile int statusFlag;
extern volatile int *pstatusFlag;
extern volatile int gpustate;

extern "C" void release_nvidia_gpu_context();
extern "C" void saveDataInFile(const char *fname, void *buffer, size_t len);
extern "C" void allocate_gpu_nvidia_memory();
extern "C" void init_nvidia_cuda_gpu_device();
extern "C" void select_cpu(int dir);
extern "C" void set_cpu(int);
extern "C" void *cuda_gpu_compute_mainloop(void *data);

/////////////////////////////////
// CUDA error
#define cudaCheckErrors(msg)                                                   \
  do {                                                                         \
    cudaError_t __err = cudaGetLastError();                                    \
    if (__err != cudaSuccess) {                                                \
      fprintf(stderr, "Fatal error: %s (%s at %s:%d)\n", msg,                  \
              cudaGetErrorString(__err), __FILE__, __LINE__);                  \
      fprintf(stderr, "*** FAILED - ABORTING\n");                              \
      exit(1);                                                                 \
    }                                                                          \
  } while (0)

#endif
