#ifndef __ROMULU_GDRCOPY_H_INCLUDED__
#define __ROMULU_GDRCOPY_H_INCLUDED__

#define GPU_PAGE_SHIFT 16
#define GPU_PAGE_SIZE (1UL << GPU_PAGE_SHIFT)
#define GPU_PAGE_OFFSET (GPU_PAGE_SIZE - 1)
#define GPU_PAGE_MASK (~GPU_PAGE_OFFSET)
#define PAGE_SHIFT 12
#define PAGE_SIZE (1UL << PAGE_SHIFT)
#define PAGE_MASK (~(PAGE_SIZE - 1))

extern void *local_nvidia_gdrcopy_backend_receiver(void *arg);
extern void *map_nvidia_gpu_memory(int totalSize, void *gpu_addr);
extern int gdr_copy_to_bar(void *gpubar_ptr, const void *cpumem_ptr,
                           size_t size);

#endif
