/*
 *
 *
 * first implementation :
 * 	- eth/siw to gpu device memory
 * 	-            cpu memory
 *
 *  - eth/RoCEv2
 *
 *
 *
 */

#include <arpa/inet.h>
#include <errno.h>
#include <fcntl.h>
#include <getopt.h>
#include <netdb.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <sys/mman.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
typedef unsigned short __be16;
#include <rdma/rsocket.h>

#include "librashpa.h"
#include "romulu_gdrcopy.h"
#include "romulu_rsocket.h"

union rsocket_address {
  struct sockaddr sa;
  struct sockaddr_in sin;
  struct sockaddr_in6 sin6;
  struct sockaddr_storage storage;
};
static char sPort[REMOTE_HOST_PORT_MAX];

static int server_listen() {
  struct addrinfo hints, *res;
  int ret, rs;

  memset(&hints, 0, sizeof hints);
  hints.ai_flags = RAI_PASSIVE;
  ret = getaddrinfo(NULL, sPort, &hints, &res);
  if (ret) {
    printf("getaddrinfo failed: %s\n", gai_strerror(ret));
    return ret;
  }

  rs = rsocket(res->ai_family, res->ai_socktype, res->ai_protocol);
  if (rs < 0) {
    printf("---rsocket failed\n");
    ret = rs;
    goto free;
  }

  ret = 1;
  ret = rsetsockopt(rs, SOL_SOCKET, SO_REUSEADDR, &ret, sizeof ret);
  if (ret) {
    printf("rsetsockopt failed\n");
    goto close;
  }

  ret = rbind(rs, res->ai_addr, res->ai_addrlen);
  if (ret) {
    printf("rbind failed\n");
    goto close;
  }

  ret = rlisten(rs, 1);
  if (ret) {
    printf("rlisten failed\n");
    goto close;
  }

  ret = rs;
  goto free;

close:
  rclose(rs);
free:
  freeaddrinfo(res);
  return ret;
}

void *nvidia_rsocket_backend_receiver(void *arg) {
  int lrs, rs;
  union rsocket_address rsa;
  socklen_t len;

  printf("STARTING  nvidia_rsocket_backend_receiver\n");
  char gdrname[256];

  rashpa_ctx_t *link = &g_rashpa;

  uint32_t port = link->port;
  void *gpu_addr = link->dest_addr;
  uint32_t _size = link->dsSize;
  pid_t pid = link->computeProcessPid;
  pid_t nimages = link->nImages;

  snprintf(sPort, REMOTE_HOST_PORT_MAX, "%d", port);
  lrs = server_listen();
  if (lrs < 0) {
    printf("rlisten error (there is another socket ?)\n");
    return NULL;
  }
  len = sizeof rsa;
  rs = raccept(lrs, &rsa.sa, &len);
  printf("raccept size=%d gpu_addr %p\n", _size, gpu_addr);

  strcpy(gdrname, "/dev/mem");
  int fd_mem = open(gdrname, O_RDWR | O_SYNC);
  if (fd_mem == -1) {
    printf("accessing %s requires cap_dac_override+ep cap_sys_rawio+ep %p\n",
           gdrname, gpu_addr);
    return NULL;
  }

  size_t size = (_size + GPU_PAGE_SIZE - 1) & GPU_PAGE_MASK;
  size_t rounded_size = (size + PAGE_SIZE - 1) & PAGE_MASK;
  // TODO Check this rounded_size
  void *bar_ptr = mmap(0, rounded_size, PROT_READ | PROT_WRITE, MAP_SHARED,
                       fd_mem, (uint64_t)gpu_addr);
  if (bar_ptr == MAP_FAILED) {
    printf("ERROR OPENING GPU MEMORY REGION\n");
    return NULL;
  }

  // FIXME : for CPU memory access

  // int fd = open("data", O_RDWR|O_SYNC,S_IRWXU);
  // void* file_addr = mmap(0,nimages*_size, PROT_READ | PROT_WRITE,
  // MAP_SHARED,fd ,0 );
  // fd = open("/hugepages/hugepagefile", O_CREAT | O_RDWR, 0755);
  //#define LENGTH (4UL*1024*1024*1024)
  //#define PROTECTION (PROT_READ | PROT_WRITE)
  // addr = mmap(ADDR, LENGTH, PROTECTION, FLAGS, fd, 0);

  int offset = 0, off = 0, i;
  for (i = 0; i < nimages; i++) {
    for (offset = 0; offset < _size;) {
      int len = rrecv(rs, bar_ptr + offset, _size - offset, 0);
      // int len 	= rrecv(rs, file_addr + off + offset, _size - offset,
      // 0);
      offset += len;
      printf("received data %d %d size %d\n", offset, len, _size);
    }
    // gdr_copy_to_bar(buf_ptr , file_addr+off, _size);
    kill(pid, SIGUSR1);
    off += _size;
    offset = 0;
  }
  rshutdown(rs, SHUT_RDWR);
  rshutdown(lrs, SHUT_RDWR);
  rclose(lrs);
  rclose(rs);
  // printf("end of  backend_receiver\n");
  return NULL;
}
