/*
 *
 * gpudirect/nvpeermem
 * nvidia/mellanox
 *
 * 
 */

#include <arpa/inet.h>
#include <fcntl.h>
#include <iostream>
#include <malloc.h>
#include <math.h>
#include <numa.h>
#include <sched.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>

#include <cuda.h>
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>

#include "librashpa.h"
#include "rashpa_main.h"
#include "romulu.h"
#include "romulu_jf.h"
#include "romulu_nvidia.h"

#define A_C_RED "\x1b[31m"
#define A_C_RESET "\x1b[0m"

cuda_gpu_ctx_t g_cuda_gpu;
CUdeviceptr d_statusFlag,d_storeNum;

extern volatile int statusFlag;
extern volatile int *pstatusFlag;
extern volatile int storeNUM;
extern volatile int *pstoreNUM;

size_t double_buffer_size;

// compute using cum-sum
#define SCANKERNEL 0
// compute counting using attomicAdd in shared memory
#define USESHARED 1
// try scan algo included in kernel
#define TSTKERNEL 0
// OpenMP cpu implementation
#define USEOMP 0
// single kernel launch
#define DYNAMIC_PARALLELISM 0
// veto algorithm
#define VETO 0
// pyFAI azimuthal integration
#define PYFAI 0

#define SINGLE  0

////////////////////////////////////////////////////////////////////////////////
// some cuda kernel :
////////////////////////////////////////////////////////////////////////////////
typedef unsigned int scan_type;
#include "persistent.cu"
#include "scan.cu"

#if USEOMP
#include "cpukernel.cu"
#endif

#if PYFAI
#include "pyFAI_kernel.cu"
#endif

#include "kernel.cu"
///////////////////// END OF GPU KERNEL SAMPLES ///////////////////////////////
///////////////////////////////////////////////////////////////////////////////

void release_nvidia_gpu_context() //
{
  cuda_gpu_ctx_t *gpu = &g_cuda_gpu;
  rashpa_ctx_t *link = &g_rashpa;
  int res;

  debug_printf("release_nvidia_gpu_context\n");

  if ((
       (link->link_type == RASHPA_LINK_ROCEV2_UC)) &&
      (link->receiver_type == RASHPA_RECEIVER_NVIDIA_CUDA_GPU)) //
  {
    debug_printf("cuMemFree\n");
    cuMemFree((CUdeviceptr)link->dest_addr);
    cudaCheckErrors("cuMemFree");

  } else {
    debug_printf("Host memory Free\n");
    cudaHostUnregister(link->dest_addr);
    cudaCheckErrors("cudaHostUnregister");
    res = munmap(link->dest_addr, double_buffer_size);
    res = unlink("/dev/hugepages/rashpadata");
  }
  // printf("cudaHostUnregister destaddr\n");
  // cudaHostUnregister(link->dest_addr);
  cudaHostUnregister((void *)pstatusFlag);
  cudaCheckErrors("cudaHostUnregister");
  cudaHostUnregister((void *)pstoreNUM);
  cudaCheckErrors("cudaHostUnregister");
  debug_printf("cudaFreeHost output1\n");
  cudaFreeHost((void *)link->h_output1);
  cudaCheckErrors("cudaFreeHost fail");
  cudaFreeHost((void *)link->h_output2);
  cudaCheckErrors("cudaFreeHost fail");
  cudaFreeHost((void *)link->h_output11);
  cudaCheckErrors("cudaFreeHost fail");
  cudaFreeHost((void *)link->h_output22);
  cudaCheckErrors("cudaFreeHost fail");
}

void init_nvidia_cuda_gpu_device() {
  rashpa_ctx_t *link = &g_rashpa;
  cuda_gpu_ctx_t *gpu = &g_cuda_gpu;

  cudaSetDevice(link->gpuId);
  cuDeviceGet(&gpu->cuDevice, link->gpuId);
  cudaCheckErrors("cuDeviceGet fail");

  char name[128];
  cuDeviceGetName(name, sizeof(name), gpu->cuDevice);
  debug_printf("init_nvidia_cuda_gpu_device pid = %d, dev = %d device name = [%s]\n",
         getpid(), gpu->cuDevice, name);

  int prop = -1;
  cuDeviceGetAttribute(&prop, CU_DEVICE_ATTRIBUTE_CAN_USE_STREAM_MEM_OPS,
                       gpu->cuDevice);
  if (prop == 1)
  {
    debug_printf("CU_DEVICE_ATTRIBUTE_CAN_USE_STREAM_MEM_OPS enable\n");
  }
  else {
    debug_printf("Cuda CU_DEVICE_ATTRIBUTE_CAN_USE_STREAM_MEM_OPS not supported, Required for RDMA synchro. "
           "try moprobe nvidia NVreg_EnableStreamMemOPs=1\n");
    exit(0);
  }

  // to enable non blocking access to statusFlag
  // cudaSetDevice(link->gpuId);
  cuCtxCreate(&gpu->cuContext, CU_CTX_MAP_HOST, gpu->cuDevice);

#if SCANKERNEL
  debug_printf("compiled with SCAN GPU code\n");
#endif
#if USESHARED
  debug_printf("compiled with atomic add in SHARED code\n");
#endif
}

/**
 *
 * allocate_gpu_device_memory: map gpu memory for DMA
 *
 * @size : in bytes
 * @devID : cuda device id
 *
 *
 * called from rashpa_rocev2_uc_backend_receiver_init (start_IB)
 */

// CHECKME handle GPU larger than a single image
#define COMPUTE_SIZE                                                           \
  debug_printf(                                                                \
      "gpu_width %d gpu_height %d in_bdepth %d out_bdepth %d rashpa_bunch "    \
      "%d nImage %d nIter %d\n",                                               \
      gpu_width, gpu_height, in_bdepth, out_bdepth, rashpa_bunch, nImages,     \
      nIter);                                                                  \
  if (gpucode == 2) /*JF gain/pedestal+HDR*/                                   \
  {                                                                            \
    inhdr_imgSize = 2 * 128 * 8246 * 1; /* 2*128*(8192+54)*1	*/                \
    in_imgSize = 2 * 512 * 1024 * 2;                                           \
    out_imgSize = 1 * 512 * 1024 * 4;                                          \
    double_buffer_size = inhdr_imgSize * rashpa_bunch;                         \
    /* buffers of bunch x batch images * NMODULE */                            \
    debug_printf("JF data with header 2x128x8246\n"                            \
                 "inhdr_imgSize %u in_imgSize %u out_imgSize %u "              \
                 "double_buffer_size %u \n",                                   \
                 inhdr_imgSize, in_imgSize, out_imgSize, double_buffer_size);  \
  } else if (gpucode == 1 || gpucode == 10 ||                                  \
             gpucode == 11) /* gain/pedestal */                                \
  {                                                                            \
    inhdr_imgSize = 0;                                                         \
    /*in_imgSize  		= width * height * in_bdepth ;*/                           \
    /*out_imgSize 		= width * height/2 * out_bdepth;*/                         \
    in_imgSize = gpu_width * 2 * gpu_height *                                  \
                 in_bdepth; /*2*gpu_h for pedestal, gpu_h for kernel*/         \
    out_imgSize = gpu_width * gpu_height * out_bdepth;                         \
    double_buffer_size = in_imgSize * rashpa_bunch;                            \
    debug_printf("image+pedestal allocation w/o header\n"                      \
                 "inhdr_imgSize %u in_imgSize %u out_imgSize %u "              \
                 "double_buffer_size %u\n",                                    \
                 inhdr_imgSize, in_imgSize, out_imgSize, double_buffer_size);  \
  } else {                                                                     \
    inhdr_imgSize = 0;                                                         \
    in_imgSize = gpu_width * gpu_height * in_bdepth;                           \
    out_imgSize = gpu_width * gpu_height * out_bdepth;                         \
    double_buffer_size = in_imgSize * rashpa_bunch;                            \
    debug_printf("Regular case\n"                                              \
                 "inhdr_imgSize %u in_imgSize %u out_imgSize %u "              \
                 "double_buffer_size %u \n",                                   \
                 inhdr_imgSize, in_imgSize, out_imgSize, double_buffer_size);  \
  }

void allocate_gpu_nvidia_memory() {
  rashpa_ctx_t *link = &g_rashpa;
  cuda_gpu_ctx_t *gpu = &g_cuda_gpu;

  int gpu_width = link->gpu_w;
  int gpu_height = link->gpu_h;

  int width = link->roi_w[0];
  int height = link->roi_h[0];

  int in_bdepth = link->bdepth;
  int out_bdepth = sizeof(float);
  int rashpa_bunch = link->rashpa_bunch;
  int nImages = link->nImages;
  int nIter = link->nIter;
  int gpucode = link->gpucode;
  pstatusFlag = &statusFlag;
  pstoreNUM = &storeNUM;

#define NMODULES 1 // 8 for large GPU

  debug_printf("allocate_gpu_nvidia_memory\n"
         "gpu_width %d, gpu_height %d, width %d, height %d, in_bdepth %d, "
         "out_bdepth "
         "%d, rashpa_bunch %d, nImages %d, nIter %d, gpucode %d\n",
         gpu_width, gpu_height, width, height, in_bdepth, out_bdepth,
         rashpa_bunch, nImages, nIter, gpucode);

  size_t in_imgSize, inhdr_imgSize, out_imgSize; // output buffer
  // double_buffer_size; //device memory

  COMPUTE_SIZE;

  /**
   *
   * input buffer for bunch images
   * in pinned memory or GPU memory if RDMA gpuDirect
   *
   */

  // 2 bank of memory
  // GPUDirect : in device memory
  if ((
       link->link_type == RASHPA_LINK_ROCEV2_UC) &&
      link->receiver_type != RASHPA_RECEIVER_NVIDIA_CUDAMEM_GPU) {
    CUdeviceptr d_A;
    cuMemAlloc(&d_A, double_buffer_size * NMODULES);
    cudaCheckErrors("cuMemAlloc fail");
    link->dest_addr = (void *)d_A;
    cuMemHostGetDevicePointer(&d_statusFlag, (void *)pstatusFlag, 0);
    cudaCheckErrors("cuMemHostGetDevicePointer");
    cuMemHostGetDevicePointer(&d_storeNum, (void *)pstoreNUM, 0);
    cudaCheckErrors("cuMemHostGetDevicePointer");
    debug_printf(
        A_C_GRN
        "GPUDirect/RDMA GPU, using GPU memory (%d Bytes at %p)\n" A_C_RESET,
        double_buffer_size * NMODULES, link->dest_addr);
  } else { // in host memory
    char devname[128] = "/dev/hugepages/rashpadata";

    int fd = open(devname, O_RDWR | O_CREAT, 0755);
    if (fd < 0) {
      perror("/dev/hugepages/rashpadata Open failed");
      exit(1);
    }
    link->dest_addr =
        mmap(NULL, double_buffer_size * NMODULES, PROT_READ | PROT_WRITE,
             MAP_SHARED | MAP_HUGETLB, fd, 0);

    if (link->dest_addr == MAP_FAILED) {
      perror("huge mmap");
      unlink("/dev/hugepages/rashpadata");
      exit(1);
    }

    // link->dest_addr = numa_alloc_local(double_buffer_size*NMODULES);
    // printf("pinning destination memory make cuMemcpyAsync overlap\n");

    cudaHostRegister(link->dest_addr, double_buffer_size * NMODULES,
                     cudaHostRegisterPortable | cudaHostRegisterMapped); 
    // cudaHostRegisterMapped|
    // cudaHostRegisterIoMemory|
    cudaCheckErrors("cudaHostRegister");

    debug_printf(A_C_GRN "RDMA Input buffer in CPU pinned memory (%d Bytes x "
                         "nmodules %d at %p)\n" A_C_RESET,
                 double_buffer_size, NMODULES, link->dest_addr);
  }

  if (link->savePinnedBuf == 3) {
    if (gpucode == 2) {
      debug_printf("h_output (JF) %d\n", nImages / 2 * inhdr_imgSize * NMODULES);
      cudaMallocHost((void **)&link->h_output1,
                     nImages / 2 * inhdr_imgSize * NMODULES);
      cudaMallocHost((void **)&link->h_output2,
                     nImages / 2 * inhdr_imgSize * NMODULES);
      cudaCheckErrors("cudaMallocHost fail");
    } else {
      debug_printf("h_output (in) %d\n", nImages * in_imgSize * NMODULES);
      cudaMallocHost((void **)&link->h_output1,
                     nImages * in_imgSize * NMODULES);
      cudaMallocHost((void **)&link->h_output2,
                     nImages * in_imgSize * NMODULES);
      cudaCheckErrors("cudaMallocHost fail");
    }
  } else {
    debug_printf("h_output (out) %ld nImages/2 %d out_imgSize %d\n",
           nImages / 2 * out_imgSize, nImages / 2, out_imgSize);
    cudaMallocHost((void **)&link->h_output1,
                   (nImages / rashpa_bunch + 1) * rashpa_bunch / 2 *
                       out_imgSize * NMODULES); // issue if bunch not divisor of
                                                // nImages saveoption mode 24
    cudaMallocHost((void **)&link->h_output2,
                   (nImages / rashpa_bunch + 1) * rashpa_bunch / 2 *
                       out_imgSize * NMODULES); //
    cudaCheckErrors("cudaMallocHost fail");
  }

  cudaMallocHost((void **)&link->h_output11, nIter * nImages / 2 * sizeof(int));
  cudaMallocHost((void **)&link->h_output22, nIter * nImages / 2 * sizeof(int));
  cudaCheckErrors("cudaMallocHost fail");
  cudaMemset(link->h_output11, 0, nIter * nImages / 2 * sizeof(int));
  cudaMemset(link->h_output22, 0, nIter * nImages / 2 * sizeof(int));
  cudaCheckErrors("cudaMemset fail");

  cudaHostRegister((void *)&statusFlag, 4,
                   cudaHostRegisterPortable | cudaHostRegisterMapped);
  cudaCheckErrors("cudaHostRegister fail");
  cudaHostRegister((void *)&storeNUM, 4,
                   cudaHostRegisterPortable | cudaHostRegisterMapped);
  cudaCheckErrors("cudaHostRegister fail");
  cuMemHostGetDevicePointer(&d_statusFlag, (void *)&statusFlag, 0);
  debug_printf("cuMemHostRegister statusFlag %p d_statusFlag %p\n", &statusFlag,
         d_statusFlag);
  cudaCheckErrors("cuMemHostRegister fail");
  cuMemHostGetDevicePointer(&d_storeNum, (void *)&storeNUM, 0);
  debug_printf("cuMemHostRegister statusFlag %p d_statusFlag %p\n", &statusFlag,
         d_statusFlag);
  cudaCheckErrors("cuMemHostRegister fail");
  #if __CUDA_VERSION > 101
  // cudaGraph_t graph;
  // cudaGraphExec_t instance;
  #endif
}

/**
 *
 * launch_gpu_compute: thread code executing cuda kernel when RDMA data
 * available
 *
 * @data : struct with relevant info
 *
 */
void *cuda_gpu_compute_mainloop(void *data) {
  debug_printf("cuda_gpu_compute_mainloop version %s %s\n",__DATE__,__TIME__);

  // select_cpu (RASHPA_BACKENDGPU);
  set_cpu(9);
  struct sched_param param;
  param.sched_priority = 99;
  sched_setscheduler(0, SCHED_FIFO, &param);

  rashpa_ctx_t *link = &g_rashpa;
  cuda_gpu_ctx_t *gpu = &g_cuda_gpu;

  int gpu_width = link->gpu_w;
  int gpu_height = link->gpu_h;
  int width = link->roi_w[0];
  int height = link->roi_h[0];
  int in_bdepth = link->bdepth;
  int out_bdepth = sizeof(float);
  int nImages = link->nImages;
  int nIter = link->nIter;
  int rashpa_bunch = link->rashpa_bunch;
  int gpucode = link->gpucode;
  int pixnum = gpu_width * gpu_height;

  debug_printf("cuda_gpu_compute_mainloop\n"
         "gpu_width %d, gpu_height %d, width %d, height %d, in_bdepth %d, "
         "out_bdepth "
         "%d, rashpa_bunch %d, nImages %d, nIter %d, gpucode %d pixnum %d\n",
         gpu_width, gpu_height, width, height, in_bdepth, out_bdepth,
         rashpa_bunch, nImages, nIter, gpucode,pixnum);

  cuCtxSetCurrent(gpu->cuContext);
  cudaCheckErrors("cuCtxSetCurrent fail");

  size_t inhdr_imgSize, in_imgSize, out_imgSize, h2dsize, double_buffer_size;
  void *d_In1, *d_In2, *d_In1bis, *d_In2bis, *d_InA, *d_InB, *d_InAbis,
      *d_InBbis, *d_Out1, *d_Out2, *d_OutA, *d_OutB, *d_Out11, *d_Out22,
      *jfLut1, *jfLut2;

  float *gainMap, *dGainMap,               // Jungfrau correction matrix
      *d_csrval, *pyfai_coef, *dpyfai_coef // pyFAI calibration matrix
      ;

  unsigned int peak, *peakNum1, *peakNum2, *d_indicesa, *d_indptra, *d_indicesb,
      *d_indptrb, *d_boolsa, *d_scana, *block_resultsa, *dummy_resultsa,
      *d_boolsb, *d_scanb, *block_resultsb, *dummy_resultsb;

  int k_w, k_h;
  unsigned int *L;

  // THIS IS A MACRO
  COMPUTE_SIZE;
  if (gpucode != 10 && gpucode != 11) {
    //*2 for double image
    cudaMalloc(&d_In1, rashpa_bunch / 2 * in_imgSize);
    cudaMalloc(&d_In2, rashpa_bunch / 2 * in_imgSize);
    cudaMalloc(&d_In1bis, rashpa_bunch / 2 * inhdr_imgSize);
    cudaMalloc(&d_In2bis, rashpa_bunch / 2 * inhdr_imgSize);
    cudaCheckErrors("cudaMalloc fail");
    cudaMemset(d_In1, 0, rashpa_bunch / 2 * in_imgSize);
    cudaMemset(d_In2, 0, rashpa_bunch / 2 * in_imgSize);
    cudaMemset(d_In1bis, 0, rashpa_bunch / 2 * inhdr_imgSize);
    cudaMemset(d_In2bis, 0, rashpa_bunch / 2 * inhdr_imgSize);
    cudaCheckErrors("cudaMemset fail");

    cudaMalloc(&d_Out1, rashpa_bunch / 2 * out_imgSize);
    cudaMalloc(&d_Out2, rashpa_bunch / 2 * out_imgSize);
    cudaMalloc(&d_Out11, rashpa_bunch / 2 * out_imgSize);
    cudaMalloc(&d_Out22, rashpa_bunch / 2 * out_imgSize);
    cudaCheckErrors("cudaMalloc fail");
    cudaMemset(d_Out1, 0, rashpa_bunch / 2 * out_imgSize);
    cudaMemset(d_Out2, 0, rashpa_bunch / 2 * out_imgSize);
    cudaMemset(d_Out11, 0, rashpa_bunch / 2 * out_imgSize);
    cudaMemset(d_Out22, 0, rashpa_bunch / 2 * out_imgSize);
    cudaCheckErrors("cudaMemset fail");

    // for the parallel scan
    cudaMalloc(&d_boolsa,
               rashpa_bunch / 2 * sizeof(int) *
                   (2*pixnum + rashpa_bunch)); // pixnum
    cudaMalloc(&d_scana,
               rashpa_bunch / 2 * sizeof(int) * (pixnum + rashpa_bunch));
    cudaCheckErrors("cudaMalloc fail");

    cudaMalloc(&block_resultsa, sizeof(int) * (pixnum + rashpa_bunch));
    cudaMalloc(&dummy_resultsa, sizeof(int) * (pixnum + rashpa_bunch));
    cudaCheckErrors("cudaMalloc fail");

    // accumulate  csrvalue //CHECKME / 10 ??????
    cudaMalloc(&d_csrval,
               (nImages + rashpa_bunch) * sizeof(float) * out_imgSize / 10);
    cudaCheckErrors("cudaMalloc fail");

    cudaMalloc(&d_indicesa, sizeof(int) * (pixnum + rashpa_bunch));
    cudaMalloc(&d_indptra, sizeof(int) * width);
    cudaCheckErrors("cudaMalloc fail");

    cudaMalloc(&d_boolsb,
               rashpa_bunch / 2 * sizeof(int) * (2*pixnum + rashpa_bunch));
    cudaCheckErrors("cudaMalloc fail");

    cudaMalloc(&d_scanb,
               rashpa_bunch / 2 * sizeof(int) * (pixnum + rashpa_bunch));
    cudaMalloc(&block_resultsb, sizeof(int) * (pixnum + rashpa_bunch));
    cudaMalloc(&dummy_resultsb, sizeof(int) * (pixnum + rashpa_bunch));
    cudaMalloc(&d_indicesb, sizeof(int) * (pixnum + rashpa_bunch));
    cudaMalloc(&d_indptrb, sizeof(int) * width);
    cudaCheckErrors("cudaMalloc fail");

    cudaMalloc(&L, 4);
    cudaCheckErrors("cudaMalloc fail");
    cudaMemset(L, 0, 4);
    cudaCheckErrors("cudaMemset fail");
  }

  // CHECKME hard coded for parallel scan
  int interval_size, num_groups;

  if (width == 512) { // 512x1024;
    interval_size = 2304;
    num_groups = 228;
  } else { // 2048x2048
    interval_size = 17664;
    num_groups = 238;
  }

  cudaCheckErrors("cudaMalloc fail");

  if (gpucode == 1 || gpucode == 2 || gpucode == 10 || gpucode == 11) {
    struct stat st;
    size_t res, lenGainMap = 0;

    stat(link->gain_file, &st);
    lenGainMap = st.st_size;
    debug_printf("open gainmap %s %d %d %d %d\n", link->gain_file, lenGainMap,
           out_imgSize, gpu_width, gpu_height);
    gainMap = (float *)malloc(st.st_size);

    if (gainMap == NULL) {
      printf("error malloc gainmap\n");
      return NULL;
    }
    int f = open(link->gain_file, O_RDONLY, S_IRWXU);
    if (f == -1) {
      printf(A_C_RED "error loading %s\n" A_C_RESET, link->gain_file);
      return NULL;
    }
    res = 0;

    do {
      res += read(f, (void *)(((uint64_t)gainMap) + res), lenGainMap - res);
      debug_printf("gainmap res %d / %d\n", res, lenGainMap);
    } while (res < lenGainMap);
    debug_printf("first gain %f %f %f\n", *(gainMap + out_imgSize / 4),
           *(gainMap + 2 * out_imgSize / 4), *(gainMap + 3 * out_imgSize / 4));
    close(f);
#if PYFAI == 1
    int lenPyFAIcoef = 1024 * 512 * 3000;
    res = 0;
    f = open("/tmp/pyfaicalib.data", O_RDONLY, S_IRWXU);
    if (f == -1) {
      printf(A_C_RED "error loading %s\n" A_C_RESET, "/tmp/pyfaicalib.data");
      return NULL;
    }
    do {
      res +=
          read(f, (void *)(((uint64_t)pyfai_coef) + res), lenPyFAIcoef - res);
      debug_printf("loading pyFAI coef %d / %d\n", res, lenPyFAIcoef);
    } while (res < lenPyFAIcoef);
    cudaMalloc(&dpyfai_coef, lenPyFAIcoef);
    cudaMemcpy(dpyfai_coef, pyfai_coef, lenPyFAIcoef, cudaMemcpyHostToDevice);
    cudaCheckErrors("cudaMemcpy fail");
#endif

    if (gpucode == 2) {
      cudaMalloc(&jfLut1, rashpa_bunch / 2 * 512 * sizeof(char));
      cudaMalloc(&jfLut2, rashpa_bunch / 2 * 512 * sizeof(char));
    }
    if (gpucode != 10 && gpucode != 11) {
      cudaMalloc(&peakNum1, nIter * nImages * 2 * sizeof(int));
      cudaMalloc(&peakNum2, nIter * nImages * 2 * sizeof(int));
      cudaMemset(peakNum1, 0, nIter * nImages * sizeof(int));
      cudaMemset(peakNum2, 0, nIter * nImages * sizeof(int));
      cudaCheckErrors("cudaMemset fail");

      cudaMalloc(&dGainMap, lenGainMap);
      cudaMemcpy(dGainMap, gainMap, lenGainMap, cudaMemcpyHostToDevice);
      cudaCheckErrors("cudaMemcpy fail");
    }
  }
  // streams H2D K D2H overlapping
  cudaStream_t s1, s2, s3;
  cudaStreamCreate(&s1);
  cudaStreamCreate(&s2);
  cudaStreamCreate(&s3);
  cudaCheckErrors("cudaStreamCreate fail");

  link->nImgOnGpu = 0;
  // consider warmup, but may require correctly configured dataset

  size_t offset = 0, offset2 = 0, peakOffset = 0;

  debug_printf("CUDA_GPU_compute_mainloop waiting for a total of %d bunch %d\n",
               nImages * nIter, rashpa_bunch);

#ifdef PERSISTENT
  // printf("cpu totalNN %d\n",totalN);
  // resident kernel
  if (gpucode == 3) {
#include "host4persistent.cu"
  }
#endif

  // lock used for synchro with rdma receiver thread

  *pstatusFlag = -1; 
  *pstoreNUM = 0; //where store the keept data

  if (link->receiver_type == RASHPA_RECEIVER_NVIDIA_CUDAMEM_GPU) {
    if (gpucode == 2) // JF with header for UDP
    {

      d_InAbis = d_In1bis;
      d_InBbis = d_In2bis;
      h2dsize = inhdr_imgSize;
      k_w = 1024;
      k_h = 512;
      debug_printf("buffer for JF setup in cpu memory/header removal h2dsize %d\n",
             h2dsize);
    }
    if (gpucode == 1 || gpucode == 10 || gpucode == 11) // JF pedestal only
    {
      d_InAbis = d_In1;
      d_InBbis = d_In2;
      h2dsize = in_imgSize;
      k_w = gpu_width;
      k_h = gpu_height;
      debug_printf("buffer in cpu pinned memory width %d height %d h2d %d\n", k_w,
             k_h, h2dsize);
    }

    d_InA = d_In1;
    d_OutA = d_Out11;
    d_InB = d_In2;
    d_OutB = d_Out22;
  } else // (GPUDirect, no header)
  {
    debug_printf("GPUDIRECT buffer %d\n", in_imgSize);
    d_InA = (void *)(((uint64_t)link->dest_addr));
    d_OutB = d_Out22;
    d_InB = (void *)(((uint64_t)link->dest_addr) + double_buffer_size / 2);
    d_OutA = d_Out11;
    // CHECK THIS
    k_w = gpu_width;
    if (gpucode == 1)
      k_h = gpu_height;
    else
      k_h = height;
  }

  /***********************************
   *
   * main loop
   *
   ***********************************
   */
  printf("romulu_nvidia version %s %s\n",__DATE__,__TIME__);

  // DO NOT CHANGE *2 & +2
  // do not change i += 2 this is required to compute waitvalue lock
  for (int i = 0; i * rashpa_bunch < 2 * nImages * nIter; i += 2) {
    // debug_printf("V1.1.0 in gpu loop %d offset %d nImgOnGpu %d  \n",i,offset,link->nImgOnGpu);

#if USEOMP
    // while((*pstatusFlag) < (KERNELREADY+i));
    // *(volatile int*)pstatusFlag = -1;
    debug_printf("cpuapplyGainPedestalandCount11 starting %d\n", link->nImgOnGpu);

    cpuapplyGainPedestalandCount((uint16_t *)link->dest_addr,
                                 (float *)link->h_output1, gainMap,
                                 (unsigned int *)link->h_output11, k_w, k_h,
                                 i / 2); // Work with bunch=2
    link->nImgOnGpu = nImages * nIter;
    printf("cpuapplyGainPedestalandCount11 done %d\n", link->nImgOnGpu);
    i = 2 * nImages * nIter / rashpa_bunch + 1;
#else

    // get data from buffer
    if ((
         link->link_type == RASHPA_LINK_ROCEV2_UC) &&
        link->receiver_type != RASHPA_RECEIVER_NVIDIA_CUDAMEM_GPU) {
      // printf("GPUDirect\n");

      if (link->savePinnedBuf == 12) {
        if ((i / 2) % 2)
          cudaMemcpyAsync((void *)(((uint64_t)link->h_output1) +
                                   offset * rashpa_bunch / 2 * h2dsize),
                          (void *)d_InAbis, rashpa_bunch / 2 * h2dsize,
                          cudaMemcpyDeviceToHost, s3);
        else
          cudaMemcpyAsync((void *)(((uint64_t)link->h_output2) +
                                   offset * rashpa_bunch / 2 * h2dsize),
                          (void *)d_InBbis, rashpa_bunch / 2 * h2dsize,
                          cudaMemcpyDeviceToHost, s3);
        cudaCheckErrors("cudaMemcpyAsync fail");
      }
    } else if (gpucode != 10 && gpucode != 11) {
      // H1 H2
      // printf("in H1 h2dsize %d\n",h2dsize);

      cuStreamWaitValue32(s3, d_statusFlag, KERNELREADY + i,
                          CU_STREAM_WAIT_VALUE_GEQ);
      cudaCheckErrors("cuStreamWaitValue32 fail");
      cudaMemcpyAsync(d_InAbis, (void *)((uint64_t)link->dest_addr),
                      rashpa_bunch / 2 * h2dsize, cudaMemcpyHostToDevice, s3);
      cudaCheckErrors("cudaMemcpyAsync fail");
      // cuStreamWaitValue32(
      // 	s3,
      // 	d_statusFlag, KERNELREADY+i,
      // 	CU_STREAM_WAIT_VALUE_GEQ);
      // cudaCheckErrors("cuStreamWaitValue32 fail");
      // ugly check if we are on time ; will block here....
      cuStreamWriteValue32(s3, d_statusFlag, -1, CU_STREAM_WRITE_VALUE_DEFAULT);
      cudaCheckErrors("cuStreamWriteValue32 fail");

      if (link->savePinnedBuf == 11) {
        cudaMemcpyAsync((void *)(((uint64_t)link->h_output1) +
                                 offset * rashpa_bunch / 2 * h2dsize),
                        (void *)d_InAbis, rashpa_bunch / 2 * h2dsize,
                        cudaMemcpyDeviceToHost, s3);
        printf("copy to offset %d h2dsize %d\n", offset, h2dsize);
      }

      cuStreamWaitValue32(s3, d_statusFlag, KERNELREADY + i + 1,
                          CU_STREAM_WAIT_VALUE_GEQ);
      cudaCheckErrors("cuStreamWaitValue32 fail");
      cudaMemcpyAsync(d_InBbis,
                      (void *)(((uint64_t)link->dest_addr) +
                               double_buffer_size /
                                   2), //  rashpa_bunch/2*h2dsizeTESTING HERE
                      rashpa_bunch / 2 * h2dsize, cudaMemcpyHostToDevice, s3);
      cudaCheckErrors("cudaMemcpyAsync fail");
      cuStreamWriteValue32(s3, d_statusFlag, -1, CU_STREAM_WRITE_VALUE_DEFAULT);
      cudaCheckErrors("cuStreamWriteValue32 fail");

      if (link->savePinnedBuf == 11) {
        cudaMemcpyAsync((void *)(((uint64_t)link->h_output2) +
                                 offset * rashpa_bunch / 2 * h2dsize),
                        (void *)d_InBbis, rashpa_bunch / 2 * h2dsize,
                        cudaMemcpyDeviceToHost, s3);
        cudaCheckErrors("cudaMemcpyAsync fail");

        // printf("copy2 to offset %d\n",offset);
      }
    }
    /////////////////////
    // kernel computations K1
    // synchro
    cuStreamWaitValue32(s2, d_statusFlag, KERNELREADY + i,
                        CU_STREAM_WAIT_VALUE_GEQ);
    cudaCheckErrors("cuStreamWaitValue32 fail");

    if (gpucode == 2) // UDP + header
    {
      // printf("gpucode==2.1\n");
      // cudaMemsetAsync is blocking

      createLut<<<dim3(1, 1, rashpa_bunch / 2), dim3(1, 128, 1), 0, s2>>>(
          (char *)d_InBbis, (char *)jfLut1, 8246, 128);
      cudaCheckErrors("createLut fail");
      reorder<<<dim3(1, 128, rashpa_bunch), dim3(1024, 1, 1), 0, s2>>>(
          (uint16_t *)d_InBbis, (uint16_t *)d_InB, (char *)jfLut1, 1024, 512);
      cudaCheckErrors("reorder fail");
    }

    if ((gpucode == 1 || gpucode == 2)) {
// cudaMemsetAsync : seems to be be blocking on stream
// printf("in K1 k_h %d\n",k_h);

#if TSTKERNEL
      applyGainPedestalandCount2
#else
      applyGainPedestalandCount
#endif
          <<<dim3(k_w / 64, k_h / 16, rashpa_bunch / 2), dim3(64, 16, 1), 0,
             s2>>>((uint16_t *)d_InB, (float *)d_OutB, dGainMap, peakNum1, k_w,
                   k_h, offset2, d_boolsb);
      cudaCheckErrors("applyGainPedestalandCount fail");


#if DYNAMIC_PARALLELISM
        // printf("DYNAMIC_PARALLELISM");
        singleKernel<<<dim3(1, 1, 1), dim3(1, 1, 1), 0, s2>>>(
            (uint16_t *)d_InB, (float *)d_OutB, dGainMap, peakNum1, width,
            height, k_w, k_h, 0, //k|offset
            nImages, L, num_groups, interval_size,
            out_imgSize, (float *)d_OutB, d_scanb, d_boolsb, dummy_resultsb,
            block_resultsb,
            d_csrval, (int *)(((uint64_t)peakNum1))); //
        cudaCheckErrors("singleKernel fail");

#endif
#if SCANKERNEL
      for (int k = 0; k < rashpa_bunch / 2; k++)
      {
        scan_scan_intervals<<<dim3(num_groups, 1), dim3(WG_SIZE, 1, 1), 0,
                              s2>>>(d_boolsb + k * k_w * k_h, out_imgSize,
                                    interval_size, d_scanb, block_resultsb);
        cudaCheckErrors("scan_scan_intervals fail");

        scan_scan_intervals<<<dim3(1, 1), dim3(WG_SIZE, 1, 1), 0, s2>>>(
            block_resultsb, num_groups, interval_size, block_resultsb,
            dummy_resultsb);
        cudaCheckErrors("scan_scan_intervals fail");

        scan_final_update<<<dim3(num_groups, 1), dim3(WG_SIZE2, 1, 1), 0, s2>>>(
            d_scanb, out_imgSize, interval_size, block_resultsb);
        cudaCheckErrors("scan_final_update fail");

        scatter<<<dim3(k_w / 64, k_h / 16, 1), dim3(64, 16, 1), 0, s2>>>(
            k_w, k_h, d_csrval, (float *)d_OutB + k_w * k_h * k,
            d_boolsb + k * k_w * k_h, d_scanb, nImages, L,
            (int *)(((uint64_t)peakNum1) +
                    4 * (offset2 * rashpa_bunch / 2 + k)));
        cudaCheckErrors("scatter fail");
      }
#endif
    }
    // kernel computation K2
    // synchro
    cuStreamWaitValue32(s2, d_statusFlag, KERNELREADY + i + 1,
                        CU_STREAM_WAIT_VALUE_GEQ);
    cudaCheckErrors("cuStreamWaitValue32 fail");

    if (gpucode == 2) // UDP + header
    {
      // printf("gpucode==2.2\n");
      createLut<<<dim3(1, 1, rashpa_bunch / 2), dim3(1, 128, 1), 0, s2>>>(
          (char *)d_InAbis, (char *)jfLut2, 8246, 128);
      cudaCheckErrors("createLut fail");
      reorder<<<dim3(1, 128, rashpa_bunch), dim3(1024, 1, 1), 0, s2>>>(
          (uint16_t *)d_InAbis, (uint16_t *)d_InA, (char *)jfLut2, 1024, 512);
      cudaCheckErrors("reorder fail");
    }

    if ((gpucode == 1 || gpucode == 2)) {
      cudaCheckErrors("error before fail");

#if TSTKERNEL
      applyGainPedestalandCount2
#else
      applyGainPedestalandCount
#endif
          <<<dim3(k_w / 64, k_h / 16, rashpa_bunch / 2), dim3(64, 16, 1),
             0, // RoCE
             s2>>>((uint16_t *)d_InA, (float *)d_OutA, (float *)dGainMap,
                   peakNum2, k_w, k_h, offset2, d_boolsa);
      cudaCheckErrors("kernel fail");

#if DYNAMIC_PARALLELISM
        //printf("DYNAMIC_PARALLELISM %d\n",k);
        //      const uint16_t *data, float *outputImage, float *gain, unsigned int *gCount, 
        //      const int Width, const int Height, int k_w,int k_h, int k, 
        //      int nImages, unsigned int *L,int num_groups,int interval_size, 
        //      int out_imgSize, float *d_OutB, scan_type *d_scan, scan_type *d_bools, scan_type *dummy_results,
        //      scan_type *block_results, float *d_csrval, int *peaknum
        singleKernel<<<dim3(1, 1, 1), dim3(1, 1, 1), 0, s2>>>(
            (uint16_t *)d_InA, (float *)d_OutA, dGainMap, peakNum2, 
            width, height, k_w, k_h, 0, //k|offset
            nImages, L, num_groups, interval_size,
            out_imgSize, (float *)d_OutA, d_scana, d_boolsa, dummy_resultsa,
            block_resultsa,d_csrval, (int *)(((uint64_t)peakNum2))); //
        cudaCheckErrors("singleKernel2 fail");
#endif
#if SCANKERNEL
      for (int k = 0; k < rashpa_bunch / 2; k++) 
      {
        scan_scan_intervals<<<dim3(num_groups, 1), dim3(WG_SIZE, 1, 1), 0,
                              s2>>>(d_boolsa + k * k_w * k_h, out_imgSize,
                                    interval_size, d_scana, block_resultsa);
        cudaCheckErrors("scan_scan_intervals fail");

        scan_scan_intervals<<<dim3(1, 1), dim3(WG_SIZE, 1, 1), 0, s2>>>(
            block_resultsa, num_groups, interval_size, block_resultsa,
            dummy_resultsa);

        cudaCheckErrors("scan_scan_intervals fail");

        scan_final_update<<<dim3(num_groups, 1), dim3(WG_SIZE2, 1, 1), 0, s2>>>(
            d_scana, out_imgSize, interval_size, block_resultsa);
        cudaCheckErrors("scan_final_update fail");

        scatter<<<dim3(k_w / 64, k_h / 16, 1), dim3(64, 16, 1), 0, s2>>>(
            k_w, k_h, d_csrval, (float *)d_OutA + k * k_w * k_h,
            d_boolsa + k * k_w * k_h, d_scana, nImages, L,
            (int *)(((uint64_t)peakNum2) +
                    4 * (offset2 * rashpa_bunch / 2 + k)));
        cudaCheckErrors("scatter fail");
      }
#endif
    }

    ///////////////////////////////
    // D2 (D1) device to host copies
    // synchro
    cuStreamWaitValue32(s1, d_statusFlag, KERNELREADY + i,
                        CU_STREAM_WAIT_VALUE_GEQ);

    switch (link->savePinnedBuf) {
    case 0: // do not save anything
      break;

    case 10: // not here
      break;
    case 11:
      // gpu buffer
      break;
    case 12: // gpudirect buffer
      // special case for for JF testing
      break;
    case 13:
      cudaMemcpyAsync((void *)(((uint64_t)link->h_output1) +
                               offset * rashpa_bunch / 2 * h2dsize),
                      (void *)d_InAbis, rashpa_bunch / 2 * h2dsize,
                      cudaMemcpyDeviceToHost, s1);
      break;

    case 21:
      // printf("out_imgSize %d\n",out_imgSize);
      // cudaCheckErrors("cudaMemcpyAsync fail");
      cudaMemcpyAsync((void *)(((uint64_t)link->h_output1) +
                               offset * rashpa_bunch / 2 * out_imgSize),
                      (void *)d_OutA, rashpa_bunch / 2 * out_imgSize,
                      cudaMemcpyDeviceToHost, s1);
      cudaCheckErrors("cudaMemcpyAsync fail");

    case 23:
      // #if USESHARED==0
      // 					cudaMemcpyAsync(
      // 						(void*)
      // (((uint64_t)peakNum1)+4*offset2),
      // 						(void*)
      // (((uint64_t)d_scana)+k_w*k_h*4-4),
      // 						4,
      // 						cudaMemcpyDeviceToDevice,
      // 						s1) ;
      // 					cudaCheckErrors("cudaMemcpyAsync
      // fail");
      // #endif
      break;
    case 3:
      // cudaMemcpyAsync(
      // 	(void*)(((uint64_t)link->h_output1) + offset),
      // 	(void*) d_InAbis,
      // 	rashpa_bunch/2*h2dsize,
      // 	cudaMemcpyDeviceToHost,
      // 	s1) ;
      // memcpy(
      // 	(void*)((uint64_t)h_output + offset),
      // 	(void*)(((uint64_t)link->dest_addr) + rashpa_bunch/2*h2dsize),
      // 	rashpa_bunch/2*h2dsize);
      break;
    }

    // synchro
    cuStreamWaitValue32(s1, d_statusFlag, KERNELREADY + i + 1,
                        CU_STREAM_WAIT_VALUE_GEQ);
    switch (link->savePinnedBuf) {
    case 0: // do not save anything
      break;

    case 10: // not here
      break;
    case 11:
      // gpu buffer
      break;
    case 12: // gpudirect buffer
      break;
    case 13:
      cudaMemcpyAsync((void *)(((uint64_t)link->h_output2) +
                               offset * rashpa_bunch / 2 * h2dsize),
                      (void *)d_InBbis, rashpa_bunch / 2 * h2dsize,
                      cudaMemcpyDeviceToHost, s1);
      break;
    case 21:
      cudaMemcpyAsync((void *)(((uint64_t)link->h_output2) +
                               offset * rashpa_bunch / 2 * out_imgSize), //
                      (void *)d_OutB, rashpa_bunch / 2 * out_imgSize,
                      cudaMemcpyDeviceToHost, s1);
      cudaCheckErrors("cudaMemcpyAsync fail");

    case 23:
      // #if USESHARED==0
      // 					cudaMemcpyAsync(
      // 						(void*)
      // (((uint64_t)peakNum2)+4*offset2),
      // 						(void*)
      // (((uint64_t)d_scanb)+k_w*k_h*4-4),
      // 						4,
      // 						cudaMemcpyDeviceToDevice,
      // 						s1) ;
      // 					cudaCheckErrors("cudaMemcpyAsync
      // fail");
      // #endif
      break;
    }

    offset++; // never more than nImages
    offset2++;// count bunch in peakNum array
    if (offset * rashpa_bunch / 2 >= nImages / 2)
      offset = 0;
#endif // OMP
    link->nImgOnGpu++;
    if ((link->nImgOnGpu % 100) == 0) {
      debug_printf("async execution %d/%d images\r",
                   link->nImgOnGpu * rashpa_bunch, nImages * nIter);
      fflush(stdout);
    }
  } // end of main kernel loop

  if (gpucode != 10 && gpucode != 11) {
    debug_printf("\ntransfer and kernel prelaunched on stream, waiting for completion\n");
    cudaCheckErrors("cudaStreamSynchronize fail");
    cudaStreamSynchronize(s3);
    cudaCheckErrors("cudaStreamSynchronize fail");
    cudaStreamSynchronize(s2);
    cudaCheckErrors("cudaStreamSynchronize fail");
    cudaStreamSynchronize(s1);
    cudaCheckErrors("cudaStreamSynchronize fail");
  }
  // stop receiver main loop
  gpustate = 0;

  switch (link->savePinnedBuf) {
  case 0: // do not save anything
    break;

  case 10: // save nImages/2
    saveDataInFile("/tmp/pinnedres1.data", link->h_output1,
                   nImages / 2 * in_imgSize);
    saveDataInFile("/tmp/pinnedres2.data", link->h_output2,
                   nImages / 2 * in_imgSize);
    saveDataInFile("/tmp/pinnedres11.data", link->h_output11,
                   nImages / 2 * nIter * sizeof(int));
    saveDataInFile("/tmp/pinnedres22.data", link->h_output22,
                   nImages / 2 * nIter * sizeof(int));
    break;

  case 13:
  case 11: // save nImages/4 for JF but nImages /2 for Large GPU
    saveDataInFile("/tmp/pinnedres1.data", link->h_output1,
                   nImages / 2 * h2dsize); // /2
    saveDataInFile("/tmp/pinnedres2.data", link->h_output2,
                   nImages / 2 * h2dsize); /// 2
    break;

  case 12:
    // special case for for JF testing
    // printf("saving selected images and last received image \n");
    // checkCuda( cudaMemcpy(
    // 	(void*)((uint64_t)link->h_output2 ),
    // 	(void*)((uint64_t)d_Out22+(rashpa_bunch/2-1)*out_imgSize),
    // 	out_imgSize,
    // 	cudaMemcpyDeviceToHost) );
    // saveDataInFile("/tmp/pinnedres1.data", link->h_output1, offset1 );
    // saveDataInFile("/tmp/pinnedres2.data", link->h_output2, out_imgSize);

    saveDataInFile("/tmp/pinnedres11.data", link->h_output11,
                   nIter * nImages / 2 * sizeof(int));
    saveDataInFile("/tmp/pinnedres22.data", link->h_output22,
                   nIter * nImages / 2 * sizeof(int));
    break;

  case 21:
    // printf("out_imgSize %d\n", out_imgSize);
    saveDataInFile("/tmp/pinnedres1.data", link->h_output1,
                   nImages / 2 * out_imgSize);
    saveDataInFile("/tmp/pinnedres2.data", link->h_output2,
                   nImages / 2 * out_imgSize);

  case 23: // special case for for JF testing save all Images and pepak
    cudaCheckErrors("cudaMemcpy");
    cudaMemcpy((void *)((uint64_t)link->h_output11), (void *)peakNum1,
               nIter * nImages / 2 * sizeof(int), cudaMemcpyDeviceToHost);
    cudaCheckErrors("cudaMemcpy");
    cudaMemcpy((void *)((uint64_t)link->h_output22), (void *)peakNum2,
               nIter * nImages / 2 * sizeof(int), cudaMemcpyDeviceToHost);
    cudaCheckErrors("cudaMemcpy");
    saveDataInFile("/tmp/pinnedres11.data", link->h_output11,
                   nIter * nImages / 2 * sizeof(int));
    saveDataInFile("/tmp/pinnedres22.data", link->h_output22,
                   nIter * nImages / 2 * sizeof(int));
    break;
  case 24: // special case for for JF testing save all Images and pepak

    cudaMemcpy((void *)((uint64_t)link->h_output1), (void *)d_csrval,
               nImages * k_w * k_h * sizeof(int), cudaMemcpyDeviceToHost);
    cudaCheckErrors("cudaMemcpy");

    saveDataInFile("/tmp/pinnedres1.data", link->h_output1,
                   nImages / 2 * out_imgSize);
    saveDataInFile("/tmp/pinnedres2.data", link->h_output2,
                   nImages / 2 * out_imgSize);
    saveDataInFile("/tmp/pinnedres11.data", link->h_output11,
                   nImages / 2 * sizeof(int));
    saveDataInFile("/tmp/pinnedres22.data", link->h_output22,
                   nImages / 2 * sizeof(int));
    break;

  case 3: // save input data
    // checkCuda( cudaMemcpy((void*)((uint64_t)link->h_output1 + offset1),
    // 					(void*) d_Out11,
    // 					rashpa_bunch * out_imgSize,
    // 					cudaMemcpyDeviceToHost) );
    // checkCuda( cudaMemcpy((void*)((uint64_t)link->h_output2 + offset2),
    // 					(void*) d_Out22,
    // 					rashpa_bunch * out_imgSize,
    // cudaMemcpyDeviceToHost)
    // );

    //
    if (gpucode == 2) {
      saveDataInFile("/tmp/pinnedres1.data", link->h_output1,
                     nImages / 2 * h2dsize);
      saveDataInFile("/tmp/pinnedres2.data", link->h_output2,
                     nImages / 2 * h2dsize);
    } else {
      saveDataInFile("/tmp/pinnedres1.data", link->h_output1,
                     nImages / 2 * in_imgSize);
      saveDataInFile("/tmp/pinnedres2.data", link->h_output2,
                     nImages / 2 * in_imgSize);
    }

    break;
  }
  debug_printf("saved...\n");

  // signal completion to RASHPAMANAGER
  int sock, r;
  struct sockaddr_in adr;
  sock = socket(AF_INET, SOCK_DGRAM, 0);
  if (sock == -1)
    printf("erreur socket\n");
  adr.sin_family = AF_INET;
  adr.sin_port = htons(3333);
  // rashpa manager ip addr and port
  adr.sin_addr.s_addr = inet_addr("127.0.0.1");

  r = bind(sock, (struct sockaddr *)(&adr), sizeof(struct sockaddr));
  // if (r==-1) printf("BIND error\n");

  // FIXME ??? need to wait for file saving
  sleep(3);
  debug_printf("status send to manager %s\n", link->messages);
  r = sendto(sock, link->messages, strlen(link->messages), 0,
             (struct sockaddr *)(&adr), sizeof(struct sockaddr_in));
  if (r == -1)
    printf("SENDTO error%s\n", link->messages);
  close(sock);

  // free cpu/device memory
  cudaFree(d_In1);
  cudaFree(d_In2);
  cudaFree(d_In1bis);
  cudaFree(d_In2bis);
  cudaCheckErrors("cudafreefail");

  cudaFree(d_Out1);
  cudaFree(d_Out2);
  cudaFree(d_Out11);
  cudaFree(d_Out22);
  cudaFree(peakNum1);
  cudaFree(peakNum2);
  cudaFree(jfLut1);
  cudaFree(jfLut2);
  cudaFree(dGainMap);
  cudaCheckErrors("cudafreefail");

  cudaFree(d_boolsa);
  cudaFree(d_scana);
  cudaFree(block_resultsa);
  cudaFree(dummy_resultsa);
  // cudaFree(d_csrvala);
  cudaFree(d_csrval);
  cudaFree(d_indicesa);
  cudaFree(d_indptra);
  cudaFree(d_boolsb);
  cudaFree(d_scanb);
  cudaFree(block_resultsb);
  cudaFree(dummy_resultsb);
  // cudaFree(d_csrvalb);
  cudaFree(d_indicesb);
  cudaFree(d_indptrb);
  cudaCheckErrors("cudafreefail");

  cudaStreamDestroy(s1);
  cudaStreamDestroy(s2);
  cudaStreamDestroy(s3);
  cudaCheckErrors("cudaStreamdestroy fail");

  free(gainMap);
  link->romulu_state = ROMULU_LINK_READY;
  return NULL;
}
