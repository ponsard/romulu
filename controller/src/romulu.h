/*---------------------------------------------------------------------------
 * ESRF -- The European Synchrotron
 *         Instrumentation Services and Development Division
 *
 * Project: romulu   //  RASHPA emulator backend
 *
 * $URL: $
 * $Rev: $
 * $Date: $
 *------------------------------------------------------------------------- */

#ifndef __ROMULU_H_INCLUDED__
#define __ROMULU_H_INCLUDED__

#include <infiniband/verbs.h>
#include <librashpa.h>
#include <stdint.h>

#define ROMULU_OK 0
#define ROMULU_ERROR 1

typedef struct {
  int state; // consider one state par receiver ?
  //~ rashpa_ctx_t 					link; //should be an
  //array
  //~ roce_ctxt_t 					roce_ctx;
  //~ zmq_ctxt_t						zmq_ctx;
  //~ gpu_ctxt_t						gpu_ctx;
} romulu_globals_t;

extern romulu_globals_t *g_romulu;
extern const char *ROMULU_STATE[];

#endif //  __ROMULU_H_INCLUDED__
