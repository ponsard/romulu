// H->D transfer
			if (link->link_type == RASHPA_LINK_ROCEV2_UD && link->receiver_type != RASHPA_RECEIVER_NVIDIA_CUDAMEM_GPU) 
			{
				d_In1 = link->dest_addr;		
			}
			else
			{
				// printf("cudaMemcpyAsync1\n");
				checkCuda( cudaMemcpyAsync(
					d_In1,
					(void*)((uint64_t)link->dest_addr),
					rashpa_bunch * in_imgSize,
					cudaMemcpyHostToDevice, gpu->s1) ); 
			}
			// to float32 and reconstruction
			if (link->link_type == RASHPA_LINK_UDP_JF) 
			{
				// printf("cudaMemcpyAsync11\n");
				checkCuda( cudaMemsetAsync(
					jfLut1,
					0,rashpa_bunch * sizeof(char), gpu->s1) );
				
				//8246x128
				// printf("createLut\n");
				//rashpa_bunch (each image) x block (1,128,1) 1 thread per line 
				createLut<<< dim3(1,1,rashpa_bunch), dim3(1,128,1), 0, gpu->s1>>> (
					(char*)d_In1,  
					(char*)jfLut1, 
					8246, 128);		
				//1024x512
				// printf("reorder\n");
				// grid 128 x 4 lines by 1024 thread
				reorder<<< dim3(1,128,rashpa_bunch), dim3(1024,1,1), 0, gpu->s1>>> (
					(uint16_t*)d_In1, 
					(uint16_t*)d_Out1, 
					(char*)jfLut1, 
					1024, 512);		
				// printf("applyGainPedestal\n");
				applyGainPedestal<<< dim3(1,512,rashpa_bunch/2), dim3(1024,1,1), 0, gpu->s1>>> (
					(uint16_t*)d_Out1, 
					(float*)d_Out11, 
					(float*)dGainMap, 
					1024, 512);	
				// printf("cudaMemsetAsync\n");
				checkCuda( cudaMemsetAsync(
					peakNum1,
					0,rashpa_bunch/2 * sizeof(int), gpu->s1) );
				// printf("peakCounter\n");
				peakCounter<<< dim3(1,512,rashpa_bunch/2), dim3(1024,1,1), 0, gpu->s1>>> (
					(float*)d_Out11, 
					peakNum1, 
					1024, 512);
				// printf("cudaMemcpyAsync12\n");
				checkCuda( cudaMemcpyAsync(
					(void*)(((uint64_t)link->h_output11) + peakOffset1), 
					(void*) peakNum1, 
					rashpa_bunch/2*sizeof(int), 
					cudaMemcpyDeviceToHost, gpu->s1) );
				peakOffset1 += rashpa_bunch/2*sizeof(int) ;

				cudaStreamSynchronize(gpu->s1);
				simag1 = veto((int*)(((uint64_t)link->h_output11) + peakOffset1), &vetoCount);
			}
			if (link->link_type == RASHPA_LINK_ROCEV2_UD) 
			{
				//printf("applyGainPedestal %p  bunch %d\n",d_In1,rashpa_bunch);
				checkCuda( cudaMemsetAsync(d_Out11,0,rashpa_bunch * out_imgSize, gpu->s1) );
				// applyGainPedestal<<< dim3(1,512,rashpa_bunch), dim3(1024,1,1), 0, gpu->s1>>> ( //
				// 	(uint16_t*)d_In1, 
				// 	(float*)d_Out11, 
				// 	(float*)dGainMap, 
				// 	1024, 512);	

				uint16tofloat<<< dim3(1,512,rashpa_bunch), dim3(1024,1,1), 0, gpu->s1>>> ( 
					(uint16_t*)d_In1, 
					(float*)d_Out11, 
					1024, 512);	

				checkCuda( cudaMemsetAsync(d_In1,0,rashpa_bunch * in_imgSize, gpu->s1) );

				// printf("cudaMemsetAsync\n");
				// checkCuda( cudaMemsetAsync(
				// 	peakNum1,
				// 	0,rashpa_bunch/2 * sizeof(int), gpu->s1) );
				// // printf("peakCounter\n");
				// peakCounter<<< dim3(1,512,rashpa_bunch/2), dim3(1024,1,1), 0, gpu->s1>>> (
				// 	(float*)d_Out11, 
				// 	peakNum1, 
				// 	1024, 512);
				// // printf("cudaMemcpyAsync12\n");
				// checkCuda( cudaMemcpyAsync(
				// 	(void*)(((uint64_t)link->h_output11) + peakOffset1), 
				// 	(void*) peakNum1, 
				// 	rashpa_bunch/2*sizeof(int), 
				// 	cudaMemcpyDeviceToHost, gpu->s1) );
				// peakOffset1 += rashpa_bunch/2*sizeof(int) ;

				// cudaStreamSynchronize(gpu->s1);
				// simag1 = veto((int*)(((uint64_t)link->h_output11) + peakOffset1), &vetoCount);
			}
			if(0)
			{
				leftRotation_float<uint16_t, float> <<< dimGrid0, dimBlock0, 0, gpu->s1>>> (
					(uint16_t*)d_In1, 
					(float*)d_Out1, 
					width, height);
			}

			//common kernel
			
			switch(link->savePinnedBuf) 
			{
				case 1:
						checkCuda( cudaMemcpyAsync(
							(void*)(((uint64_t)link->h_output1) + offset1), 
							(void*) d_Out1,
							rashpa_bunch * out_imgSize,
							cudaMemcpyDeviceToHost, gpu->s1) );
						offset1  += rashpa_bunch * out_imgSize;
						break;
				case 21:
						//2 images per sampling
						//printf("cudaMemcpyAsync13 %d\n",offset1);
						checkCuda( cudaMemcpyAsync(
							(void*)(((uint64_t)link->h_output1) + offset1), 
							(void*) d_Out11,
							rashpa_bunch * out_imgSize,
							cudaMemcpyDeviceToHost, gpu->s1) );
						offset1  += rashpa_bunch * out_imgSize;
						break;
				case 22:
						if (simag1!=-1) 
						{
							int nb = veto_h < (rashpa_bunch/2-simag1) ? veto_h : (rashpa_bunch/2-simag1) ;
							printf("veto1 simag1 %d save %d\n",simag1, nb);
							checkCuda( cudaMemcpyAsync(
								(void*)(((uint64_t)link->h_output1) + offset1), 
								(void*)((uint64_t)d_Out11+simag1*out_imgSize),
								nb*out_imgSize,
								cudaMemcpyDeviceToHost, gpu->s1));
							offset1  += nb*out_imgSize;
							if (offset1>nImages*out_imgSize) offset1=0;
						}
						break;
				case 3:
						memcpy((void*)((uint64_t)link->h_output1 + offset1), (void*) d_In1, rashpa_bunch *out_imgSize);							
						offset1  += rashpa_bunch * out_imgSize;
						break;		
			}
			if (offset1 >= nImages/2*out_imgSize) 
				offset1 = 0;	
			cudaStreamSynchronize(gpu->s1);
		} 
		// end of kernel1 on low addr buffer

		//Kernel 2
		else 
		{
			// H->D transfer
			if (link->link_type == RASHPA_LINK_ROCEV2_UD && link->receiver_type != RASHPA_RECEIVER_NVIDIA_CUDAMEM_GPU) 
			{
				d_In2 = (void*)(((uint64_t)link->dest_addr) + rashpa_bunch*in_imgSize);		
			}
			else
			{
				// printf("cudaMemcpyAsync2\n");
				checkCuda( cudaMemcpyAsync(
					d_In2,
					(void*)(((uint64_t)link->dest_addr) + rashpa_bunch*in_imgSize),
					rashpa_bunch * in_imgSize,
					cudaMemcpyHostToDevice, gpu->s2) ); 
			}
			// to float32 and reconstruction
			if (link->link_type == RASHPA_LINK_UDP_JF) 
			{
				// printf("cudaMemcpyAsync21\n");
				checkCuda( cudaMemsetAsync(
					jfLut2,
					0,rashpa_bunch * sizeof(char), gpu->s2) );
				
				//8246x128
				// printf("createLut2\n");
				createLut<<< dim3(1,1,rashpa_bunch), dim3(1,128,1), 0, gpu->s2>>> (
					(char*)d_In2,  
					(char*)jfLut2, 
					8246, 128);		
				//1024x512
				// printf("reorder2\n");
				reorder<<< dim3(1,128,rashpa_bunch), dim3(1024,1,1), 0, gpu->s2>>> (
					(uint16_t*)d_In2, 
					(uint16_t*)d_Out2, 
					(char*)jfLut1, 
					1024, 512);		
				// printf("applyGainPedestal2\n");
				applyGainPedestal<<< dim3(1,512,rashpa_bunch/2), dim3(1024,1,1), 0, gpu->s2>>> (
					(uint16_t*)d_Out2, 
					(float*)d_Out22, 
					(float*)dGainMap, 
					1024, 512);	
				// printf("cudaMemsetAsync2\n");
				checkCuda( cudaMemsetAsync(
					peakNum2,
					0,rashpa_bunch/2 * sizeof(int), gpu->s2) );
				// printf("peakCounter2\n");
				peakCounter<<< dim3(1,512,rashpa_bunch/2), dim3(1024,1,1), 0, gpu->s2>>> (
					(float*)d_Out22, 
					peakNum2, 
					1024, 512);
				// printf("cudaMemcpyAsync22\n");
				checkCuda( cudaMemcpyAsync(
					(void*)(((uint64_t)link->h_output22) + peakOffset2), 
					(void*) peakNum2, 
					rashpa_bunch/2*sizeof(int), 
					cudaMemcpyDeviceToHost, gpu->s2) );

				cudaStreamSynchronize(gpu->s2);

				simag2 = veto((int*)(((uint64_t)link->h_output22) + peakOffset2), &vetoCount);
		
				peakOffset2 += rashpa_bunch/2*sizeof(int);
			}

			// to float32 and reconstruction
			if (link->link_type == RASHPA_LINK_ROCEV2_UD) 
			{
				// printf("applyGainPedestal2\n");
				// applyGainPedestal<<< dim3(1,512,rashpa_bunch/2), dim3(1024,1,1), 0, gpu->s2>>> (
				// 	(uint16_t*)d_In2, 
				// 	(float*)d_Out22, 
				// 	(float*)dGainMap, 
				// 	1024, 512);	
				checkCuda( cudaMemsetAsync(d_Out22,0,rashpa_bunch * out_imgSize, gpu->s2) );

				uint16tofloat<<< dim3(1,512,rashpa_bunch), dim3(1024,1,1), 0, gpu->s2>>> (
					(uint16_t*)d_In2, 
					(float*)d_Out22, 
					1024, 512);	
				checkCuda( cudaMemsetAsync(d_In2,0,rashpa_bunch * in_imgSize, gpu->s2) );

				// printf("cudaMemsetAsync2\n");
				checkCuda( cudaMemsetAsync(
					peakNum2,
					0,rashpa_bunch/2 * sizeof(int), gpu->s2) );
				// printf("peakCounter2\n");
				peakCounter<<< dim3(1,512,rashpa_bunch/2), dim3(1024,1,1), 0, gpu->s2>>> (
					(float*)d_Out22, 
					peakNum2, 
					1024, 512);
				// printf("cudaMemcpyAsync22\n");
				checkCuda( cudaMemcpyAsync(
					(void*)(((uint64_t)link->h_output22) + peakOffset2), 
					(void*) peakNum2, 
					rashpa_bunch/2*sizeof(int), 
					cudaMemcpyDeviceToHost, gpu->s2) );

				cudaStreamSynchronize(gpu->s2);

				simag2 = veto((int*)(((uint64_t)link->h_output22) + peakOffset2), &vetoCount);
		
				peakOffset2 += rashpa_bunch/2*sizeof(int);
			}
			if(0)
			{
				leftRotation_float<uint16_t, float> <<< dimGrid0, dimBlock0, 0, gpu->s2>>> (
					(uint16_t*)d_In2, 
					(float*)d_Out2, 
					width, height);
			}
			
			//common kernel
			
			switch(link->savePinnedBuf) 
			{
				case 0: //do not save anything
						break;
				case 1: //save d_Out2
						checkCuda( cudaMemcpyAsync(
							(void*)((uint64_t)link->h_output2 + offset2), 
							(void*) d_Out2,
							rashpa_bunch * out_imgSize,
							cudaMemcpyDeviceToHost, gpu->s2) );
						offset2  += rashpa_bunch * out_imgSize;
						break;
				case 21:
						//2 images per sampling
						// printf("cudaMemcpyAsync23\n");
						checkCuda( cudaMemcpyAsync(
							(void*)((uint64_t)link->h_output2 + offset2), 
							(void*) d_Out22,
							rashpa_bunch * out_imgSize,
							cudaMemcpyDeviceToHost, gpu->s2) );
						offset2  += rashpa_bunch * out_imgSize;
						break;

				case 22:
						if (simag2!=-1) 
						{
							int nb = veto_h < (rashpa_bunch/2-simag2) ? veto_h : (rashpa_bunch/2-simag2) ;  
							//printf("veto2 simag2 %d save %d\n",simag2, nb);
							checkCuda( cudaMemcpyAsync(
								(void*)(((uint64_t)link->h_output1) + offset1), 
								(void*)((uint64_t)d_Out22+simag2*out_imgSize),
								nb*out_imgSize,
								cudaMemcpyDeviceToHost, gpu->s2));
							offset1  += nb*out_imgSize;
							if (offset1>nImages*out_imgSize) offset1=0;
						}
						break;
				case 3:
						memcpy((void*)((uint64_t)link->h_output2 + offset2), (void*) d_In2, rashpa_bunch *out_imgSize);							
						offset2  += rashpa_bunch * out_imgSize;
						break;		
			}
			if (offset2 >= nImages/2*out_imgSize) 
				offset2 = 0;	

			cudaStreamSynchronize(gpu->s2);
		} 

		//veto algorithm : save fifth non null image