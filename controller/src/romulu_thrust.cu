

//Using Thrust




template <typename T>
struct is_greater_than_threshold
{
	T threshold;

	is_greater_than_threshold(T thr) : threshold(thr) {};

  __host__ __device__
  bool operator()(const T& x) const
  {
	return x < threshold;
  }
};


template <typename T>
struct summary_stats_data
{
    T n;
    T mean;
    T M2;
   
    // initialize to the identity element
	__host__ __device__
    void initialize()
    {
      n = mean = M2 = (T)0;
    }
	__host__ __device__
    T variance_n() { return M2 / n; }
};

// stats_unary_op is a functor that takes in a value x and
// returns a variace_data whose mean value is initialized to x.
template <typename Ti,typename To>
struct to_float4
{
    __host__ __device__
    summary_stats_data<To> operator()(const Ti& x) const
    {
         summary_stats_data<To> result;
         result.n    = 1.0f;
         result.mean = (To) x;
         result.M2   = 0.0f;

         return result;
    }
};

// summary_stats_binary_op is a functor that accepts two summary_stats_data 
// structs and returns a new summary_stats_data which are an
// approximation to the summary_stats for 
// all values that have been agregated so far
template <typename T>
struct compute_stats 
    : public thrust::binary_function<const summary_stats_data<T>&, 
                                     const summary_stats_data<T>&,
                                           summary_stats_data<T> >
{
    __host__ __device__
    summary_stats_data<T> operator()(const summary_stats_data<T>& x, const summary_stats_data <T>& y) const
    {
        summary_stats_data<T> result;
        
        // precompute some common subexpressions
        T n  = x.n + y.n;
        T delta  = y.mean - x.mean;
        T delta2 = delta  * delta;
		// if (isnan(delta))  delta=0.0f;
		// if (isnan(delta2)) delta2=0.0f;

        //Basic number of samples (n), min, and max
        result.n    = n;
        result.mean = x.mean + delta * y.n / n;
        result.M2  = x.M2 + y.M2;
        result.M2 += delta2 * x.n * y.n / n;
        
        return result;
    }
};



thrust::device_ptr<uint16_t> din1 ((uint16_t*) d_In1);
thrust::device_ptr<float> dout1 ((float*) d_Out1);
thrust::device_ptr<float> dout2 ((float*) d_Out2);

summary_stats_data<float> init;
init.initialize();
summary_stats_data<float> result1;



thrust::device_ptr<uint16_t> din2 ((uint16_t*) d_In2);

for (int i=0;i<rashpa_bunch;i++) 
{
    result2 = 
    thrust::transform_reduce(thrust::device,
    						din2 + i*width * height, din2 + (i+1)*width * height,
    						to_float4<uint16_t,float>(), init, compute_stats<float>());
    thrust::replace_copy_if(thrust::device,
    						din2 + i*width * height, din2 + (i+1)*width * height,
    						dout2 + i*width * height,
    						is_greater_than_threshold<float>(result2.mean + std::sqrt(result2.variance_n())), 0.0f); 
}