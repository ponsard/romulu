#!/bin/bash
#
#
# NEW VERSION: GET RID OF LIBDANCE
#
#

# cd romulu/controller/src

source /segfs/deg/bin/degprofile.sh
RASHPA_DIR=/users/ponsard/working/rashpa_poc/
# RASHPA_DIR=/tmp_14_days/ponsard/rashpa_poc/

#change require in Makefile line 26
LIBVMA=$VMA
NOPROF=$PROF
CUDA=1
# make clean
rm -f romulu_opencl.o romulu_nvidia.o $RASHPA_DIR/librashpa/src/*.o /tmp/controller

if [ "$CUDA" = "1" ]; then
echo "cuda defined"
else
echo "cuda NOT defined"
fi

make  

g++ romulu_opencl.cpp -o romulu_opencl.o -c -I $RASHPA_DIR/librashpa/include/
#-ldance
LLIBS=" -lm -ldl -lrt -lpthread  -libverbs -lrdmacm -lxml2 -lzmq -lOpenCL -lnuma"

if [ "$CUDA" = "1" ]; then
echo "cuda defined"

#/usr/local/cuda/bin/
COMP="nvcc -ccbin g++-6 -m64  \
-gencode arch=compute_61,code=sm_61 -gencode arch=compute_70,code=sm_70 \
-DCUDA=1 -rdc=true"

$COMP -c -o romulu_nvidia.o romulu_nvidia.cu -I $RASHPA_DIR/librashpa/include -Xcompiler -fopenmp 
#$COMP -c -o romulu_nvidia.s romulu_nvidia.cu -I ~/librashpa/include -Xcompiler -fopenmp -Xcompiler -S

LLIBS=$LLIBS" -lcuda -lcudart "
# "-lnppist -lnppitc -lnppial -lnppisu -lnppc -lcusparse_static -lcublas_static -lculibos"
echo $LLIBS
echo "using CUDA compiler "$COMP
else
COMP="g++-6" 
fi


$COMP  -Xcompiler -fopenmp \
-o /tmp/controller  romulu_opencl.o newController.o romulu_nvidia.o \
romulu_rsocket.o \
$RASHPA_DIR/librashpa/src/rashpa_main.o $RASHPA_DIR/librashpa/src/rashpa_udp.o \
$RASHPA_DIR/librashpa/src/rashpa_xml.o  $RASHPA_DIR/librashpa/src/rashpa_init.o \
$RASHPA_DIR/librashpa/src/rashpa_roce_uc.o $RASHPA_DIR/librashpa/src/rashpa_zmq.o \
romulu_generic.o  \
-L $RASHPA_DIR $LLIBS

#sudo setcap "cap_dac_override+ep cap_sys_rawio+ep" /tmp/controller
#-L $RASHPA_DIR/romulu/controller/lib/libdance/src/

echo 'ROMULU DAnCE Controller is starting.....' 

if [ "$LIBVMA" = "1" ]; then
echo "using libVMA Messaging Accelerator... Incompatibility with RoCE"

# /usr/local/cuda/bin/
VMA_RX_BYTES_MIN=100000000 LD_PRELOAD=/usr/lib/libvma.so VMA_RX_BUFS=2000000 VMA_RX_PREFETCH_BYTES=4096 VMA_RX_CQ_DRAIN_RATE_NSEC=1 VMA_SELECT_POLL=-1 VMA_INTERNAL_THREAD_AFFINITY=3,7,11 VMA_TRACELEVEL=info \
LD_LIBRARY_PATH=. \
/tmp/controller -lAPP 
else
if [ "$NOPROF" = "0" ]; then
LD_LIBRARY_PATH=. \
/tmp/controller -lAPP 
else
LD_LIBRARY_PATH=. \
nvprof -f -o /tmp/log_romulu /tmp/controller -lAPP
fi
fi
echo $PRE

#nvprof -f -o /tmp/log_romulu 
#cuda-memcheck  
#/users/ponsard/nv-nsight-cu-cli 