/* 
    parallel scan algorithm 
    generated with PyCuda

*/

#define local_barrier() __syncthreads();

#define WITHIN_KERNEL __device__
//#define KERNEL extern "C" __global__
#define KERNEL  __global__
#define GLOBAL_MEM /* empty */
#define LOCAL_MEM __shared__
#define LOCAL_MEM_ARG /* empty */
#define REQD_WG_SIZE(X,Y,Z) __launch_bounds__(X*Y*Z, 1)

#define LID_0 threadIdx.x
#define LID_1 threadIdx.y
#define LID_2 threadIdx.z

#define GID_0 blockIdx.x
#define GID_1 blockIdx.y
#define GID_2 blockIdx.z

#define LDIM_0 blockDim.x
#define LDIM_1 blockDim.y
#define LDIM_2 blockDim.z

#define GDIM_0 gridDim.x
#define GDIM_1 gridDim.y
#define GDIM_2 gridDim.z

#define WG_SIZE 128
#define WG_SIZE2 256
#define SCAN_EXPR(a, b) a+b


///CHANGE TYPE HERE
// typedef float scan_type;
//CL//
#define K 6

    WITHIN_KERNEL
    void scan_group(LOCAL_MEM_ARG scan_type *array
    )
    {
        scan_type val = array[LID_0];



            if (LID_0 >= 1
            )
            {
                scan_type tmp = array[LID_0 - 1];
                val = SCAN_EXPR(tmp, val);
            }

            local_barrier();
            array[LID_0] = val;
            local_barrier();


            if (LID_0 >= 2
            )
            {
                scan_type tmp = array[LID_0 - 2];
                val = SCAN_EXPR(tmp, val);
            }

            local_barrier();
            array[LID_0] = val;
            local_barrier();


            if (LID_0 >= 4
            )
            {
                scan_type tmp = array[LID_0 - 4];
                val = SCAN_EXPR(tmp, val);
            }

            local_barrier();
            array[LID_0] = val;
            local_barrier();


            if (LID_0 >= 8
            )
            {
                scan_type tmp = array[LID_0 - 8];
                val = SCAN_EXPR(tmp, val);
            }

            local_barrier();
            array[LID_0] = val;
            local_barrier();


            if (LID_0 >= 16
            )
            {
                scan_type tmp = array[LID_0 - 16];
                val = SCAN_EXPR(tmp, val);
            }

            local_barrier();
            array[LID_0] = val;
            local_barrier();


            if (LID_0 >= 32
            )
            {
                scan_type tmp = array[LID_0 - 32];
                val = SCAN_EXPR(tmp, val);
            }

            local_barrier();
            array[LID_0] = val;
            local_barrier();


            if (LID_0 >= 64
            )
            {
                scan_type tmp = array[LID_0 - 64];
                val = SCAN_EXPR(tmp, val);
            }

            local_barrier();
            array[LID_0] = val;
            local_barrier();


            if (LID_0 >= 128
            )
            {
                scan_type tmp = array[LID_0 - 128];
                val = SCAN_EXPR(tmp, val);
            }

            local_barrier();
            array[LID_0] = val;
            local_barrier();


    }


    WITHIN_KERNEL
    void scan_group_n(LOCAL_MEM_ARG scan_type *array
      , const unsigned n
    )
    {
        scan_type val = array[LID_0];



            if (LID_0 >= 1
              && LID_0 < n
            )
            {
                scan_type tmp = array[LID_0 - 1];
                val = SCAN_EXPR(tmp, val);
            }

            local_barrier();
            array[LID_0] = val;
            local_barrier();


            if (LID_0 >= 2
              && LID_0 < n
            )
            {
                scan_type tmp = array[LID_0 - 2];
                val = SCAN_EXPR(tmp, val);
            }

            local_barrier();
            array[LID_0] = val;
            local_barrier();


            if (LID_0 >= 4
              && LID_0 < n
            )
            {
                scan_type tmp = array[LID_0 - 4];
                val = SCAN_EXPR(tmp, val);
            }

            local_barrier();
            array[LID_0] = val;
            local_barrier();


            if (LID_0 >= 8
              && LID_0 < n
            )
            {
                scan_type tmp = array[LID_0 - 8];
                val = SCAN_EXPR(tmp, val);
            }

            local_barrier();
            array[LID_0] = val;
            local_barrier();


            if (LID_0 >= 16
              && LID_0 < n
            )
            {
                scan_type tmp = array[LID_0 - 16];
                val = SCAN_EXPR(tmp, val);
            }

            local_barrier();
            array[LID_0] = val;
            local_barrier();


            if (LID_0 >= 32
              && LID_0 < n
            )
            {
                scan_type tmp = array[LID_0 - 32];
                val = SCAN_EXPR(tmp, val);
            }

            local_barrier();
            array[LID_0] = val;
            local_barrier();


            if (LID_0 >= 64
              && LID_0 < n
            )
            {
                scan_type tmp = array[LID_0 - 64];
                val = SCAN_EXPR(tmp, val);
            }

            local_barrier();
            array[LID_0] = val;
            local_barrier();


            if (LID_0 >= 128
              && LID_0 < n
            )
            {
                scan_type tmp = array[LID_0 - 128];
                val = SCAN_EXPR(tmp, val);
            }

            local_barrier();
            array[LID_0] = val;
            local_barrier();


    }


KERNEL
REQD_WG_SIZE(WG_SIZE, 1, 1)
void scan_scan_intervals(
    GLOBAL_MEM scan_type *input,
    const unsigned int N,
    const unsigned int interval_size,
    GLOBAL_MEM scan_type *output,
    GLOBAL_MEM scan_type *group_results)
{
    // padded in WG_SIZE to avoid bank conflicts
    // index K in first dimension used for carry storage
    LOCAL_MEM scan_type ldata[K + 1][WG_SIZE + 1];

    const unsigned int interval_begin = interval_size * GID_0;
    const unsigned int interval_end   = min(interval_begin + interval_size, N);

    const unsigned int unit_size  = K * WG_SIZE;

    unsigned int unit_base = interval_begin;


            for(; unit_base + unit_size <= interval_end; unit_base += unit_size)

        {
            // Algorithm: Each work group is responsible for one contiguous
            // 'interval', of which there are just enough to fill all compute
            // units.  Intervals are split into 'units'. A unit is what gets
            // worked on in parallel by one work group.

            // Each unit has two axes--the local-id axis and the k axis.
            //
            // * * * * * * * * * * ----> lid
            // * * * * * * * * * *
            // * * * * * * * * * *
            // * * * * * * * * * *
            // * * * * * * * * * *
            // |
            // v k

            // This is a three-phase algorithm, in which first each interval
            // does its local scan, then a scan across intervals exchanges data
            // globally, and the final update adds the exchanged sums to each
            // interval.

            // Exclusive scan is realized by performing a right-shift inside
            // the final update.

            // read a unit's worth of data from global

            for(unsigned int k = 0; k < K; k++)
            {
                const unsigned int offset = k*WG_SIZE + LID_0;

                {
                    ldata[offset % K][offset / K] = input[unit_base + offset];
                }
            }

            // carry in from previous unit, if applicable.
            if (LID_0 == 0 && unit_base != interval_begin)
                ldata[0][0] = SCAN_EXPR(ldata[K][WG_SIZE - 1], ldata[0][0]);

            local_barrier();

            // scan along k (sequentially in each work item)
            scan_type sum = ldata[0][LID_0];


            for(unsigned int k = 1; k < K; k++)
            {
                {
                    scan_type tmp = ldata[k][LID_0];
                    sum = SCAN_EXPR(sum, tmp);
                    ldata[k][LID_0] = sum;
                }
            }

            // store carry in out-of-bounds (padding) array entry in the K direction
            ldata[K][LID_0] = sum;
            local_barrier();

            // tree-based parallel scan along local id
                scan_group(&ldata[K][0]);

            // update local values
            if (LID_0 > 0)
            {
                sum = ldata[K][LID_0 - 1];

                for(unsigned int k = 0; k < K; k++)
                {
                    {
                        scan_type tmp = ldata[k][LID_0];
                        ldata[k][LID_0] = SCAN_EXPR(sum, tmp);
                    }
                }
            }

            local_barrier();

            // write data
            for(unsigned int k = 0; k < K; k++)
            {
                const unsigned int offset = k*WG_SIZE + LID_0;

                {
                    output[unit_base + offset] = ldata[offset % K][offset / K];
                }
            }

            local_barrier();
        }


            if (unit_base < interval_end)

        {
            // Algorithm: Each work group is responsible for one contiguous
            // 'interval', of which there are just enough to fill all compute
            // units.  Intervals are split into 'units'. A unit is what gets
            // worked on in parallel by one work group.

            // Each unit has two axes--the local-id axis and the k axis.
            //
            // * * * * * * * * * * ----> lid
            // * * * * * * * * * *
            // * * * * * * * * * *
            // * * * * * * * * * *
            // * * * * * * * * * *
            // |
            // v k

            // This is a three-phase algorithm, in which first each interval
            // does its local scan, then a scan across intervals exchanges data
            // globally, and the final update adds the exchanged sums to each
            // interval.

            // Exclusive scan is realized by performing a right-shift inside
            // the final update.

            // read a unit's worth of data from global

            for(unsigned int k = 0; k < K; k++)
            {
                const unsigned int offset = k*WG_SIZE + LID_0;

                if (unit_base + offset < interval_end)
                {
                    ldata[offset % K][offset / K] = input[unit_base + offset];
                }
            }

            // carry in from previous unit, if applicable.
            if (LID_0 == 0 && unit_base != interval_begin)
                ldata[0][0] = SCAN_EXPR(ldata[K][WG_SIZE - 1], ldata[0][0]);

            local_barrier();

            // scan along k (sequentially in each work item)
            scan_type sum = ldata[0][LID_0];

                const unsigned int offset_end = interval_end - unit_base;

            for(unsigned int k = 1; k < K; k++)
            {
                if (K * LID_0 + k < offset_end)
                {
                    scan_type tmp = ldata[k][LID_0];
                    sum = SCAN_EXPR(sum, tmp);
                    ldata[k][LID_0] = sum;
                }
            }

            // store carry in out-of-bounds (padding) array entry in the K direction
            ldata[K][LID_0] = sum;
            local_barrier();

            // tree-based parallel scan along local id
                scan_group_n(&ldata[K][0], offset_end / K);

            // update local values
            if (LID_0 > 0)
            {
                sum = ldata[K][LID_0 - 1];

                for(unsigned int k = 0; k < K; k++)
                {
                    if (K * LID_0 + k < offset_end)
                    {
                        scan_type tmp = ldata[k][LID_0];
                        ldata[k][LID_0] = SCAN_EXPR(sum, tmp);
                    }
                }
            }

            local_barrier();

            // write data
            for(unsigned int k = 0; k < K; k++)
            {
                const unsigned int offset = k*WG_SIZE + LID_0;

                if (unit_base + offset < interval_end)
                {
                    output[unit_base + offset] = ldata[offset % K][offset / K];
                }
            }

            local_barrier();
        }


    // write interval sum
    if (LID_0 == 0)
    {
        group_results[GID_0] = output[interval_end - 1];
    }
}


//////////////////////////////////////////
////////final_update_src =
/////////////////////////////




//typedef float scan_type;
//CL//
KERNEL
REQD_WG_SIZE(WG_SIZE2, 1, 1)
void scan_final_update(
    GLOBAL_MEM scan_type *output,
    const unsigned int N,
    const unsigned int interval_size,
    GLOBAL_MEM scan_type *group_results)
{
    const unsigned int interval_begin = interval_size * GID_0;
    const unsigned int interval_end   = min(interval_begin + interval_size, N);

    if (GID_0 == 0)
        return;

    // value to add to this segment
    scan_type prev_group_sum = group_results[GID_0 - 1];

    // advance result pointer
    output += interval_begin + LID_0;

    for(unsigned int unit_base = interval_begin;
        unit_base < interval_end;
        unit_base += WG_SIZE2, output += WG_SIZE2)
    {
        const unsigned int i = unit_base + LID_0;

        if(i < interval_end)
        {
            *output = SCAN_EXPR(prev_group_sum, *output);
        }
    }
}