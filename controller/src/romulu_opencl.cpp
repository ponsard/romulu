/*
 *
g++ romulu_opencl.cpp -o romulu_opencl -lOpenCL -lm -I /opt/AMDAPPSDK-3.0/include/SDKUtil/ -I /opt/AMDAPPSDK-3.0/include

ldel04:
g++ romulu_opencl.cpp -o romulu_opencl -lOpenCL -lm -I /media/ponsard/NVMESSD/AMDAPPSDK-3.0/include/SDKUtil/ -I /media/ponsard/NVMESSD/AMDAPPSDK-3.0/include
*
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <math.h>
#include <CL/opencl.h>
#include <sys/mman.h>
#include <string.h>
#include <pthread.h>


#include "librashpa.h"
#include "rashpa_init.h"
#include "romulu_opencl.h"

extern "C" void saveDataInFile(const char*fname,void* buffer,size_t len);
// extern "C" void displayProfilingResult(struct timespec *end, struct timespec *start);
extern "C" void* opencl_gpu_compute_mainloop(void*arg);
extern "C" void init_nvidia_opencl_gpu_device(int roi_id);
extern "C" void release_opencl_gpu_context();



opencl_gpu_ctx_t g_opencl_gpu;

char KernelSource[16*1024];


void load_opencl_kernel(const char*fname)
{
int f = open(fname,O_RDONLY);
int offset = 0,n;
do
{
	n = read(f,KernelSource+offset, 1024);
	offset += n;
} while (n > 0);
close(f);
}




cl_device_id device_id;             // compute device id
cl_mem d_input;
cl_mem d_input2;
void init_nvidia_opencl_gpu_device(int roi_id)
{

	opencl_gpu_ctx_t* 	gpu 				= &g_opencl_gpu;
	rashpa_ctx_t* 		link				= &g_rashpa;

	int 				width 				= link->gpu_w;
	int 				height				= link->gpu_h;
	int 				in_bdepth			= link->bdepth;
	int 				out_bdepth			= sizeof(float);
	int 				imgSize				= width*height;
	int 				rashpa_bunch 		= link->rashpa_bunch;
	int 				nImages				= link->nImages;
	int err=0;

	int in_imgSize = width*height*in_bdepth;
	int out_imgSize = width * height * out_bdepth;
	cl_program 			program;                 // compute program
	cl_platform_id * 	platforms = NULL;
	cl_device_id* 		devices = NULL;

	printf("init_opencl_gpu_device devID %d\n",link->gpuId);

	//Output data in CPU memory
	link->h_output1 = (float*) malloc(nImages * out_imgSize  / 2);
	link->h_output2 = (float*) malloc(nImages * out_imgSize  / 2);
	memset(link->h_output1, 0, nImages * out_imgSize / 2);
	memset(link->h_output2, 0, nImages * out_imgSize / 2);

	//input buffer mapped to hugetlbfs 
	char devname[128]="/dev/hugepages/rashpadata";

	int fd = open(devname, O_RDWR|O_CREAT, 0755);
	if (fd < 0) {
		perror("/dev/hugepages/rashpadata Open failed");
		exit(1);
	}
	void* addr = mmap(	NULL,
						2 * rashpa_bunch * in_imgSize,
						PROT_READ|PROT_WRITE,
						MAP_SHARED|MAP_HUGETLB,
						fd,
						0);
	if (addr == MAP_FAILED)
	{
		perror("mmap ");
		printf("of %d error\n",imgSize * in_bdepth);
		unlink("/dev/hugepages/rashpadata");
		exit(1);
	}	
	link->dest_addr =  addr;

	//scan Platform and GPU
    cl_uint platformCount;
    clGetPlatformIDs(0, NULL, &platformCount);

    platformCount = 1; // FIXME hardcoded for scisoft14
    platforms = (cl_platform_id *) malloc(sizeof(cl_platform_id) * platformCount);
    clGetPlatformIDs(platformCount, platforms, NULL);

    // Connect to a compute device
    cl_uint devCount;
    clGetDeviceIDs( platforms[0], CL_DEVICE_TYPE_GPU, 0, NULL, &devCount);

    //devCount = 2;	// FIXME hardcoded for P6000
	devices = (cl_device_id *) malloc(sizeof(cl_device_id)*devCount);
	clGetDeviceIDs( platforms[0], CL_DEVICE_TYPE_GPU, devCount, devices, NULL);

	device_id = devices[link->gpuId]; 

	printf("clCreateContext\n");
    // Create a compute context
	gpu->context = clCreateContext(0, 1, &device_id, NULL, NULL, &err);
    if (!gpu->context)
    {
        printf("Error: Failed to create a compute context!\n");
        return ;
    }

	
 
	printf("clCreateBuffer\n");
	d_input  = clCreateBuffer(	gpu->context, 
								CL_MEM_READ_WRITE|CL_MEM_USE_HOST_PTR, //CL_MEM_ALLOC_HOST_PTR, 
								rashpa_bunch * in_imgSize,
								addr,
								NULL);
	d_input2  = clCreateBuffer(	gpu->context, 
								CL_MEM_READ_WRITE|CL_MEM_USE_HOST_PTR, //CL_MEM_ALLOC_HOST_PTR, 
								rashpa_bunch * in_imgSize ,
								addr + rashpa_bunch * in_imgSize ,
								NULL);

	printf("we have %d bytes successfully mapped CPU memory at %p\n",
						2 * rashpa_bunch * in_imgSize, link->dest_addr);


	printf("clCreateProgramWithSource %s ---\n", KernelSource);
    // Create the compute program from the source buffer
	char * srcfile = KernelSource;
	load_opencl_kernel("romulu_opencl.cl");
	size_t sourceSize[] = {strlen(srcfile)};
	//printf("%s",srcfile);
    program = clCreateProgramWithSource(gpu->context, 1, (const char **) & srcfile, sourceSize, &err);
    if (!program)
    {
        printf("Error: Failed to create compute program!\n");
        return ;
    }

	printf("clBuildProgram\n");
    // Build the program executable
    err = clBuildProgram(program, 0, NULL, NULL, NULL, NULL);
    if (err != CL_SUCCESS)
    {
        size_t len;
        char buffer[2048];

        printf("Error: Failed to build program executable!\n");
        clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG, sizeof(buffer), buffer, &len);
        printf("%s\n", buffer);
        exit(1);
    }

	printf("clCreateKernel\n");
    // Create the compute kernel in the program we wish to run
    gpu->kernel = clCreateKernel(program, "myrotate", &err);
    if (!gpu->kernel || err != CL_SUCCESS)
    {
        printf("Error: Failed to create compute kernel!\n");
        exit(1);
    }
 }





void* opencl_gpu_compute_mainloop(void*arg)
{
	opencl_gpu_ctx_t* 	gpu 				= &g_opencl_gpu;
	rashpa_ctx_t* 		link				= &g_rashpa;
	int 				width 				= link->gpu_w;
	int 				height				= link->gpu_h;
	int 				in_bdepth			= link->bdepth;
	int 				out_bdepth			= sizeof(float);
	int 				imgSize				= width*height;
	int 				rashpa_bunch 		= link->rashpa_bunch;
	int 				nImages				= link->nImages;
	int 				nIter				= link->nIter;

	cl_int err = 0;
    // cl_event ev; // = new cl_event[1];

    //output DEVICE memory for a bunch of images
	printf("clCreateBuffer %d\n",rashpa_bunch);
	cl_mem d_output1 = clCreateBuffer(	gpu->context,
										CL_MEM_WRITE_ONLY,
										nImages/2*imgSize*out_bdepth,NULL, NULL);
	cl_mem d_output2 = clCreateBuffer(	gpu->context,
										CL_MEM_WRITE_ONLY,
										nImages/2*imgSize*out_bdepth,NULL, NULL);

	size_t globalThreads[3]	= {(size_t)width, (size_t)height, (size_t)rashpa_bunch};   /**< global NDRange */
	size_t localThreads[3]	= {(size_t)32, (size_t)32, 1};

	
	if (err != CL_SUCCESS)
	{
		printf("Error: Failed to clEnqueueWriteBuffer!\n");
		exit(1);
	}
	//prepare kernel
	printf("clSetKernelArg0\n");
	err |= clSetKernelArg(gpu->kernel, 2, sizeof(int), &width);
	err |= clSetKernelArg(gpu->kernel, 3, sizeof(int), &height);
	if (err != CL_SUCCESS)
	{
		printf("Error: Failed to clSetKernelArg !\n");
		exit(1);
	}

	char msg[32];
	strcpy(msg,RASHPA_LINK_NAME[link->link_type]);

	size_t offset1,offset2; 
	offset1 = 0;
	offset2 = 0;
	link->nImgOnGpu=0;

	printf("clCreateCommandQueue\n");
    //Create a command queue
    gpu->queue = clCreateCommandQueue(gpu->context, device_id, 0, &err);
    if (!gpu->queue)
    {
        printf("Error: Failed to create a command commands1!\n");
        return NULL;
    }
	cl_command_queue queue2 = clCreateCommandQueue(gpu->context, device_id, 0, &err);
    if (!queue2)
    {
        printf("Error: Failed to create a command commands2!\n");
        return NULL;
    }


	//prepare buffer
	//if (!(link->nImgOnGpu % 2))
		{
			// clEnqueueMapBuffer(	gpu->queue,
			// 				d_input,
			// 				CL_TRUE, 
			// 				CL_MAP_READ, 
			// 				0,
			// 				rashpa_bunch * imgSize * in_bdepth,
			// 				0, NULL, NULL, &err);
			//printf("clEnqueueMapBuffer1 %p\n",p);
		}	
		//else 
		{
			//printf("clEnqueueMapBuffer2\n");
			// clEnqueueMapBuffer(	queue2,
			// 				d_input2,
			// 				CL_TRUE, 
			// 				CL_MAP_READ, 
			// 				0,
			// 				rashpa_bunch * imgSize * in_bdepth,
			// 				0, NULL, NULL, &err);
			//printf("clEnqueueMapBuffer2 %p\n",p);
		}

	while (link->nImgOnGpu*rashpa_bunch < nImages*nIter)
	{
		//printf("clCreateUserEvent %d\n",link->nImgOnGpu);
		//ev = clCreateUserEvent(gpu->context,&err);
		
		//wait for data
		pthread_cond_wait(&link->condition, &link->mutex);


		//FIXME BNKS DEPEND PROTOCOL

		if ((link->nImgOnGpu % 2))
		{
			// printf("clEnqueueWriteBuffer\n");
			//clEnqueueUnmapMemObject(gpu->queue, d_input, link->dest_addr, 0, NULL, NULL);
			err |= clEnqueueWriteBuffer(gpu->queue, 
										d_input,  
										CL_TRUE, 
										0, 
										rashpa_bunch*imgSize*in_bdepth, 
										(void*) ((uint64_t)link->dest_addr),  
										0, NULL, NULL );

			// printf("clSetKernelArg\n");
			clSetKernelArg(gpu->kernel, 0, sizeof(cl_mem), &d_input);
			// printf("clSetKernelArg\n");
			clSetKernelArg(gpu->kernel, 1, sizeof(cl_mem), &d_output1);
			// printf("clEnqueueNDRangeKernel\n");
			err = clEnqueueNDRangeKernel(gpu->queue,
										gpu->kernel, 3, NULL,
										globalThreads, localThreads,
										0, NULL, //&ev,
										NULL
										);
			//printf("clFinish\n");
			err |= clFinish(gpu->queue);
			if (err != CL_SUCCESS)
			{
				printf("Error: Failed to clEnqueueReadBuffer !\n");
				exit(1);
			}
			// printf("clEnqueueReadBuffer offset1 %d\n",offset1);
			err |= clEnqueueReadBuffer(	gpu->queue, 
										d_output1,  
										CL_TRUE, 
										0, 
										rashpa_bunch*imgSize*out_bdepth, 
										(void*) ((uint64_t)link->h_output1 + offset1),  
										0, NULL, NULL );

			// float zero=0.0f;
			// clEnqueueFillBuffer(gpu->queue, 
			// 			d_output1,  
			// 			(void*)&zero, 
			// 			in_bdepth, 
			// 			0,
			// 			rashpa_bunch*imgSize*out_bdepth,  
			// 			0, NULL, NULL );
			// clEnqueueFillBuffer(gpu->queue, 
			// 			d_input,  
			// 			(void*)&zero, 
			// 			in_bdepth, 
			// 			0,
			// 			rashpa_bunch*imgSize*out_bdepth,  
			// 			0, NULL, NULL );



			offset1  += rashpa_bunch*imgSize*out_bdepth;	//next bunch
			if (offset1 >= nImages/2*imgSize*out_bdepth) 
			{
				offset1 = 0;
				//debug_printf("reset offset2\n");
			}
			
		}
		else
		{
			// printf("clEnqueueWriteBuffer2\n");
			//clEnqueueUnmapMemObject(queue2, d_input2, link->dest_addr+rashpa_bunch*imgSize*in_bdepth, 0, NULL, NULL);
			err |= clEnqueueWriteBuffer(	gpu->queue, 
										d_input2,  
										CL_TRUE, 
										0, 
										rashpa_bunch*imgSize*in_bdepth, 
										(void*) ((uint64_t)link->dest_addr + rashpa_bunch*imgSize*in_bdepth),  
										0, NULL, NULL );
			// printf("clSetKernelArg2\n");
			clSetKernelArg (gpu->kernel, 0, sizeof(cl_mem), &d_input2); 
			clSetKernelArg(gpu->kernel, 1, sizeof(cl_mem), &d_output2);
			// printf("clEnqueueNDRangeKernel2\n");
			clEnqueueNDRangeKernel(queue2,
									gpu->kernel, 3, NULL,
									globalThreads, localThreads,
									0, NULL, 
									NULL
									);
			// printf("clEnqueueNDRangeKernel2\n");
			err |= clFinish(queue2);
			
			// printf("clEnqueueReadBuffer offset2 %d\n",offset2);
			err |= clEnqueueReadBuffer(	queue2, 
										d_output2,  
										CL_TRUE, 
										0, 
										rashpa_bunch*imgSize*out_bdepth, 
										(void*) ((uint64_t)link->h_output2 + offset2),  
										0, NULL, NULL );
			
			offset2  += rashpa_bunch*imgSize*out_bdepth;	
			if (offset2 >= nImages/2*imgSize*out_bdepth) 
			{
				offset2 = 0;
			}
		}

		link->nImgOnGpu++;
	}

	if (link->savePinnedBuf)
	{
		saveDataInFile("/tmp/pinnedres1.data", link->h_output1, nImages/2 * imgSize * out_bdepth);
		saveDataInFile("/tmp/pinnedres2.data", link->h_output2, nImages/2 * imgSize * out_bdepth);

	}

	link->romulu_state = ROMULU_LINK_READY;

}

void release_opencl_gpu_context()
{
	opencl_gpu_ctx_t* 	gpu 				= &g_opencl_gpu;
	rashpa_ctx_t* 		link				= &g_rashpa;

	// release OpenCL resources
	clReleaseMemObject(gpu->d_input);
	clReleaseMemObject(gpu->d_output);
//	clReleaseProgram(gpu->program);
	clReleaseKernel(gpu->kernel);
	clReleaseCommandQueue(gpu->queue);
	clReleaseContext(gpu->context);

	//release host memory
	//free(h_input);
	printf("FREE h_outpu\n");

	free(link->h_output1);
	free(link->h_output2);

}
