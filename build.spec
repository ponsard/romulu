[buildargs]
DANCE_SDK_PLATFORM = rnice
RULE = devel

[target controller]
method = cmake
path = controller/src
depsdir = controller/lib/

[dep libdance]
version = branches/dev-paf, rev:last

